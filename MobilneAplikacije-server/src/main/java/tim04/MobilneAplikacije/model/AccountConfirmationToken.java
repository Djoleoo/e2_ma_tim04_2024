package tim04.MobilneAplikacije.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@Getter
@Setter
public class AccountConfirmationToken {

    private String tokenId;

    private String confirmationToken;

    private Date createdDate;

    private User user;

    public AccountConfirmationToken(User user) {
        this.user = user;
        this.createdDate = new Date();
        confirmationToken = UUID.randomUUID().toString();
    }
}
