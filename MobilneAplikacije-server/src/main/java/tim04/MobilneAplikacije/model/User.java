package tim04.MobilneAplikacije.model;

import lombok.*;
import tim04.MobilneAplikacije.enums.UserType;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class User {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String address;
    private String phoneNumber;
    private UserType userType;
    private String userImage;
    private boolean isEnabled;
    private String companyId;

}