package tim04.MobilneAplikacije.repository;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Repository;
import tim04.MobilneAplikacije.TEST.CRUD;
import tim04.MobilneAplikacije.model.User;

import java.util.concurrent.ExecutionException;

@Repository
public class UserRepository {

    private final Firestore dbFirestore = FirestoreClient.getFirestore();

    public User getUserByDocumentId(String documentId ) throws ExecutionException, InterruptedException {
        DocumentReference documentReference = dbFirestore.collection("user").document(documentId);
        ApiFuture<DocumentSnapshot> future = documentReference.get();
        DocumentSnapshot document = future.get();
        User user;
        if(document.exists()){
            user = document.toObject(User.class);
            return user;
        }
        return null;
    }

    public void updateUser(User user){
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection("user").document(user.getId()).set(user);
    }

}
