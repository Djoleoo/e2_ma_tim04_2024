package tim04.MobilneAplikacije.repository;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Repository;
import tim04.MobilneAplikacije.model.User;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Repository
public class AuthRepository {

    private final Firestore dbFirestore = FirestoreClient.getFirestore();
    private final Storage storage = StorageOptions.getDefaultInstance().getService();
   // private final String bucketName = "mobilne-db.appspot.com";

    public User registerUser(User user) throws ExecutionException, InterruptedException, IOException {
        /*if (user.getUserImage() != null && !user.getUserImage().isEmpty()) {
            String imageName = UUID.randomUUID().toString(); // Generišemo jedinstveno ime slike
            String imageUrl = uploadImage(imageName, user.getUserImage()); // Čuvamo sliku u Firebase Storage
            user.setUserImage(imageUrl); // Postavljamo URL slike u objekat korisnika
        }
        */

        DocumentReference documentReference = dbFirestore.collection("user").add(user).get();
        String documentId = documentReference.getId();
        user.setId(documentId);
        dbFirestore.collection("user").document(documentId).update("id", documentId).get();
        return user;
    }
    /*
    private String uploadImage(String imageName, String imageData) throws IOException {
        byte[] imageBytes = imageData.getBytes(); // Konvertujemo string u bajt niz
        Blob blob = storage.create(BlobInfo.newBuilder(bucketName, imageName).build(), new ByteArrayInputStream(imageBytes));
        return blob.getMediaLink(); // Vraćamo URL slike
    }
    */
}


