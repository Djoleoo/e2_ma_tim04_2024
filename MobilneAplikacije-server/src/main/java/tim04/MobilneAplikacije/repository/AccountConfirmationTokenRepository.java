package tim04.MobilneAplikacije.repository;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Repository;
import tim04.MobilneAplikacije.TEST.CRUD;
import tim04.MobilneAplikacije.model.AccountConfirmationToken;

import java.util.concurrent.ExecutionException;

@Repository
public class AccountConfirmationTokenRepository {

    private final Firestore dbFirestore = FirestoreClient.getFirestore();

    public void createToken(AccountConfirmationToken confirmationToken) throws ExecutionException, InterruptedException {
        ApiFuture<WriteResult> collectionsApiFuture  = dbFirestore.collection("account-confirmation-token").document(confirmationToken.getConfirmationToken()).set(confirmationToken);
        //String documentId = documentReference.getId();
        //confirmationToken.setTokenId(documentId);
        //dbFirestore.collection("account-confirmation-token").document(documentId).update("tokenId", documentId).get();
    }

    public AccountConfirmationToken getByToken(String confirmationToken) throws ExecutionException, InterruptedException {
        DocumentReference documentReference = dbFirestore.collection("account-confirmation-token").document(confirmationToken);
        ApiFuture<DocumentSnapshot> future = documentReference.get();
        DocumentSnapshot document = future.get();
        AccountConfirmationToken token;
        if(document.exists()){
            token = document.toObject(AccountConfirmationToken.class);
            return token;
        }
        return null;
    }
}
