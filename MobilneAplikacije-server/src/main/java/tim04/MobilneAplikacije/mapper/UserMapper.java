package tim04.MobilneAplikacije.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.modelmapper.ModelMapper;
import tim04.MobilneAplikacije.dto.RegistrationDto;
import tim04.MobilneAplikacije.model.User;

@Component
public class UserMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public UserMapper(ModelMapper mapper) {this.modelMapper = mapper;}

    public User registrationDtoToUser(RegistrationDto dto){
        User user = modelMapper.map(dto,User.class);
        return user;
    }
}
