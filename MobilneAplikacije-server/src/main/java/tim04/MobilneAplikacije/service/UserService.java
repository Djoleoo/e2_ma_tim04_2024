package tim04.MobilneAplikacije.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tim04.MobilneAplikacije.model.User;
import tim04.MobilneAplikacije.repository.UserRepository;

import java.util.concurrent.ExecutionException;

@Service
public class UserService {

    @Autowired
    private UserRepository _userRepository;

    public User getByDocumentId(String id) throws ExecutionException, InterruptedException {
        return _userRepository.getUserByDocumentId(id);
    }

    public void updateUser(User user) {
        _userRepository.updateUser(user);
    }
}
