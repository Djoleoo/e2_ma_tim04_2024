package tim04.MobilneAplikacije.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import tim04.MobilneAplikacije.model.AccountConfirmationToken;
import tim04.MobilneAplikacije.model.User;
import tim04.MobilneAplikacije.repository.AccountConfirmationTokenRepository;
import tim04.MobilneAplikacije.repository.UserRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private UserRepository _userRepository;
    @Autowired
    private AccountConfirmationTokenRepository  _confirmationTokenRepository;
    @Autowired
    private Environment env;

    @Async
    public void sendEmail(User user) throws MailException, ExecutionException, InterruptedException {
        Optional<User> userOptional = Optional.ofNullable(user);
        if(userOptional.isPresent()) {
            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(user.getEmail());
            mail.setFrom(env.getProperty("spring.mail.username"));
            mail.setSubject("Complete Registration!");
            mail.setText("To confirm your account,please click here: " + "http://localhost:8080/api/auth/confirm-account?token=" + generateToken(userOptional.get()));
            javaMailSender.send(mail);
        }else{
            System.out.println("User not found for email: " + user.getEmail());
        }
    }

    private String generateToken(User user) throws ExecutionException, InterruptedException {
        AccountConfirmationToken confirmationToken = new AccountConfirmationToken(user);
        _confirmationTokenRepository.createToken(confirmationToken);
        return confirmationToken.getConfirmationToken();
    }

    public AccountConfirmationToken getConfirmationToken(String confirmationToken) throws ExecutionException, InterruptedException {
        AccountConfirmationToken token = _confirmationTokenRepository.getByToken(confirmationToken);

        if (token != null) {
            LocalDateTime issuedDateTime = token.getCreatedDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime currentDateTime = LocalDateTime.now();

            Duration duration = Duration.between(issuedDateTime, currentDateTime);
            long hoursSinceIssued = duration.toHours();

            if (hoursSinceIssued >= 24) {
                return null; // Ako je prošlo 24 sata, vraćamo null
            }
        }

        return token;
    }
}
