package tim04.MobilneAplikacije.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tim04.MobilneAplikacije.dto.RegistrationDto;
import tim04.MobilneAplikacije.mapper.UserMapper;
import tim04.MobilneAplikacije.model.User;
import tim04.MobilneAplikacije.repository.AuthRepository;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@Service
public class AuthService {

    @Autowired
    private AuthRepository _authRepository;

    @Autowired
    private  UserMapper _userMapper;

    public AuthService(UserMapper userMapper){
        this._userMapper = userMapper;
    }

    public User registerUser(RegistrationDto regDto) throws ExecutionException, InterruptedException, IOException {
        if(regDto.getPassword().equals(regDto.getConfirmPassword()) && regDto.mandatoryFieldCheck()){
            User user = _userMapper.registrationDtoToUser(regDto);
            return _authRepository.registerUser(user);
        } else {
            return null;
        }
    }

}
