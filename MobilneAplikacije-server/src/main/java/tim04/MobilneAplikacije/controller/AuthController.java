package tim04.MobilneAplikacije.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tim04.MobilneAplikacije.dto.RegistrationDto;
import tim04.MobilneAplikacije.model.AccountConfirmationToken;
import tim04.MobilneAplikacije.model.User;
import tim04.MobilneAplikacije.service.AuthService;
import tim04.MobilneAplikacije.service.EmailService;
import tim04.MobilneAplikacije.service.UserService;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@RestController
@CrossOrigin
@RequestMapping("/auth/")
public class AuthController {

    @Autowired
    public AuthService _authService;
    @Autowired
    public EmailService _emailService;
    @Autowired
    public UserService _userService;

    public AuthController(AuthService authService){
        this._authService = authService;
    }

    @PostMapping("/registerUser")
    public ResponseEntity<Void> registerUser(@RequestBody RegistrationDto regDto) throws ExecutionException, InterruptedException, IOException {
        User newUser = _authService.registerUser(regDto);
        if(newUser == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else if (newUser.getId() == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        _emailService.sendEmail(newUser);
        return new ResponseEntity<>( HttpStatus.OK);
    }

    @GetMapping(value = "/confirm-account")
    public ResponseEntity<String>confirmUserAccount(@RequestParam("token") String token) throws ExecutionException, InterruptedException {

        AccountConfirmationToken confirmationToken = _emailService.getConfirmationToken(token);
        if (token != null) {
            User user = _userService.getByDocumentId(confirmationToken.getUser().getId());
            if (user != null) {
                user.setEnabled(true);
                _userService.updateUser(user);
                return new ResponseEntity<>("Account successfully confirmed.", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("User not found for the provided token.", HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>("Invalid or expired token.", HttpStatus.BAD_REQUEST);
        }
    }



}
