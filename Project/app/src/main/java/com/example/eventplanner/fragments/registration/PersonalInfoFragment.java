package com.example.eventplanner.fragments.registration;

import android.app.Activity;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentPersonalInfoBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.LoginFragment;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.PUPVRegistration;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PersonalInfoFragment extends Fragment {

    private FragmentPersonalInfoBinding binding;
    private static final String USER_TYPE_PARAM = "userType";
    private static final String PUPVREGISTRATION = "pupvRegistration";
    private PUPVRegistration pupvRegistration;
    private String usertype;
    private String userImage;
    ImageView img;
    Uri imageUri;
    private ActivityResultLauncher<Intent> galleryLauncher;
    StorageReference storageReference;
    private FirebaseAuth auth;
    ExecutorService executor = Executors.newSingleThreadExecutor();

    public PersonalInfoFragment() {
        // Required empty public constructor
    }

    public static PersonalInfoFragment newInstance(String selectedRole,PUPVRegistration registration) {
        PersonalInfoFragment fragment = new PersonalInfoFragment();
        Bundle args = new Bundle();
        args.putString(USER_TYPE_PARAM,selectedRole);
        if(registration != null){
            args.putParcelable(PUPVREGISTRATION,registration);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usertype = getArguments().getString(USER_TYPE_PARAM);
            if(usertype.equals("PUP")){
                pupvRegistration = getArguments().getParcelable(PUPVREGISTRATION);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPersonalInfoBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();

        auth = FirebaseAuth.getInstance();
        EditText emailEditText = binding.userInfoEmail;
        EditText passwordEditText = binding.userInfoPassword;
        EditText confirmPasswordEditText = binding.passwordConfirmation;
        EditText nameEditText = binding.userInfoName;
        EditText lastNameEditText = binding.userInfoLastName;
        EditText addressEditText = binding.userInfoAddress;
        EditText phoneNumberEditText = binding.userInfoPhoneNumber;
        Button finishedButton = binding.finishBtn;

        img = binding.userPicture;
        registerResult();

        finishedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = passwordEditText.getText().toString();
                String confirmPassword = confirmPasswordEditText.getText().toString();
                String email = emailEditText.getText().toString();
                if(!password.equals(confirmPassword)){
                    Toast.makeText(requireContext(),"Your passwords dont match!",Toast.LENGTH_SHORT).show();
                }else if(password.isEmpty()) {
                    passwordEditText.setError("Password cannot be empty");
                }else if(email.isEmpty()){
                    emailEditText.setError("Email cannot be empty!");
                }else {
                    String name = nameEditText.getText().toString();
                    String lastName = lastNameEditText.getText().toString();
                    String address = addressEditText.getText().toString();
                    String phoneNumber = phoneNumberEditText.getText().toString();
                    User user  = new User(name, lastName, email, password,phoneNumber,address, User.UserType.valueOf(usertype),userImage,null);
                    if(pupvRegistration != null && usertype.equals("PUP")){
                        showPupVConfirmationDialog(user);
                    }else{
                        showConfirmationDialog(user);
                    }
                }
            }
        });

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryLauncher.launch(galleryIntent); // Pokreće galeriju
            }
        });

        return root;
    }

    private void showConfirmationDialog(User user){
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setTitle("Confirm Registration");
        builder.setMessage("Are you sure you want to finish registration?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(imageUri != null){
                    String imageName = "user_image_" + System.currentTimeMillis();
                    user.setUserImage(imageName);
                }
                auth.createUserWithEmailAndPassword(user.getEmail(),user.getPassword())
                        .addOnSuccessListener(authResult -> {
                            FirebaseUser firebaseUser = authResult.getUser();
                            String userId = firebaseUser.getUid(); // Dobijanje ID-ja korisnika iz Firebase Authentication
                            FirebaseFirestore.getInstance().collection("users").document(userId)
                                    .set(user)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void unused) {
                                            Toast.makeText(requireContext(),"Registration successful",Toast.LENGTH_SHORT).show();
                                            executor.execute(()->{
                                                if(user.getUserImage() != null){
                                                    uploadImage(user.getUserImage());
                                                }
                                                auth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful()){
                                                            Toast.makeText(requireContext(),"Email sent",Toast.LENGTH_SHORT).show();
                                                        }
                                                        else {
                                                            Toast.makeText(requireContext(),"Email sending failed!",Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            });
                                            FragmentTransition.to(LoginFragment.newInstance(),getActivity(),true,R.id.loginCard);
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(requireContext(),"Greska priliko rada sam firestore-om",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        })
                        .addOnFailureListener(e -> {
                            Toast.makeText(requireContext(),"Greska priliko rada sa firebaseauth",Toast.LENGTH_SHORT).show();
                        });

            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void showPupVConfirmationDialog(User user){
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setTitle("Confirm PUPV Registration");
        builder.setMessage("Are you sure you want to send this registration request?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(imageUri != null){
                    //String imageName = "user_image_" + System.currentTimeMillis();
                    user.setUserImage(imageUri.toString());
                }
                pupvRegistration.setCreationTime(new Date());
                pupvRegistration.setUser(user);
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("pupvRegistrationRequests").add(pupvRegistration)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                executor.execute(()->{
                                    Notification notification = new Notification("PUPV Registration", "New PUPV registration request", "PbEmjK2rj9YnSQeBmC9K98o6WSw1", false, Notification.UserType.ADMIN);
                                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                                    db.collection("notifications").add(notification);
                                });
                                Toast.makeText(requireContext(),"Pupv registracija poslata",Toast.LENGTH_SHORT).show();
                                FragmentTransition.to(LoginFragment.newInstance(),getActivity(),true,R.id.loginCard);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(requireContext(),"Greska prilikom registracije pupv",Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void registerResult(){
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                try{
                    imageUri = result.getData().getData();
                    img.setImageURI(imageUri);
                }catch (Exception e){
                    Toast.makeText(getActivity(),"NO image selected" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void uploadImage(String imageName){
        storageReference = FirebaseStorage.getInstance().getReference("images/" + imageName);
        storageReference.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(getActivity(),"Uploaded the file",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(),"Popusih",Toast.LENGTH_SHORT).show();
                    }
                });
    }


}