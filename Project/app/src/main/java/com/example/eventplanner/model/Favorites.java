package com.example.eventplanner.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Favorites {

    private String id;
    private User user;
    private List<Product> favoriteProducts;
    private List<Service> favoriteServices;
    private List<Package> favoritePackages;

    public Favorites() {
        this.favoriteProducts = new ArrayList<>();
        this.favoriteServices = new ArrayList<>();
        this.favoritePackages = new ArrayList<>();
    }

    public Favorites(String id, User user) {
        this.id = id;
        this.user = user;
        this.favoriteProducts = new ArrayList<>();
        this.favoriteServices = new ArrayList<>();
        this.favoritePackages = new ArrayList<>();
    }

    // Getters and Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Product> getFavoriteProducts() {
        return favoriteProducts;
    }

    public void setFavoriteProducts(List<Product> favoriteProducts) {
        this.favoriteProducts = favoriteProducts;
    }

    public List<Service> getFavoriteServices() {
        return favoriteServices;
    }

    public void setFavoriteServices(List<Service> favoriteServices) {
        this.favoriteServices = favoriteServices;
    }

    public List<Package> getFavoritePackages() {
        return favoritePackages;
    }

    public void setFavoritePackages(List<Package> favoritePackages) {
        this.favoritePackages = favoritePackages;
    }

    // Methods to add and remove favorites
    public void addFavoriteProduct(Product product) {
        if (!favoriteProducts.contains(product)) {
            favoriteProducts.add(product);
        }
    }

    public void removeFavoriteProduct(Product product) {
        favoriteProducts.remove(product);
    }

    public void addFavoriteService(Service service) {
        if (!favoriteServices.contains(service)) {
            favoriteServices.add(service);
        }
    }

    public void removeFavoriteService(Service service) {
        favoriteServices.remove(service);
    }

    public void addFavoritePackage(Package packageItem) {
        if (!favoritePackages.contains(packageItem)) {
            favoritePackages.add(packageItem);
        }
    }

    public void removeFavoritePackage(Package packageItem) {
        favoritePackages.remove(packageItem);
    }

    // Methods to remove by ID
    public void removeFavoriteProductById(String productId) {
        Iterator<Product> iterator = favoriteProducts.iterator();
        while (iterator.hasNext()) {
            Product product = iterator.next();
            if (product.getId().equals(productId)) {
                iterator.remove();
                break;
            }
        }
    }

    public void removeFavoriteServiceById(String serviceId) {
        Iterator<Service> iterator = favoriteServices.iterator();
        while (iterator.hasNext()) {
            Service service = iterator.next();
            if (service.getId().equals(serviceId)) {
                iterator.remove();
                break;
            }
        }
    }

    public void removeFavoritePackageById(String packageId) {
        Iterator<Package> iterator = favoritePackages.iterator();
        while (iterator.hasNext()) {
            Package packageItem = iterator.next();
            if (packageItem.getId().equals(packageId)) {
                iterator.remove();
                break;
            }
        }
    }
}
