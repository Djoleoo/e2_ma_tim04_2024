package com.example.eventplanner.fragments.products;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner.adapters.CategoryListAdapter;
import com.example.eventplanner.adapters.PackageListAdapter;
import com.example.eventplanner.adapters.ProductListAdapter;
import com.example.eventplanner.databinding.FragmentCategoriesBinding;
import com.example.eventplanner.databinding.FragmentPackagesBinding;
import com.example.eventplanner.databinding.FragmentProductsBinding;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Package;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class PackagesFragment extends ListFragment {

    private PackageListAdapter adapter;
    private ArrayList<DocumentSnapshot> mPackage;

    private FragmentPackagesBinding binding;
    private static final String ARG_PARAM = "param";

    private String mParam1;
    private String mParam2;

    public PackagesFragment() {
        // Required empty public constructor
    }

    public static PackagesFragment newInstance(ArrayList<DocumentSnapshot> packages) {
        PackagesFragment fragment = new PackagesFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, packages);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPackage = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new PackageListAdapter(getActivity(), mPackage);
            //setListAdapter(adapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPackagesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}