package com.example.eventplanner.fragments.employeeManagement;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEmployeeInfoBinding;
import com.example.eventplanner.model.Availability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.Calendar;
import java.util.Locale;

public class EmployeeInfoFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private FirebaseFirestore db;
    private FragmentEmployeeInfoBinding binding;

    private String userName;
    private String userEmail;
    private String workingHours;
    private String address;
    private String phoneNumber;
    private String services;
    private Availability availability;  // Availability object
    private String currentUserType;
    private String currentUserCompanyId;
    private boolean enabled;  // Field to track the enabled state of the user

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = auth.getCurrentUser();

    public EmployeeInfoFragment() {
        // Required empty public constructor
    }

    public static EmployeeInfoFragment newInstance(String param1, String param2) {
        EmployeeInfoFragment fragment = new EmployeeInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeInfoBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        checkLoggedInUser(() -> {
            if (mParam1 != null && !mParam1.isEmpty()) {
                searchUser(mParam1);
            }
        });

        // Set up button listeners for activate and deactivate
        binding.buttonActivate.setOnClickListener(v -> {
            updateUserActiveStatus(true);  // Set 'enabled' to true
        });

        binding.buttonDeactivate.setOnClickListener(v -> {
            updateUserActiveStatus(false); // Set 'enabled' to false
        });

        // Set up button listener for changing working hours
        binding.buttonChangeWorkingHours.setOnClickListener(v -> {
            showChangeWorkingHoursDialog();
        });

        return root;
    }


    private void checkLoggedInUser(FirestoreCallback firestoreCallback) {
        if (currentUser != null) {
            String uid = currentUser.getUid();

            // Access Firestore
            db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("users").document(uid);

            docRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    currentUserType = documentSnapshot.getString("userType");
                    currentUserCompanyId = documentSnapshot.getString("companyId");

                    System.out.println("Current User Type: " + currentUserType);
                    System.out.println("Current User Company ID: " + currentUserCompanyId);

                    // Trigger the callback after data is fetched
                    firestoreCallback.onCallback();
                } else {
                    System.out.println("No such document exists!");
                }
            }).addOnFailureListener(e -> {
                System.out.println("Error retrieving document: " + e.getMessage());
            });
        } else {
            System.out.println("No user is currently logged in.");
        }
    }

    private void searchUser(String searchText) {
        if (currentUserType != null) {
            db.collection("users")
                    .whereEqualTo("email", searchText)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    String userCompanyId = document.getString("companyId");
                                    String userId = document.getId(); // Get the user ID

                                    if (currentUserType.equals("PUPV") || (currentUserType.equals("PUPZ") && currentUserCompanyId.equals(userCompanyId))) {
                                        userName = document.getString("firstName") + " " + document.getString("lastName");
                                        userEmail = document.getString("email");
                                        address = document.getString("address");
                                        phoneNumber = document.getString("phoneNumber");
                                        enabled = document.getBoolean("enabled");  // Get the enabled status

                                        // Fetch availability using the user ID
                                        fetchAvailability(userEmail);
                                    } else {
                                        handleNoMatchFound();
                                    }
                                }
                            } else {
                                handleNoMatchFound();
                            }
                        } else {
                            Toast.makeText(getContext(), "Error getting documents: " + task.getException(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }


    private void handleNoMatchFound() {
        binding.textViewName.setText("User not found!");

        // Hide all other text views
        binding.textViewEmail.setVisibility(View.GONE);
        binding.textViewWorkingHours.setVisibility(View.GONE);
        binding.textViewAddress.setVisibility(View.GONE);
        binding.textViewPhone.setVisibility(View.GONE);
        binding.textViewServices.setVisibility(View.GONE);
        binding.textViewAvailability.setVisibility(View.GONE);

        // Hide titles
        binding.textViewNameTitle.setVisibility(View.GONE);
        binding.textViewEmailTitle.setVisibility(View.GONE);
        binding.textViewWorkingHoursTitle.setVisibility(View.GONE);
        binding.textViewAddressTitle.setVisibility(View.GONE);
        binding.textViewPhoneTitle.setVisibility(View.GONE);
        binding.textViewServicesTitle.setVisibility(View.GONE);
        binding.textViewAvailabilityTitle.setVisibility(View.GONE);

        // Hide buttons
        binding.buttonActivate.setVisibility(View.GONE);
        binding.buttonDeactivate.setVisibility(View.GONE);
        binding.buttonChangeWorkingHours.setVisibility(View.GONE);
    }

    private void displayUserDetails() {
        binding.textViewName.setText(userName);
        binding.textViewEmail.setVisibility(View.VISIBLE);
        binding.textViewEmail.setText(userEmail);
        binding.textViewAddress.setVisibility(View.VISIBLE);
        binding.textViewAddress.setText(address);
        binding.textViewPhone.setVisibility(View.VISIBLE);
        binding.textViewPhone.setText(phoneNumber);

        // Update the Availability section with start date and end date
        if (availability != null) {
            binding.textViewAvailability.setVisibility(View.VISIBLE);
            binding.textViewAvailability.setText(
                    String.format(Locale.getDefault(),
                            "Start Date: %s\nEnd Date: %s",
                            availability.getStartDate(), availability.getEndDate()));
        } else {
            binding.textViewAvailability.setVisibility(View.GONE);
        }

        // Update the Working Hours section with hours from Monday to Sunday
        if (availability != null) {
            String workingHours = String.format(Locale.getDefault(),
                    "Mon: %s-%s\nTue: %s-%s\nWed: %s-%s\nThu: %s-%s\nFri: %s-%s\nSat: %s-%s\nSun: %s-%s",
                    availability.getMondayStartTime(), availability.getMondayEndTime(),
                    availability.getTuesdayStartTime(), availability.getTuesdayEndTime(),
                    availability.getWednesdayStartTime(), availability.getWednesdayEndTime(),
                    availability.getThursdayStartTime(), availability.getThursdayEndTime(),
                    availability.getFridayStartTime(), availability.getFridayEndTime(),
                    availability.getSaturdayStartTime(), availability.getSaturdayEndTime(),
                    availability.getSundayStartTime(), availability.getSundayEndTime());

            binding.textViewWorkingHours.setVisibility(View.VISIBLE);
            binding.textViewWorkingHours.setText(workingHours);
        } else {
            binding.textViewWorkingHours.setVisibility(View.GONE);
        }

        // Update button visibility based on the enabled status
        if (enabled) {
            binding.buttonActivate.setVisibility(View.GONE);
            binding.buttonDeactivate.setVisibility(View.VISIBLE);
        } else {
            binding.buttonActivate.setVisibility(View.VISIBLE);
            binding.buttonDeactivate.setVisibility(View.GONE);
        }
    }


    private void fetchAvailability(String userId) {
        db.collection("Availability")
                .whereEqualTo("userId", userId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            availability = document.toObject(Availability.class);
                            displayUserDetails();  // Update UI after fetching availability
                        }
                    } else {
                        Toast.makeText(getContext(), "Availability not found", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateUserActiveStatus(boolean isActive) {
        if (userEmail != null) {
            db.collection("users")
                    .whereEqualTo("email", userEmail)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful() && !task.getResult().isEmpty()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                db.collection("users")
                                        .document(document.getId())
                                        .update("enabled", isActive)
                                        .addOnSuccessListener(aVoid -> {
                                            enabled = isActive;  // Update the local enabled status
                                            displayUserDetails();  // Refresh the UI
                                            Toast.makeText(getContext(), "User status updated: " + (isActive ? "Activated" : "Deactivated"), Toast.LENGTH_SHORT).show();
                                        })
                                        .addOnFailureListener(e -> {
                                            Toast.makeText(getContext(), "Error updating user status", Toast.LENGTH_SHORT).show();
                                        });
                            }
                        } else {
                            Toast.makeText(getContext(), "User not found", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }


    private void showChangeWorkingHoursDialog() {
        // Inflate the dialog layout
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_change_working_hours, null);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder.setView(dialogView);

        EditText editTextStartDate = dialogView.findViewById(R.id.editTextStartDate);
        EditText editTextEndDate = dialogView.findViewById(R.id.editTextEndDate);
        EditText editTextMonday = dialogView.findViewById(R.id.editTextMonday);
        EditText editTextTuesday = dialogView.findViewById(R.id.editTextTuesday);
        EditText editTextWednesday = dialogView.findViewById(R.id.editTextWednesday);
        EditText editTextThursday = dialogView.findViewById(R.id.editTextThursday);
        EditText editTextFriday = dialogView.findViewById(R.id.editTextFriday);
        EditText editTextSaturday = dialogView.findViewById(R.id.editTextSaturday);
        EditText editTextSunday = dialogView.findViewById(R.id.editTextSunday);

        // Pre-fill current values if available
        if (availability != null) {
            editTextStartDate.setText(availability.getStartDate());
            editTextEndDate.setText(availability.getEndDate());
            editTextMonday.setText(availability.getMondayStartTime() + "-" + availability.getMondayEndTime());
            editTextTuesday.setText(availability.getTuesdayStartTime() + "-" + availability.getTuesdayEndTime());
            editTextWednesday.setText(availability.getWednesdayStartTime() + "-" + availability.getWednesdayEndTime());
            editTextThursday.setText(availability.getThursdayStartTime() + "-" + availability.getThursdayEndTime());
            editTextFriday.setText(availability.getFridayStartTime() + "-" + availability.getFridayEndTime());
            editTextSaturday.setText(availability.getSaturdayStartTime() + "-" + availability.getSaturdayEndTime());
            editTextSunday.setText(availability.getSundayStartTime() + "-" + availability.getSundayEndTime());
        }

        dialogBuilder.setPositiveButton("Save", (dialog, which) -> {
            // Parse the input and update availability
            availability.setStartDate(editTextStartDate.getText().toString());
            availability.setEndDate(editTextEndDate.getText().toString());

            availability.setMondayStartTime(editTextMonday.getText().toString().split("-")[0]);
            availability.setMondayEndTime(editTextMonday.getText().toString().split("-")[1]);

            availability.setTuesdayStartTime(editTextTuesday.getText().toString().split("-")[0]);
            availability.setTuesdayEndTime(editTextTuesday.getText().toString().split("-")[1]);

            availability.setWednesdayStartTime(editTextWednesday.getText().toString().split("-")[0]);
            availability.setWednesdayEndTime(editTextWednesday.getText().toString().split("-")[1]);

            availability.setThursdayStartTime(editTextThursday.getText().toString().split("-")[0]);
            availability.setThursdayEndTime(editTextThursday.getText().toString().split("-")[1]);

            availability.setFridayStartTime(editTextFriday.getText().toString().split("-")[0]);
            availability.setFridayEndTime(editTextFriday.getText().toString().split("-")[1]);

            availability.setSaturdayStartTime(editTextSaturday.getText().toString().split("-")[0]);
            availability.setSaturdayEndTime(editTextSaturday.getText().toString().split("-")[1]);

            availability.setSundayStartTime(editTextSunday.getText().toString().split("-")[0]);
            availability.setSundayEndTime(editTextSunday.getText().toString().split("-")[1]);

            // Save updated availability to Firestore
            updateAvailability();
        });

        dialogBuilder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }


    private void updateAvailability() {
        db.collection("Availability")
                .whereEqualTo("userId", userEmail)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            db.collection("Availability")
                                    .document(document.getId())
                                    .set(availability)
                                    .addOnSuccessListener(aVoid -> {
                                        displayUserDetails();
                                        Toast.makeText(getContext(), "Availability updated successfully!", Toast.LENGTH_SHORT).show();
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(getContext(), "Error updating availability", Toast.LENGTH_SHORT).show();
                                    });
                        }
                    } else {
                        Toast.makeText(getContext(), "Availability not found", Toast.LENGTH_SHORT).show();
                    }
                });
    }



    private void updateWorkingHours(String newWorkingHours) {
        this.workingHours = newWorkingHours;
        displayUserDetails();
        Toast.makeText(getContext(), "Working Hours Updated: " + newWorkingHours, Toast.LENGTH_SHORT).show();
    }

    // FirestoreCallback interface for callback after checking logged-in user
    public interface FirestoreCallback {
        void onCallback();
    }
}
