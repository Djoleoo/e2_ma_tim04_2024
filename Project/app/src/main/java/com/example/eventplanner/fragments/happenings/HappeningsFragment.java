package com.example.eventplanner.fragments.happenings;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner.adapters.CategoryListAdapter;
import com.example.eventplanner.adapters.HappeningListAdapter;
import com.example.eventplanner.adapters.ProductListAdapter;
import com.example.eventplanner.databinding.FragmentCategoriesBinding;
import com.example.eventplanner.databinding.FragmentHappeningsBinding;
import com.example.eventplanner.databinding.FragmentProductsBinding;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Happening;
import com.example.eventplanner.model.Product;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class HappeningsFragment extends ListFragment {

    private HappeningListAdapter adapter;
    private ArrayList<DocumentSnapshot> mHappenings = new ArrayList<>();

    private FragmentHappeningsBinding binding;
    private static final String ARG_PARAM = "param";

    private String mParam1;
    private String mParam2;

    public HappeningsFragment() {
        // Required empty public constructor
    }

    public static HappeningsFragment newInstance(ArrayList<DocumentSnapshot> happenings) {
        HappeningsFragment fragment = new HappeningsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, happenings);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mHappenings = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new HappeningListAdapter(getActivity(), mHappenings);
            //setListAdapter(adapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentHappeningsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}