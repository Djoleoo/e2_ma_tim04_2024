package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.SingleCategoryFragment;
import com.example.eventplanner.fragments.categories.SingleSuggestionFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.SubCategorySuggestion;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.lang.annotation.Documented;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SuggestionsListAdapter extends ArrayAdapter<DocumentSnapshot> {

    private ArrayList<DocumentSnapshot> aSuggestion;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    private FragmentActivity mContext;

    public SuggestionsListAdapter(FragmentActivity context , ArrayList<DocumentSnapshot> suggestions){
        super(context, R.layout.suggestions_list_item);
        mContext = context;
        aSuggestion = suggestions;
    }

    @Override
    public int getCount() {
        return aSuggestion.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aSuggestion.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.suggestions_list_item,
                    parent, false);
        }
        LinearLayout suggestionItem = convertView.findViewById(R.id.suggestion_list_item);
        TextView subCategoryName = convertView.findViewById(R.id.suggestion_name);
        TextView subCategoryUser = convertView.findViewById(R.id.suggestion_username);
        TextView subCategoryType = convertView.findViewById(R.id.suggestion_type);
        Button acceptBtn = convertView.findViewById(R.id.accept_button);

        Button details = convertView.findViewById(R.id.details_button);
        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubCategorySuggestion suggestion = document.toObject(SubCategorySuggestion.class);
                FragmentTransition.to(SingleSuggestionFragment.newInstance(suggestion,document.getId()),mContext,true,R.id.list_layout);
            }
        });

        if(document != null){

            subCategoryName.setText(document.getString("subCategoryName"));
            subCategoryUser.setText(document.getString("senderEmail"));
            String statusText = document.getString("status");
            if (statusText != null) {
                try {
                    SubCategory.Status status = SubCategory.Status.valueOf(statusText.toUpperCase());
                    subCategoryType.setText(status.toString());
                } catch (IllegalArgumentException e) {
                    // Ako nema odgovarajuće enum konstante, postavljamo neku podrazumevanu vrednost
                    subCategoryType.setText("Unknown");
                }
            } else {
                subCategoryType.setText("Unknown");
            }
        }

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubCategorySuggestion subCategorySuggestion = document.toObject(SubCategorySuggestion.class);
                showAcceptDialog(subCategorySuggestion,document.getId(),subCategorySuggestion.getUserId());
            }
        });
        return convertView;
    }

    private void showAcceptDialog(final SubCategorySuggestion suggestion,String id,String senderId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Accept suggestion");
        builder.setMessage("Are you sure you want to accept this suggestion?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SubCategory newSubCategory = new SubCategory(suggestion.getsubCategoryName(),suggestion.getDescription(),suggestion.getCategory(),suggestion.getStatus());
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("subCategories").add(newSubCategory)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                executor.execute(() -> {
                                    Notification notification = new Notification("Suggestion accepted!","You suggestion for a new subCategory has been accepted!",senderId,false, Notification.UserType.PUP);
                                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                                    db.collection("subCategoriesSuggestions").document(id).delete();
                                    db.collection("notifications").add(notification);
                                });
                                aSuggestion.remove(suggestion);
                                notifyDataSetChanged();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
