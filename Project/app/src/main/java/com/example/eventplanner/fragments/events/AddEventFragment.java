package com.example.eventplanner.fragments.events;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentAddEventBinding;
import com.example.eventplanner.databinding.FragmentEventsBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AddEventFragment extends Fragment {

    private FragmentAddEventBinding binding;
    private ArrayList<SubCategory> selectedSubcategories = new ArrayList<>();
    private ArrayList<SubCategory> allSubcategories = new ArrayList<>();
    private ArrayList<String> chosenSubCategoriNames = new ArrayList<>();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ExecutorService executor = Executors.newSingleThreadExecutor();

    private String mParam1;
    private String mParam2;

    public AddEventFragment() {
        // Required empty public constructor
    }


    public static AddEventFragment newInstance() {
        AddEventFragment fragment = new AddEventFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAddEventBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        EditText eventName = binding.eventName;
        TextView eventDescription = binding.descriptionTextview;
        Button addSubcategory = binding.addSubcategoryButton;
        Button createEventType = binding.createCategoryBtn;

        selectSubCategories();
        addSubcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!allSubcategories.isEmpty()) {
                    allSubcategories.removeAll(selectedSubcategories);
                }
                showSubcategoryDialog(allSubcategories);
            }
        });

        createEventType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = eventName.getText().toString();
                String description = eventDescription.getText().toString();
                Event event = new Event(name,description,selectedSubcategories,false);

                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("eventTypes").add(event)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                String documentId = documentReference.getId();
                                executor.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        db.collection("eventTypes").document(documentId)
                                                .update("id", documentId);
                                    }
                                });
                                FragmentTransition.to(EventPageFragment.newInstance(),getActivity(),true,R.id.list_layout);
                                Toast.makeText(getActivity(), "New Event type created!", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Nesto je puklo bajo!", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        return root;
    }

    private void showSubcategoryDialog(ArrayList<SubCategory> subcategories) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_subcategory, null);
        builder.setView(dialogView);

        ArrayList<String>subCategorNames= getSubCategoryNames(subcategories);

        // Initialize Spinner
        Spinner subcategorySpinner = dialogView.findViewById(R.id.subcategory_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(),
                android.R.layout.simple_spinner_item,
                subCategorNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subcategorySpinner.setAdapter(adapter);

        builder.setPositiveButton("Add", (dialog, which) -> {
            String selectedSubcategory = (String) subcategorySpinner.getSelectedItem();
            SubCategory subCategory = null;
            for(SubCategory subCategory1 : subcategories){
                if(subCategory1.getName().equals(selectedSubcategory)){
                    subCategory = subCategory1;
                    break;
                }
            }
            addSelectedSubcategory(subCategory);
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void addSelectedSubcategory(SubCategory addedSubCategory) {
        selectedSubcategories.add(addedSubCategory); // Dodajemo novu subkategoriju u listu
        chosenSubCategoriNames = getSubCategoryNames(selectedSubcategories);
        // Pronalazimo ListView
        ListView selectedSubcategoriesListView = requireView().findViewById(R.id.subcategories_listview);

        // Ako je ListView bio nevidljiv, sada ga prikazujemo
        selectedSubcategoriesListView.setVisibility(View.VISIBLE);
        ArrayAdapter<String> selectedSubcategoriesAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, chosenSubCategoriNames);
        selectedSubcategoriesListView.setAdapter(selectedSubcategoriesAdapter);

    }

    private void selectSubCategories() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("subCategories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            if(task.isSuccessful()){
                                allSubcategories.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    SubCategory subCategory = document.toObject(SubCategory.class);
                                    allSubcategories.add(subCategory);
                                }
                            }
                        }
                    }
                });
    }
    private ArrayList<String> getSubCategoryNames(ArrayList<SubCategory> list) {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (SubCategory subCategory : list) {
            categoryNames.add(subCategory.getName());
        }
        return categoryNames;
    }
}