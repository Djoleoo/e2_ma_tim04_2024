package com.example.eventplanner.fragments.products;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentAddServiceBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class AddServiceFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private FragmentAddServiceBinding binding;

    private ArrayList<Category> categories = new ArrayList<>();
    private ArrayList<SubCategory> subCategories= new ArrayList<>();
    private ArrayList<Event> eventTypes = new ArrayList<>();
    private ArrayList<Integer> selectedImages = new ArrayList<>();
    private Category selectedCategory = new Category();


    private String mParam1;
    private String mParam2;

    public AddServiceFragment() {
        // Required empty public constructor
    }


    public static AddServiceFragment newInstance() {
        AddServiceFragment fragment = new AddServiceFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        binding = FragmentAddServiceBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        Spinner categorySpinner=binding.categoryBtnService;
        Spinner subCategorySpinner=binding.subcategoryBtnService;
        Button addImage=binding.addImageButton;
        Spinner eventTypeSpinner=binding.eventTypeBtnService;
        Spinner personsSpinner=binding.personsBtnService;
        EditText serviceName=binding.serviceName;
        EditText serviceDescription= binding.serviceDescription;
        EditText serviceSpecify= binding.serviceSpecificity;
        EditText serviceDiscount=binding.serviceDiscount;
        EditText servicePrice=binding.servicePricePerHour;
        EditText serviceHours=binding.serviceHours;
        EditText serviceDeadlineReservations=binding.serviceDeadlineReservations;
        EditText serviceDeadlineCancel=binding.serviceDeadlineCancel;
        CheckBox serviceVisibleCheckbox =binding.serviceVisibleCheckbox;
        CheckBox serviceAutomaticAcceptanceCheckbox= binding.serviceAutomaticAcceptanceCheckbox;
        CheckBox serviceAvailableCheckbox=binding.serviceAvailableCheckbox;
        EditText serviceLocation=binding.serviceLocation;


        /*ArrayList<Category> categories = new ArrayList<>();
        Category fotoCategory=new Category(1L,"Foto","cap");
        Category restaurantCategory=new Category(2L,"Ugostiteljstvo","cap");
        categories.add(fotoCategory);
        categories.add(restaurantCategory);


        ArrayAdapter<Category> adapterCategory = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapterCategory);

        ArrayList<SubCategory> subCategories=new ArrayList<>();
        SubCategory fotoalbum=new SubCategory("fotoalbum","lol",fotoCategory,SubCategory.Status.PRODUCT);
        SubCategory ketering= new SubCategory("ketering","cap",restaurantCategory,SubCategory.Status.SERVICE);
        subCategories.add(fotoalbum);
        subCategories.add(ketering);

        ArrayAdapter<SubCategory> adapterSubCategory = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subCategories);
        adapterSubCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subCategorySpinner.setAdapter(adapterSubCategory);*/

        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.album);
        images.add(R.drawable.camera);
        images.add(R.drawable.photographing);

        ArrayList<Event> events=new ArrayList<>();
        /*Event svadba=new Event("Svadba","svadba",subCategories,false);
        Event rodjendan= new Event("Rodjendan","cap",subCategories,false);
        events.add(svadba);
        events.add(rodjendan);

        ArrayAdapter<Event> adapterEvent = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, events);
        adapterEvent.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventTypeSpinner.setAdapter(adapterEvent);*/

        ArrayList<String> persons1 = new ArrayList<>();
        persons1.add("Marko");
        persons1.add("Aleksandar");
        persons1.add("Djorjde");
        persons1.add("Antonije");

        ArrayAdapter<String> adapterPersons = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, persons1);
        adapterPersons.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        personsSpinner.setAdapter(adapterPersons);

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageDialog(images);
            }
        });

        selectCategories(new AddProductFragment.OnCategoriesLoadedListener() {
            @Override
            public void onCategoriesLoaded() {
                // Kreiraj adapter nakon što su kategorije učitane
                setupSpinner();
            }
        });

        selectSubCategories(new AddProductFragment.OnSubCategoriesLoadedListener() {
            @Override
            public void onSubCategoriesLoaded() {
                setupSubCategorySpinner();
            }
        });

        selectEventType(new AddProductFragment.OnEventTypeLoadedListener() {
            @Override
            public void onEventTypesLoaded() {
                setupEventTypeSpinner();
            }
        });

        //watch how to add new subcategory
        /*categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategory = (Category) parent.getSelectedItem();
                ArrayList<SubCategory> filteredSubCategories = filterSubCategoriesByCategory(selectedCategory);
                ArrayAdapter<SubCategory> adapterSubCategory = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, filteredSubCategories);
                adapterSubCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                subCategorySpinner.setAdapter(adapterSubCategory);




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle when nothing is selected
            }

            private ArrayList<SubCategory> filterSubCategoriesByCategory(Category category) {
                ArrayList<SubCategory> filteredSubCategories = new ArrayList<>();
                for (SubCategory subCategory : subCategories) {
                    if (subCategory.getCategory().equals(category)) {
                        filteredSubCategories.add(subCategory);
                    }
                }
                filteredSubCategories.add(new SubCategory( "Add New Subcategory", "", category, SubCategory.Status.PRODUCT));
                return filteredSubCategories;
            }
        });*/

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCategoryName = (String) parent.getSelectedItem();
                //Category selectedCategory = findCategoryByName(selectedCategoryName);
                if (!selectedCategoryName.equals("")) {
                    ArrayList<SubCategory> filteredSubCategories = filterSubCategoriesByCategory(selectedCategoryName);
                    ArrayAdapter<String> subCategoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                            getSubCategoryNames(filteredSubCategories));
                    subCategoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    subCategorySpinner.setAdapter(subCategoryArrayAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle when nothing is selected
            }
        });

        Button button = binding.createProductBtn;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = serviceName.getText().toString();
                String description = serviceDescription.getText().toString();
                String specificity = serviceSpecify.getText().toString();
                String location=serviceLocation.getText().toString();
                double price=Double.parseDouble(servicePrice.getText().toString());
                double discount=Double.parseDouble(serviceDiscount.getText().toString());
                Spinner categorySpinner = binding.categoryBtnService;
                Spinner subCategorySpinner=binding.subcategoryBtnService;
                int deadlineReservation=Integer.parseInt(serviceDeadlineReservations.getText().toString());
                int deadlineCancel=Integer.parseInt(serviceDeadlineCancel.getText().toString());
                int hours=Integer.parseInt(serviceHours.getText().toString());
                boolean available=serviceAvailableCheckbox.isChecked();
                boolean visible=serviceVisibleCheckbox.isChecked();
                boolean acceptance=serviceAutomaticAcceptanceCheckbox.isChecked();

                String selectedCategoryName = (String) categorySpinner.getSelectedItem();
                String selectedSubCategoryName= (String) subCategorySpinner.getSelectedItem();
                String selectedEventTypeName= (String) eventTypeSpinner.getSelectedItem();

                Category selectedCategory = null;
                for (Category category : categories) {
                    if (category.getName().equals(selectedCategoryName)) {
                        selectedCategory = category;
                        break;
                    }
                }
                SubCategory selectedSubCategory=null;
                for(SubCategory subCategory:subCategories){
                    if(subCategory.getName().equals(selectedSubCategoryName)){
                        selectedSubCategory=subCategory;
                        break;
                    }
                }
                Event selectedEventType = null;
                for(Event event:eventTypes){
                    if(event.getName().equals(selectedEventTypeName)){
                        selectedEventType=event;
                        events.add(selectedEventType);
                        break;
                    }
                }
                if (selectedCategory!=null && selectedSubCategory!=null  && selectedEventType!=null) {

                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                    if (currentUser != null) {
                        Category finalSelectedCategory = selectedCategory;
                        SubCategory finalSelectedSubCategory = selectedSubCategory;
                        findCompanyByUserEmail(currentUser.getEmail(), new AddProductFragment.OnCompanyLoadedListener() {
                            @Override
                            public void onCompanyLoaded(Company company) {
                                Service service = new Service(finalSelectedCategory,finalSelectedSubCategory,name,
                                        description,images, specificity,price,hours,
                                        location,discount,persons1,events,
                                        deadlineReservation,deadlineCancel,acceptance,
                                        available,visible,currentUser.getEmail(), company);
                                showConfirmationDialog(service);
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "User not logged in", Toast.LENGTH_SHORT).show();
                    }



                } else {
                    // Ako nije pronađena odgovarajuća kategorija
                    Toast.makeText(getActivity(), "Selected category not found", Toast.LENGTH_SHORT).show();
                }
            }
        });



        return root;
    }



    private void showImageDialog(ArrayList<Integer> images) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_image, null);
        builder.setView(dialogView);

        // Initialize Spinner
        Spinner imageSpinner = dialogView.findViewById(R.id.image_spinner);
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, images);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        imageSpinner.setAdapter(adapter);

        builder.setPositiveButton("Add", (dialog, which) -> {
            Integer selectedSubcategory = (Integer) imageSpinner.getSelectedItem();
            if (selectedImages != null) {
                addSelectedImage(selectedSubcategory);
            }
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void addSelectedImage(Integer selectedImage) {
        selectedImages.add(selectedImage); // Dodajemo novu subkategoriju u listu

        // Pronalazimo ListView
        ListView selectedImagesListView = requireView().findViewById(R.id.images_listview);

        // Ako je ListView bio nevidljiv, sada ga prikazujemo
        selectedImagesListView.setVisibility(View.VISIBLE);

        // Ako već nemamo adapter, kreiramo novi i postavljamo ga na ListView
        if (selectedImagesListView.getAdapter() == null) {
            ArrayAdapter<Integer> selectedImagesAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, selectedImages);
            selectedImagesListView.setAdapter(selectedImagesAdapter);
            Log.d("dodate slika",selectedImages.toString());
        } else { // Ako već imamo adapter, samo ga ažuriramo
            ArrayAdapter<Integer> selectedImagesAdapter = (ArrayAdapter<Integer>) selectedImagesListView.getAdapter();
            selectedImagesAdapter.notifyDataSetChanged();
            Log.d("dodate slike",selectedImages.toString());

        }
    }



    private void setupSpinner() {
        Spinner categorySpinner = binding.categoryBtnService;
        ArrayAdapter<String> categoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getCategoryNames());
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryArrayAdapter);
    }

    private void setupSubCategorySpinner(){
        Spinner subCategorySpinner=binding.subcategoryBtnService;
        ArrayAdapter<String> subCategoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getSubCategoryNames());
        subCategoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subCategorySpinner.setAdapter(subCategoryArrayAdapter);
    }

    private void setupEventTypeSpinner(){
        Spinner eventTypeSpinner=binding.eventTypeBtnService;
        ArrayAdapter<String> eventTypeArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getEventTypeNames());
        eventTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventTypeSpinner.setAdapter(eventTypeArrayAdapter);
    }
    private void selectCategories(AddProductFragment.OnCategoriesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            categories.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Category category = document.toObject(Category.class);
                                categories.add(category);
                            }
                            listener.onCategoriesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void selectSubCategories(AddProductFragment.OnSubCategoriesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("subCategories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            subCategories.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                SubCategory category = document.toObject(SubCategory.class);
                                subCategories.add(category);
                            }
                            listener.onSubCategoriesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void selectEventType(AddProductFragment.OnEventTypeLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("eventTypes").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            eventTypes.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Event event = document.toObject(Event.class);
                                eventTypes.add(event);
                            }
                            listener.onEventTypesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting events", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private ArrayList<String> getCategoryNames() {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
        }
        return categoryNames;
    }

    private ArrayList<String> getSubCategoryNames() {
        ArrayList<String> subCategoryNames = new ArrayList<>();
        for (SubCategory subCategory : subCategories) {
            subCategoryNames.add(subCategory.getName());

        }
        return subCategoryNames;
    }

    private ArrayList<String> getEventTypeNames() {
        ArrayList<String> eventTypeNames = new ArrayList<>();
        for (Event event : eventTypes) {
            eventTypeNames.add(event.getName());

        }
        return eventTypeNames;
    }

    interface OnCategoriesLoadedListener {
        void onCategoriesLoaded();
    }

    interface OnSubCategoriesLoadedListener {
        void onSubCategoriesLoaded();
    }

    interface OnEventTypeLoadedListener {
        void onEventTypesLoaded();
    }

    private ArrayList<String> getSubCategoryNames(ArrayList<SubCategory> subCategories) {
        ArrayList<String> subCategoryNames = new ArrayList<>();
        for (SubCategory subCategory : subCategories) {
            subCategoryNames.add(subCategory.getName());
        }
        return subCategoryNames;
    }

    private ArrayList<SubCategory> filterSubCategoriesByCategory(String category) {
        ArrayList<SubCategory> filteredSubCategories = new ArrayList<>();
        for (SubCategory subCategory : subCategories) {
            if (subCategory.getCategory().toString().equals(category) && subCategory.getStatus().equals(SubCategory.Status.SERVICE)) {
                filteredSubCategories.add(subCategory);
            }
        }
        return filteredSubCategories;
    }

    private void showConfirmationDialog(Service service) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add service");
        builder.setMessage("Are you sure you want to add this service?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("services").add(service)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                FragmentTransition.to(ProductsPageFragment.newInstance(),getActivity(),true,R.id.list_layout_products);
                                Toast.makeText(getActivity(), "Service created!", Toast.LENGTH_SHORT).show();
                                FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button_products);
                                addBtn.setVisibility(View.VISIBLE);

                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Something went wrong,try again!", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void findCompanyByUserEmail(String email, AddProductFragment.OnCompanyLoadedListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("companies").whereEqualTo("user.email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Company company = document.toObject(Company.class);
                                listener.onCompanyLoaded(company);
                                return;
                            }
                        } else {
                            Toast.makeText(getActivity(), "Error getting company", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    interface OnCompanyLoadedListener {
        void onCompanyLoaded(Company company);
    }


}