package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.reportedUsers.SingleReportCompanyFragment;
import com.example.eventplanner.fragments.reportedUsers.SingleUsersReportedFragment;
import com.example.eventplanner.model.report.CompanyReport;
import com.example.eventplanner.model.report.UserReport;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class CompanyReportedListAdapter extends ArrayAdapter<DocumentSnapshot> {

    private ArrayList<DocumentSnapshot> aProduct;
    private FragmentActivity mContext;

    public CompanyReportedListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> products) {
        super(context, R.layout.companies_reported_list_item, products);
        mContext = context;
        aProduct = products;
    }

    @Override
    public int getCount() {
        return aProduct.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.companies_reported_list_item, parent, false);
        }

        LinearLayout productItem = convertView.findViewById(R.id.companies_reported_list_item);
        TextView userReportEmail = convertView.findViewById(R.id.user_report_email);
        TextView companyReportedName = convertView.findViewById(R.id.company_reported_name);
        TextView status=convertView.findViewById(R.id.user_status);



        Button viewButton = convertView.findViewById(R.id.view_user_report_button);


        if (document != null) {
            CompanyReport product = document.toObject(CompanyReport.class);

            userReportEmail.setText(product.getUserReport());
            companyReportedName.setText(product.getCompanyReported().getName());
            status.setText(product.getStatus().toString());





            viewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CompanyReport companyReport= document.toObject(CompanyReport.class);

                    FragmentTransition.to(SingleReportCompanyFragment.newInstance(companyReport,document.getId()),mContext,true,R.id.list_layout_reports);

                }
            });



        }

        return convertView;
    }
}
