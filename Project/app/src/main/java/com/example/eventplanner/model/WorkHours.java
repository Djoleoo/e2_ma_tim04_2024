package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WorkHours implements Parcelable {

    private String Monday;
    private String Tuesday;
    private String Wednesday;
    private String Thursday;
    private String Friday;
    private String Saturday;
    private String Sunday;

    public WorkHours() {
    }

    public WorkHours(String monday, String tuesday, String wednesday, String thursday, String friday, String saturday, String sunday) {
        Monday = monday;
        Tuesday = tuesday;
        Wednesday = wednesday;
        Thursday = thursday;
        Friday = friday;
        Saturday = saturday;
        Sunday = sunday;
    }

    protected WorkHours(Parcel in) {
        Monday = in.readString();
        Tuesday = in.readString();
        Wednesday = in.readString();
        Thursday = in.readString();
        Friday = in.readString();
        Saturday = in.readString();
        Sunday = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Monday);
        dest.writeString(Tuesday);
        dest.writeString(Wednesday);
        dest.writeString(Thursday);
        dest.writeString(Friday);
        dest.writeString(Saturday);
        dest.writeString(Sunday);
    }

    public static final Creator<WorkHours> CREATOR = new Creator<WorkHours>() {
        @Override
        public WorkHours createFromParcel(Parcel in) {
            return new WorkHours(in);
        }

        @Override
        public WorkHours[] newArray(int size) {
            return new WorkHours[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getMonday() {
        return Monday;
    }

    public void setMonday(String monday) {
        Monday = monday;
    }

    public String getTuesday() {
        return Tuesday;
    }

    public void setTuesday(String tuesday) {
        Tuesday = tuesday;
    }

    public String getWednesday() {
        return Wednesday;
    }

    public void setWednesday(String wednesday) {
        Wednesday = wednesday;
    }

    public String getThursday() {
        return Thursday;
    }

    public void setThursday(String thursday) {
        Thursday = thursday;
    }

    public String getFriday() {
        return Friday;
    }

    public void setFriday(String friday) {
        Friday = friday;
    }

    public String getSaturday() {
        return Saturday;
    }

    public void setSaturday(String saturday) {
        Saturday = saturday;
    }

    public String getSunday() {
        return Sunday;
    }

    public void setSunday(String sunday) {
        Sunday = sunday;
    }


}
