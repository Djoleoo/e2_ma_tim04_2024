package com.example.eventplanner.fragments.categories;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleSubCategoryBinding;
import com.example.eventplanner.databinding.FragmentSingleSuggestionBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.SubCategorySuggestion;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleSuggestionFragment extends Fragment {

    private FragmentSingleSuggestionBinding binding;
    private SubCategorySuggestion mSuggestion;
    private String documentId;
    private static final String ARG_SUGGESTION = "suggestion";
    private static final String ARG_EVENT_ID = "suggestionId";

    private ArrayList<Category> categories = new ArrayList<>();
    ExecutorService executor = Executors.newSingleThreadExecutor();

    String[] options = {"SERVICE", "PRODUCT"};


    public SingleSuggestionFragment() {
        // Required empty public constructor
    }

    public static SingleSuggestionFragment newInstance(SubCategorySuggestion suggestion,String documentId) {
        SingleSuggestionFragment fragment = new SingleSuggestionFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_SUGGESTION, suggestion);
        args.putString(ARG_EVENT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSuggestion = getArguments().getParcelable(ARG_SUGGESTION);
            documentId = getArguments().getString(ARG_EVENT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSingleSuggestionBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        EditText name = binding.subCategoryName;
        TextView description = binding.subcategoryDescription;
        EditText subCategoryUser = binding.suggestionUser;
        Spinner category = binding.categorySpinner;
        selectCategories(new SingleSubCategoryFragment.OnCategoriesLoadedListener() {
            @Override
            public void onCategoriesLoaded() {
                setupSpinner();
            }
        });

        Spinner type = binding.typeSpinner;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, options);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(adapter);
        int selectedIndex = Arrays.asList(options).indexOf(mSuggestion.getStatus().toString());
        type.setSelection(selectedIndex);
        type.setEnabled(false);



        Button editButton = binding.buttonEdit;
        Button acceptButton = binding.buttonAccept;
        Button cancelButton = binding.buttonCancel;

        cancelButton.setVisibility(View.GONE);

        if(mSuggestion != null) {
            name.setText(mSuggestion.getsubCategoryName());
            description.setText(mSuggestion.getDescription());
            subCategoryUser.setText(mSuggestion.getSenderEmail());
        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.setEnabled(true);
                description.setEnabled(true);
                category.setEnabled(true);
                type.setEnabled(true);
                cancelButton.setVisibility(View.VISIBLE);
                editButton.setVisibility(View.GONE);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.setEnabled(false);
                description.setEnabled(false);
                category.setEnabled(false);
                type.setEnabled(false);
                cancelButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
                acceptButton.setVisibility(View.VISIBLE);
            }
        });
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String suggestionType = (String) type.getSelectedItem();
                SubCategory.Status status = SubCategory.Status.valueOf(suggestionType.toUpperCase());
                String selectedCategoryName = (String) category.getSelectedItem();
                Category selectedCategory = null;
                for (Category category : categories) {
                    if (category.getName().equals(selectedCategoryName)) {
                        selectedCategory = category;
                        break;
                    }
                }

                SubCategory newSubcategory = new SubCategory(null,name.getText().toString(),description.getText().toString(),selectedCategory,status);
                showEditConfirmationDialog(newSubcategory);
            }
        });

        return root;
    }

    private void showEditConfirmationDialog(SubCategory subCategory) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Accept suggestion");
        builder.setMessage("Are you want to accept this suggestion the way it is?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("subCategories").add(subCategory)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                executor.execute(() -> {
                                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                                    db.collection("subCategoriesSuggestions").document(documentId)
                                            .delete();

                                });
                                FragmentTransition.to(CategoriesPageFragment.newInstance(),getActivity(),true,R.id.list_layout);
                                Toast.makeText(getActivity(), "Suggestion confirmed", Toast.LENGTH_SHORT).show();
                                FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                addBtn.setVisibility(View.VISIBLE);
                                MaterialButtonToggleGroup toggleGroup = requireActivity().findViewById(R.id.toggle_group);
                                toggleGroup.check(R.id.button_categories); // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Suggestion confirmed", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private ArrayList<String> getCategoryNames() {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
        }
        return categoryNames;
    }

    private void setupSpinner() {
        ArrayList<String>categoryNames = getCategoryNames();
        Spinner categorySpinner = binding.categorySpinner;
        ArrayAdapter<String> categoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                categoryNames);
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryArrayAdapter);
        int index = categoryNames.indexOf(mSuggestion.getCategory().getName());
        // Postavlja izabrani element u spinner na osnovu pronađenog indeksa
        if (index != -1) {
            categorySpinner.setSelection(index);
        }
        categorySpinner.setEnabled(false);
    }

    private void selectCategories(SingleSubCategoryFragment.OnCategoriesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            categories.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Category category = document.toObject(Category.class);
                                categories.add(category);
                            }
                            listener.onCategoriesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    interface OnCategoriesLoadedListener {
        void onCategoriesLoaded();
    }
}