package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Activity;
import com.example.eventplanner.model.Happening;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityViewHolder> {

    private List<Activity> activities;
    private Happening happening;
    private FirebaseFirestore db;
    private Context context;

    public ActivityAdapter(List<Activity> activities, Happening happening, Context context) {
        this.activities = activities;
        this.happening = happening;
        this.context = context;
        this.db = FirebaseFirestore.getInstance();  // Initialize Firestore
    }

    @NonNull
    @Override
    public ActivityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activity, parent, false);
        return new ActivityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityViewHolder holder, int position) {
        Activity activity = activities.get(position);
        holder.activityNameTextView.setText(activity.getName());
        holder.activityDescriptionTextView.setText(activity.getDescription());
        holder.startTimeTextView.setText(activity.getStartTime());
        holder.endTimeTextView.setText(activity.getEndTime());
        holder.locationTextView.setText(activity.getLocation());

        // Handle Delete Button Click
        holder.deleteButton.setOnClickListener(v -> {
            deleteActivity(position);
        });
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public static class ActivityViewHolder extends RecyclerView.ViewHolder {
        TextView activityNameTextView;
        TextView activityDescriptionTextView;
        TextView startTimeTextView;
        TextView endTimeTextView;
        TextView locationTextView;
        View deleteButton;

        public ActivityViewHolder(@NonNull View itemView) {
            super(itemView);
            activityNameTextView = itemView.findViewById(R.id.activity_name_text_view);
            activityDescriptionTextView = itemView.findViewById(R.id.activity_description_text_view);
            startTimeTextView = itemView.findViewById(R.id.start_time_text_view);
            endTimeTextView = itemView.findViewById(R.id.end_time_text_view);
            locationTextView = itemView.findViewById(R.id.location_text_view);
            deleteButton = itemView.findViewById(R.id.btn_delete_activity);
        }
    }

    private void deleteActivity(int position) {
        activities.remove(position);
        notifyItemRemoved(position);
        updateHappeningInFirestore();
        Toast.makeText(context, "Activity deleted", Toast.LENGTH_SHORT).show();
    }

    private void updateHappeningInFirestore() {
        db.collection("happenings").document(happening.getId())
                .set(happening)
                .addOnSuccessListener(aVoid -> {
                    // Successfully updated the Happening in Firestore
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(context, "Failed to update Happening.", Toast.LENGTH_SHORT).show();
                });
    }
}
