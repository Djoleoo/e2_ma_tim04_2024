package com.example.eventplanner.fragments.products;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSinglePackageBinding;
import com.example.eventplanner.databinding.FragmentSingleProductBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.report.CompanyReport;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SingleProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SinglePackageFragment extends Fragment {

    private FragmentSinglePackageBinding binding;
    private Package mPackage;

    private String documentId;
    private static final String ARG_SERVICE_ID = "serviceId";


    private static final String ARG_PRODUCT = "product";


    public SinglePackageFragment() {
        // Required empty public constructor
    }

    public static SinglePackageFragment newInstance(Package product,String documentId) {
        SinglePackageFragment fragment = new SinglePackageFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        args.putString(ARG_SERVICE_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPackage = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_SERVICE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSinglePackageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        EditText productName = binding.packageNameEdit;
        EditText productDescription = binding.packageDescriptionEdit;
        EditText productCategory = binding.packageCategoryEdit;
        EditText productSubcategory = binding.packageSubcategoryEdit;
        EditText productPrice = binding.packagePriceEdit;
        EditText productDiscount = binding.packageDiscountEdit;
        ImageView imageView = binding.productImageSingle;
        EditText productEventType = binding.packageEventTypeEdit;
        CheckBox productAvailable = binding.packageAvailableEdit;
        CheckBox productVisible = binding.packageVisibleEdit;
        CheckBox packageAutomatic=binding.packageAutomaticAcceptanceEdit;
        EditText packageProducts=binding.packageProductsEdit;
        EditText packageServices= binding.packageServicesEdit;
        Button editButton = binding.buttonEditProduct;
        Button submitButton = binding.buttonConfirmProduct;
        Button cancelButton = binding.buttonCancelProduct;
        Spinner eventTypeSpinner=binding.eventTypeSpinner;
        Button addEventType=binding.addEventtype;
        Button deleteEventType=binding.deleteEventtype;
        Spinner productSpinner=binding.productSpinner;
        Button addProduct=binding.addProduct;
        Button deleteProduct= binding.deleteProduct;
        Spinner serviceSpinner=binding.serviceSpinner;
        Button addService=binding.addService;
        Button deleteService= binding.deleteService;

        submitButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);

        Button addImageButton=binding.addImagePackage;
        Button deleteImageButton=binding.deleteImagePackage;

        //new
        Button reportProduct=binding.buttonReportProduct;
        Button submitReport=binding.buttonSubmitReport;
        EditText reportReason=binding.productReportEdit;
        TextView reportReasonLabel=binding.productReportLabel;


        TextView companyEmail = root.findViewById(R.id.company_email);
        TextView companyName = root.findViewById(R.id.company_name);
        TextView companyAddress = root.findViewById(R.id.company_address);
        TextView companyNumber = root.findViewById(R.id.company_number);
        TextView companyDescription = root.findViewById(R.id.company_description);
        TextView monday = root.findViewById(R.id.workhours_monday);
        TextView tuesday = root.findViewById(R.id.workhours_tuesday);
        TextView wednesday = root.findViewById(R.id.workhours_wednesday);
        TextView thursday = root.findViewById(R.id.workhours_thursday);
        TextView friday = root.findViewById(R.id.workhours_friday);
        TextView saturday = root.findViewById(R.id.workhours_saturday);
        TextView sunday = root.findViewById(R.id.workhours_sunday);


        TextView userName = root.findViewById(R.id.user_email);


        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            User currentUser = documentSnapshot.toObject(User.class);
                            if (currentUser != null && currentUser.getUserType() == User.UserType.OD) {
                                editButton.setVisibility(View.GONE);
                            }
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Handle any errors here
                        Toast.makeText(getActivity(), "Failed to load user information", Toast.LENGTH_SHORT).show();
                    });
        }


        if (mPackage != null) {
            productName.setText(mPackage.getName());
            productDescription.setText(mPackage.getDescription());
            productCategory.setText(mPackage.getCategory().getName());


            //new
            companyEmail.setText(mPackage.getCompany().getEmail());
            companyAddress.setText(mPackage.getCompany().getAddress());
            companyName.setText(mPackage.getCompany().getName());
            companyDescription.setText(mPackage.getCompany().getDescription());
            companyNumber.setText(mPackage.getCompany().getNumber());
            userName.setText(mPackage.getUserEmail());

            monday.setText(mPackage.getCompany().getWorkHours().getMonday());
            tuesday.setText(mPackage.getCompany().getWorkHours().getTuesday());
            wednesday.setText(mPackage.getCompany().getWorkHours().getWednesday());
            thursday.setText(mPackage.getCompany().getWorkHours().getThursday());
            friday.setText(mPackage.getCompany().getWorkHours().getFriday());
            saturday.setText(mPackage.getCompany().getWorkHours().getSaturday());
            sunday.setText(mPackage.getCompany().getWorkHours().getSunday());

            if (mPackage.getSubCategories() != null) {
                StringBuilder subCategoryText = new StringBuilder();
                for (SubCategory subCategory : mPackage.getSubCategories()) {
                    subCategoryText.append(subCategory.getName()).append("\n");
                }
                productSubcategory.setText(subCategoryText.toString());
            }
            if (mPackage.getProducts() != null) {
                StringBuilder productsText = new StringBuilder();
                for (Product products : mPackage.getProducts()) {
                    productsText.append(products.getName()).append("\n");
                }
                packageProducts.setText(productsText.toString());
            }
            if (mPackage.getServices() != null) {
                StringBuilder servicesText = new StringBuilder();
                for (Service service : mPackage.getServices()) {
                    servicesText.append(service.getName()).append("\n");
                }
                packageServices.setText(servicesText.toString());
            }
            productPrice.setText(String.valueOf(mPackage.getPrice()));
            productDiscount.setText(String.valueOf(mPackage.getDiscount()));



            if (mPackage.getEventTypes() != null) {
                StringBuilder eventTypeText = new StringBuilder();
                for (Event type : mPackage.getEventTypes()) {
                    eventTypeText.append(type).append("\n");
                }
                productEventType.setText(eventTypeText.toString());
            }

            productAvailable.setChecked(mPackage.isAvailable());
            productVisible.setChecked(mPackage.isVisible());
            packageAutomatic.setChecked(mPackage.isAutomaticAcceptance());

        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productName.setEnabled(true);
                productDescription.setEnabled(true);
                productPrice.setEnabled(true);
                productDiscount.setEnabled(true);

                productEventType.setEnabled(false);
                productAvailable.setEnabled(true);
                productVisible.setEnabled(true);
                packageProducts.setEnabled(false);
                packageServices.setEnabled(false);
                submitButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                editButton.setVisibility(View.GONE);


                reportProduct.setVisibility(View.GONE);
                submitReport.setVisibility(View.GONE);

                reportReason.setVisibility(View.GONE);
                reportReasonLabel.setVisibility(View.GONE);

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productName.setEnabled(false);
                productDescription.setEnabled(false);
                productPrice.setEnabled(false);
                productDiscount.setEnabled(false);

                productEventType.setEnabled(false);
                productAvailable.setEnabled(false);
                productVisible.setEnabled(false);
                packageProducts.setEnabled(false);
                packageServices.setEnabled(false);
                submitButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
                addImageButton.setVisibility(View.GONE);
                deleteImageButton.setVisibility(View.GONE);
                eventTypeSpinner.setVisibility(View.GONE);
                addEventType.setVisibility(View.GONE);
                deleteEventType.setVisibility(View.GONE);
                productSpinner.setVisibility(View.GONE);
                addProduct.setVisibility(View.GONE);
                deleteProduct.setVisibility(View.GONE);
                serviceSpinner.setVisibility(View.GONE);
                addService.setVisibility(View.GONE);
                deleteService.setVisibility(View.GONE);

                reportProduct.setVisibility(View.VISIBLE);
                submitReport.setVisibility(View.GONE);

                reportReason.setVisibility(View.GONE);
                reportReasonLabel.setVisibility(View.GONE);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Package package1= new Package(productName.getText().toString(),productDescription.getText().toString(),Double.parseDouble(productDiscount.getText().toString()),productVisible.isChecked(),productAvailable.isChecked(),mPackage.getCategory(),mPackage.getProducts(),mPackage.getServices(),packageAutomatic.isChecked(),mPackage.getUserEmail(),mPackage.getCompany());
                showEditConfirmationDialog(package1);
            }
        });


        reportProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportProduct.setVisibility(View.GONE);
                submitReport.setVisibility(View.VISIBLE);

                reportReason.setVisibility(View.VISIBLE);
                reportReasonLabel.setVisibility(View.VISIBLE);

                reportReason.setEnabled(true);


                editButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                submitButton.setVisibility(View.GONE);
            }
        });

        submitReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                if (currentUser == null) {
                    Toast.makeText(getActivity(), "User not logged in", Toast.LENGTH_SHORT).show();
                    return;
                }
                String userReportEmail = currentUser.getEmail();

                // Get the report reason from EditText
                String reportReasonText = reportReason.getText().toString().trim();
                if (reportReasonText.isEmpty()) {
                    Toast.makeText(getActivity(), "Please enter a report reason", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Get the current time
                Date reportTime = new Date();

                // Create a CompanyReport object
                CompanyReport companyReport = new CompanyReport(
                        mPackage.getCompany(),  // Company reported
                        mPackage.getCompany().getUser(),     // User reported
                        userReportEmail,        // User report (email of the current user)
                        reportTime,             // Time of the report
                        reportReasonText,       // Reason for the report
                        CompanyReport.Status.REPORTED  // Initial status
                );

                // Save the CompanyReport to Firestore
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("companyReports")
                        .add(companyReport)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Toast.makeText(getActivity(), "Report submitted successfully", Toast.LENGTH_SHORT).show();

                                // Reset the report UI
                                reportProduct.setVisibility(View.VISIBLE);
                                submitReport.setVisibility(View.GONE);
                                reportReason.setVisibility(View.GONE);
                                reportReasonLabel.setVisibility(View.GONE);

                                editButton.setVisibility(View.VISIBLE);
                                cancelButton.setVisibility(View.GONE);
                                submitButton.setVisibility(View.GONE);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Failed to submit report", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });




        return root;
    }

    private void showEditConfirmationDialog(Package service) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit Package");
        builder.setMessage("Are you sure you want to edit this package?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("packages").document(documentId);
                docRef.set(service)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {

                                FragmentTransition.to(ProductsPageFragment.newInstance(),getActivity(),true, R.id.list_layout_products);
                                Toast.makeText(getActivity(), "Package updated!", Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}