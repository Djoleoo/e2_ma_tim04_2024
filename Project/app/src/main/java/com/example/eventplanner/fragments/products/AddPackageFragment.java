package com.example.eventplanner.fragments.products;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentAddPackageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class AddPackageFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private FragmentAddPackageBinding binding;
    private ArrayList<Integer> selectedImages = new ArrayList<>();
    private ArrayList<Category> categories = new ArrayList<>();
    private ArrayList<SubCategory> subCategories= new ArrayList<>();
    private ArrayList<Event> eventTypes = new ArrayList<>();
    private ArrayList<Product> products= new ArrayList<>();
    private ArrayList<Service> services= new ArrayList<>();


    private String mParam1;
    private String mParam2;

    public AddPackageFragment() {
        // Required empty public constructor
    }


    public static AddPackageFragment newInstance() {
        AddPackageFragment fragment = new AddPackageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAddPackageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Button addImage = binding.addImageButton;
        Spinner categorySpinner=binding.categoryBtnPackage;
        Spinner productSpinner=binding.productsBtnPackage;
        Spinner serviceSpinner=binding.servicesBtnPackage;
        EditText packageName=binding.packageName;
        EditText packageDescription=binding.packageDescription;
        EditText packageDiscount=binding.packageDiscount;
        CheckBox packageAvailableCheckbox=binding.packageAvailableCheckbox;
        CheckBox packageVisibleCheckbox= binding.packageVisibleCheckbox;

        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.album);
        images.add(R.drawable.camera);
        images.add(R.drawable.photographing);

        selectCategories(new AddProductFragment.OnCategoriesLoadedListener() {
            @Override
            public void onCategoriesLoaded() {
                // Kreiraj adapter nakon što su kategorije učitane
                setupSpinner();
            }
        });

        selectProducts(new AddPackageFragment.OnProductsLoadedListener() {
            @Override
            public void onProductsLoaded() {
                setupProductsSpinner();
            }
        });

        selectServices(new AddPackageFragment.OnServicesLoadedListener() {
            @Override
            public void onServicesLoaded() {
                setupServicesSpinner();
            }
        });



        /*ArrayList<Category> categories = new ArrayList<>();
        Category fotoCategory=new Category(1L,"Foto","cap");
        Category restaurantCategory=new Category(2L,"Ugostiteljstvo","cap");
        Category category1 = new Category(1L,"Foto i video","Profesionalno fotografisanje i snimanje događaja.");
        SubCategory subCategory1= new SubCategory("Fotografije i albumi","Profesionalno fotografisanje događaja (ceremonije, prijemi, portreti).\n" +
                "Angažovanje fotografa za celodnevno praćenje događaja. Specijalizovane foto\n" +
                "sesije (veridba, pre-wedding, post-wedding)",category1,SubCategory.Status.PRODUCT);
        categories.add(fotoCategory);
        categories.add(restaurantCategory);
        categories.add(category1);*/


        /*ArrayAdapter<Category> adapterCategory = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapterCategory);*/

        /*ArrayList<SubCategory> sub= new ArrayList<>();
        sub.add(subCategory1);

        ArrayList<Product> products = new ArrayList<>();

        ArrayList<Event> eventType1 = new ArrayList<>();
        eventType1.add(new Event("svadbe","svadba svadba svadba",sub,false));

        Product product = new Product(1L,category1,
                subCategory1,"Album sa 50 fotografija",
                "Album sa izradjenih 50 fotografija",2000.00,5.00,
                images,eventType1,true,false);

        Product product2 = new Product(1L,category1,
                subCategory1,"Album sa 10 fotografija",
                "Album sa izradjenih 50 fotografija",2000.00,5.00,
                images,eventType1,true,false);
        products.add(product2);
        products.add(product);

        ArrayAdapter<Product> adapterProduct = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, products);
        adapterProduct.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productSpinner.setAdapter(adapterProduct);

        ArrayList<Service> services = new ArrayList<>();*/
        ArrayList<String> persons1= new ArrayList<>();
        persons1.add("Pera");
        persons1.add("Igor");

        /*Service service = new Service(1L,category1,subCategory1,"Snimanje dronom",
                "Ovo je snimanje iz vazduha sa dronom",images,
                "ne radimo praznicma.",3000.00,2,
                "okolina Novog Sada",0.00,persons1,eventType1,
                365,2,false,
                true,true);

        Service service2= new Service(2L,category1,subCategory1,"Snimanje kamerom 4k",
                "Ovo je snimanje u 4k rezoluciji",images,
                "/",5000.00,2,
                "okolina Novog Sada",0.00,persons1,eventType1,
                365,2,false,
                true,true);
        services.add(service2);
        services.add(service);*/



        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagesDialog(images);
            }
        });

        /*categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category) parent.getSelectedItem();
                ArrayList<Product> filteredProducts = filterProductsByCategory(selectedCategory);
                ArrayAdapter<Product> adapterProduct = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, filteredProducts);
                adapterProduct.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                productSpinner.setAdapter(adapterProduct);

                ArrayList<Service> filteredService = filterServiceByCategory(selectedCategory);
                ArrayAdapter<Service> adapterService = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, filteredService);
                adapterService.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                serviceSpinner.setAdapter(adapterService);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle when nothing is selected

            }

            /*private ArrayList<Product> filterProductsByCategory(Category category) {
                ArrayList<Product> filteredProducts = new ArrayList<>();
                for (Product product : products) {
                    if (product.getCategory().equals(category)) {
                        filteredProducts.add(product);
                    }
                }
                return filteredProducts;
            }

            private ArrayList<Service> filterServiceByCategory(Category category) {
                ArrayList<Service> filteredServices = new ArrayList<>();
                for (Service service : services) {
                    if (service.getCategory().equals(category)) {
                        filteredServices.add(service);
                    }
                }
                return filteredServices;
            }
        });*/


        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCategoryName = (String) parent.getSelectedItem();
                //Category selectedCategory = findCategoryByName(selectedCategoryName);
                if (!selectedCategoryName.equals("")) {
                    ArrayList<Product> filteredProducts = filterProductsByCategory(selectedCategoryName);
                    ArrayAdapter<String> productArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                            getProductsNames(filteredProducts));
                    productArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    productSpinner.setAdapter(productArrayAdapter);


                    ArrayList<Service> filteredServices = filterServicesByCategory(selectedCategoryName);
                    ArrayAdapter<String> serviceArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                            getServicesNames(filteredServices));
                    serviceArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    serviceSpinner.setAdapter(serviceArrayAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle when nothing is selected
            }
        });

        Button button = binding.createPackageBtn;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = packageName.getText().toString();
                String description = packageDescription.getText().toString();
                //double price=Double.parseDouble(productPrice.getText().toString());
                double discount=Double.parseDouble(packageDiscount.getText().toString());
                Spinner categorySpinner = binding.categoryBtnPackage;
                Spinner productSpinner=binding.productsBtnPackage;
                Spinner serviceSpinner=binding.servicesBtnPackage;
                boolean available= packageAvailableCheckbox.isChecked();
                boolean visible=packageVisibleCheckbox.isChecked();

                String selectedCategoryName = (String) categorySpinner.getSelectedItem();
                String selectedProductName= (String) productSpinner.getSelectedItem();
                String selectedServiceName= (String) serviceSpinner.getSelectedItem();

                Category selectedCategory = null;
                for (Category category : categories) {
                    if (category.getName().equals(selectedCategoryName)) {
                        selectedCategory = category;
                        break;
                    }
                }
                ArrayList<Product> selectedProducts=new ArrayList<>();
                Product selectedProduct = null;
                for(Product product:products){
                    if(product.getName().equals(selectedProductName)){
                        selectedProduct=product;
                        selectedProducts.add(selectedProduct);
                        break;
                    }
                }
                ArrayList<Service> selectedServices=new ArrayList<>();
                Service selectedService = null;
                for(Service service:services){
                    if(service.getName().equals(selectedServiceName)){
                        selectedService=service;
                        selectedServices.add(selectedService);
                        break;
                    }
                }
                if (selectedCategory!=null && selectedProduct!=null  && selectedService!=null) {





                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                    if (currentUser != null) {
                        Category finalSelectedCategory = selectedCategory;
                        Service finalSelectedService=selectedService;

                        findCompanyByUserEmail(currentUser.getEmail(), new AddProductFragment.OnCompanyLoadedListener() {
                            @Override
                            public void onCompanyLoaded(Company company) {
                                Package packageObj = new Package(name,description,discount,visible,available,finalSelectedCategory,selectedProducts,selectedServices,finalSelectedService.isAutomaticAcceptance(),currentUser.getEmail(),company);
                                showConfirmationDialog(packageObj);
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "User not logged in", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // Ako nije pronađena odgovarajuća kategorija
                    Toast.makeText(getActivity(), "Selected category not found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return root;
    }

    private void showImagesDialog(ArrayList<Integer> images) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_image, null);
        builder.setView(dialogView);

        // Initialize Spinner
        Spinner imageSpinner = dialogView.findViewById(R.id.image_spinner);
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, images);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        imageSpinner.setAdapter(adapter);


        builder.setPositiveButton("Add", (dialog, which) -> {
            Integer selectedImage = (Integer) imageSpinner.getSelectedItem();
            if (selectedImage != null) {
                addSelectedImage(selectedImage);
            }
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void addSelectedImage(Integer selectedImage) {
        selectedImages.add(selectedImage); // Dodajemo novu subkategoriju u listu

        // Pronalazimo ListView
        ListView selectedImagesListView = requireView().findViewById(R.id.images_listview);

        // Ako je ListView bio nevidljiv, sada ga prikazujemo
        selectedImagesListView.setVisibility(View.VISIBLE);

        // Ako već nemamo adapter, kreiramo novi i postavljamo ga na ListView
        if (selectedImagesListView.getAdapter() == null) {
            ArrayAdapter<Integer> selectedImagesAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, selectedImages);
            selectedImagesListView.setAdapter(selectedImagesAdapter);
        } else { // Ako već imamo adapter, samo ga ažuriramo
            ArrayAdapter<Integer> selectedImagesAdapter = (ArrayAdapter<Integer>) selectedImagesListView.getAdapter();
            selectedImagesAdapter.notifyDataSetChanged();
        }
    }

    private void setupSpinner() {
        Spinner categorySpinner = binding.categoryBtnPackage;
        ArrayAdapter<String> categoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getCategoryNames());
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryArrayAdapter);
    }

    private void setupProductsSpinner(){
        Spinner productsSpinner=binding.productsBtnPackage;
        ArrayAdapter<String> productsArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getProductsNames());
        productsArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productsSpinner.setAdapter(productsArrayAdapter);
    }

    private void setupServicesSpinner(){
        Spinner servicesSpinner=binding.servicesBtnPackage;
        ArrayAdapter<String> servicesArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getServicesNames());
        servicesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servicesSpinner.setAdapter(servicesArrayAdapter);
    }

    private void selectCategories(AddProductFragment.OnCategoriesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            categories.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Category category = document.toObject(Category.class);
                                categories.add(category);
                            }
                            listener.onCategoriesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting products", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void selectProducts(AddPackageFragment.OnProductsLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            products.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Product product = document.toObject(Product.class);
                                products.add(product);
                            }
                            listener.onProductsLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void selectServices(AddPackageFragment.OnServicesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("services").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            services.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Service service = document.toObject(Service.class);
                                services.add(service);
                            }
                            listener.onServicesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting services", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void showConfirmationDialog(Package packageobj) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Package");
        builder.setMessage("Are you sure you want to add this package?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("packages").add(packageobj)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                FragmentTransition.to(ProductsPageFragment.newInstance(),getActivity(),true,R.id.list_layout_products);
                                Toast.makeText(getActivity(), "Package created!", Toast.LENGTH_SHORT).show();
                                FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button_products);
                                addBtn.setVisibility(View.VISIBLE);

                                // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Something went wrong,try again!", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private ArrayList<String> getCategoryNames() {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
        }
        return categoryNames;
    }

    private ArrayList<String> getProductsNames() {
        ArrayList<String> productsNames = new ArrayList<>();
        for (Product product : products) {
            productsNames.add(product.getName());

        }
        return productsNames;
    }
    private ArrayList<String> getProductsNames(ArrayList<Product> products) {
        ArrayList<String> productsNames = new ArrayList<>();
        for (Product product : products) {
            productsNames.add(product.getName());

        }
        return productsNames;
    }

    private ArrayList<String> getServicesNames() {
        ArrayList<String> servicesNames = new ArrayList<>();
        for (Service service : services) {
            servicesNames.add(service.getName());

        }
        return servicesNames;
    }
    private ArrayList<String> getServicesNames(ArrayList<Service> services) {
        ArrayList<String> servicesNames = new ArrayList<>();
        for (Service service : services) {
            servicesNames.add(service.getName());

        }
        return servicesNames;
    }


    interface OnCategoriesLoadedListener {
        void onCategoriesLoaded();
    }

    interface OnProductsLoadedListener {
        void onProductsLoaded();
    }

    interface OnServicesLoadedListener {
        void onServicesLoaded();
    }

    private ArrayList<Product> filterProductsByCategory(String category) {
        ArrayList<Product> filteredProducts = new ArrayList<>();
        for (Product product : products) {
            if (product.getCategory().toString().equals(category)) {
                filteredProducts.add(product);
            }
        }
        return filteredProducts;
    }


    private ArrayList<Service> filterServicesByCategory(String category) {
        ArrayList<Service> filteredServices = new ArrayList<>();
        for (Service service : services) {
            if (service.getCategory().toString().equals(category)) {
                filteredServices.add(service);
            }
        }
        return filteredServices;
    }

    private void findCompanyByUserEmail(String email, AddProductFragment.OnCompanyLoadedListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("companies").whereEqualTo("user.email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Company company = document.toObject(Company.class);
                                listener.onCompanyLoaded(company);
                                return;
                            }
                        } else {
                            Toast.makeText(getActivity(), "Error getting company", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    interface OnCompanyLoadedListener {
        void onCompanyLoaded(Company company);
    }

}