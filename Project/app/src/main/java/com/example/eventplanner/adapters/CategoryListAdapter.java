package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;

import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.SingleCategoryFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.annotation.Documented;
import java.util.ArrayList;

public class CategoryListAdapter extends ArrayAdapter<DocumentSnapshot> {

    private ArrayList<DocumentSnapshot> aCategory;
    private FragmentActivity mContext;

    public CategoryListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> categories){
        super(context, R.layout.category_list_item);
        mContext = context;
        aCategory = categories;
    }

    @Override
    public int getCount() {
        return aCategory.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.category_list_item,
                    parent, false);
        }
        LinearLayout categoryItem = convertView.findViewById(R.id.category_list_item);
        TextView categoryName = convertView.findViewById(R.id.category_name);
        Button viewButton = convertView.findViewById(R.id.view_button);
        Button deleteButton = convertView.findViewById(R.id.delete_button);

        if(document != null){
            String eventNameText = document.getString("name");
            categoryName.setText(eventNameText);
            deleteFilter(document.toObject(Category.class).getName(),deleteButton);
        }


        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Category category = document.toObject(Category.class);
                View parent = (View) v.getParent();
                FloatingActionButton addBtn = parent.getRootView().findViewById(R.id.floating_action_button);
                addBtn.setVisibility(View.GONE);
                FragmentTransition.to(SingleCategoryFragment.newInstance(category,document.getId()),mContext,true,R.id.list_layout);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteConfirmationDialog(document);
            }
        });

        return convertView;
    }

    private void showDeleteConfirmationDialog(DocumentSnapshot document) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete Category");
        builder.setMessage("Are you sure you want to delete this category?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("categories").document(document.getId()).delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                // Ovdje dodajte logiku za brisanje događaja iz liste
                                Toast.makeText(getContext(),"Obrisalo se hehe",Toast.LENGTH_SHORT).show();
                                aCategory.remove(document);
                                notifyDataSetChanged();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(),"Nije se obrisalo jbg",Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void deleteFilter(String categoryName,Button deleteButton){
        ArrayList<DocumentSnapshot>products = new ArrayList<>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if(categoryName.equals(document.toObject(Product.class).getCategory().getName())){
                                    products.add(document);
                                }
                            }
                            if (products.isEmpty()) {
                                deleteButton.setVisibility(View.VISIBLE);
                            } else {
                                deleteButton.setVisibility(View.INVISIBLE);
                            }

                        }
                    }
                });
    }

}
