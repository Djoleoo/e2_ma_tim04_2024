package com.example.eventplanner.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.happenings.ActivitiesFragment;
import com.example.eventplanner.fragments.happenings.BudgetPlannerFragment;
import com.example.eventplanner.fragments.happenings.GuestsFragment;
import com.example.eventplanner.fragments.happenings.HappeningsFragment;
import com.example.eventplanner.fragments.products.ProductsPageFragment;
import com.example.eventplanner.model.Happening;
import com.example.eventplanner.model.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class HappeningListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private ArrayList<DocumentSnapshot> aHappenings;
    private FragmentActivity mContext;
    private User currentUser;

    public HappeningListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> happenings) {
        super(context, R.layout.happening_list_item);
        mContext = context;
        aHappenings = happenings;
        loadCurrentUser();
    }

    private void loadCurrentUser() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            currentUser = documentSnapshot.toObject(User.class);
                            Log.d("LOADED USER", "User email: " + currentUser.getEmail());
                            // Notify the adapter to refresh the list after the user is loaded
                            notifyDataSetChanged();
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Handle any errors here
                    });
        }
    }

    @Override
    public int getCount() {
        return aHappenings.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aHappenings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot happeningSnapshot = getItem(position);
        Happening happening = happeningSnapshot.toObject(Happening.class);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.happening_list_item, parent, false);
        }
        LinearLayout happeningItem = convertView.findViewById(R.id.happening_list_item);
        TextView happeningName = convertView.findViewById(R.id.happening_name);
        Button budgetButton = convertView.findViewById(R.id.budget_happening_button);
        Button invitationsButton = convertView.findViewById(R.id.invitations_happening_button);

        if (happening != null) {
            String happeningNameText = happeningSnapshot.getString("name");
            happeningName.setText(happeningNameText);
        }

        Button goToProducts = convertView.findViewById(R.id.search_products_button);

        // Only perform comparison if the currentUser is loaded
        if (currentUser != null && happening.getCreatedBy() != null) {
            Log.d("MEJL ULOGOVANOG", currentUser.getEmail());
            Log.d("MEJL IZ HAPPENINGA", happening.getCreatedBy().getEmail());

            if (!currentUser.getEmail().equals(happening.getCreatedBy().getEmail())) {
                // If the current user didn't create this happening, disable the buttons
                budgetButton.setEnabled(false);
                invitationsButton.setEnabled(false);
                goToProducts.setEnabled(false);
            }
        } else {
            // If currentUser is not loaded, maybe show a loading state or simply disable the buttons
//            budgetButton.setEnabled(false);
//            invitationsButton.setEnabled(false);
//            goToProducts.setEnabled(false);
        }

        goToProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DocumentSnapshot happeningSnapshot = getItem(position);
                Happening happening = happeningSnapshot.toObject(Happening.class);

                if (happening != null) {
                    happening.setId(happeningSnapshot.getId());  // Ensure the ID is set correctly

                    // Pass the Happening object to the ActivitiesFragment
                    FragmentTransition.to(ActivitiesFragment.newInstance(happening), mContext, true, R.id.list_layout_happenings);

                    // Hide the FloatingActionButton when navigating to the activities page
                    View parent = (View) v.getParent();
                    FloatingActionButton addBtn = parent.getRootView().findViewById(R.id.floating_action_button_happenings);
                    addBtn.setVisibility(View.GONE);
                }
            }
        });

        budgetButton.setOnClickListener(v -> {
            FragmentTransition.to(BudgetPlannerFragment.newInstance(), mContext, true, R.id.list_layout_happenings);
            hideFloatingActionButton(v);
        });

        invitationsButton.setOnClickListener(v -> {
            Happening happeningObj = happeningSnapshot.toObject(Happening.class);
            FragmentTransition.to(GuestsFragment.newInstance(happeningObj), mContext, true, R.id.list_layout_happenings);
            hideFloatingActionButton(v);
        });

        return convertView;
    }

    private void hideFloatingActionButton(View view) {
        View parent = (View) view.getParent();
        FloatingActionButton addBtn = parent.getRootView().findViewById(R.id.floating_action_button_happenings);
        addBtn.setVisibility(View.GONE);
    }
}
