package com.example.eventplanner.fragments.happenings;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentAddHappeningBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Guest;
import com.example.eventplanner.model.Happening;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AddHappeningFragment extends Fragment {

    private Spinner eventTypeSpinner;
    private Spinner subCategorySpinner;
    private EditText nameEditText;
    private EditText descriptionEditText;
    private EditText maxGuestsEditText;
    private EditText locationEditText;
    private EditText rangeEditText;
    private CheckBox isPrivateCheckBox;
    private Button addHappening;
    private FragmentAddHappeningBinding binding;

    private List<Event> events = new ArrayList<>();
    private ArrayAdapter<Event> eventTypeAdapter;
    private ArrayAdapter<SubCategory> subCategoryAdapter;

    private FirebaseFirestore db;

    ExecutorService executor = Executors.newSingleThreadExecutor();

    public AddHappeningFragment() {
        // Required empty public constructor
    }

    public static AddHappeningFragment newInstance() {
        return new AddHappeningFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAddHappeningBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        eventTypeSpinner = view.findViewById(R.id.eventTypeSpinner);
        subCategorySpinner = view.findViewById(R.id.subCategorySpinner);
        nameEditText = view.findViewById(R.id.nameEditText);
        descriptionEditText = view.findViewById(R.id.descriptionEditText);
        maxGuestsEditText = view.findViewById(R.id.maxGuestsEditText);
        locationEditText = view.findViewById(R.id.locationEditText);
        rangeEditText = view.findViewById(R.id.rangeEditText);
        isPrivateCheckBox = view.findViewById(R.id.isPrivateCheckBox);
        addHappening = view.findViewById(R.id.addHappening);

        db = FirebaseFirestore.getInstance();

        // Load the current user's data
        loadCurrentUser();

        loadEventsAndSubcategories();

        addHappening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createHappening();
            }
        });
    }

    private User currentUser;

    private void loadCurrentUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = auth.getCurrentUser();

        if (firebaseUser != null) {
            String userId = firebaseUser.getUid();

            db.collection("users").document(userId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            currentUser = document.toObject(User.class);
                        } else {
                            Toast.makeText(getActivity(), "User not found.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Failed to load user data.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void loadEventsAndSubcategories() {
        db.collection("eventTypes")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        events.clear();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            Event event = document.toObject(Event.class);
                            event.setId(document.getId());
                            events.add(event);
                        }

                        eventTypeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_dropdown_item, events);
                        eventTypeSpinner.setAdapter(eventTypeAdapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to load events", Toast.LENGTH_SHORT).show();
                    }
                });

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Event selectedEvent = (Event) parent.getItemAtPosition(position);
                loadSubcategoriesFromEvent(selectedEvent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle case when nothing is selected
            }
        });
    }

    private void loadSubcategoriesFromEvent(Event selectedEvent) {
        List<SubCategory> subCategories = selectedEvent.getSuggestedSubCategories();
        subCategoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_dropdown_item, subCategories);
        subCategorySpinner.setAdapter(subCategoryAdapter);
    }

    private void createHappening() {
        String name = nameEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        int maxGuests = Integer.parseInt(maxGuestsEditText.getText().toString());
        String location = locationEditText.getText().toString();
        int range = Integer.parseInt(rangeEditText.getText().toString());
        boolean isPrivate = isPrivateCheckBox.isChecked();
        Event selectedEvent = (Event) eventTypeSpinner.getSelectedItem();

        Happening happening = new Happening();
        happening.setName(name);
        happening.setDescription(description);
        happening.setMaxGuests(maxGuests);
        happening.setLocation(location);
        happening.setRange(range);
        happening.setPrivate(isPrivate);
        happening.setEventType(selectedEvent);
        happening.setCreatedBy(currentUser);
//        List<Guest> guests = new ArrayList<>();
//        Guest defaultGuest = new Guest("Default Guest", "18", true, false, "None");
//        guests.add(defaultGuest);
        happening.setGuests(new ArrayList<>());

        db.collection("happenings").add(happening)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        String documentId = documentReference.getId();
                        executor.execute(() -> db.collection("happenings").document(documentId).update("id", documentId));
                        FragmentTransition.to(HappeningsPageFragment.newInstance(), getActivity(), true, R.id.list_layout_happenings);
                        Toast.makeText(getActivity(), "New Event type created!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to create happening", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}

