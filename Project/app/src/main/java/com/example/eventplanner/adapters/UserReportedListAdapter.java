package com.example.eventplanner.adapters;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.SingleProductFragment;
import com.example.eventplanner.fragments.reportedUsers.SingleUsersReportedFragment;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.report.UserReport;
import com.example.eventplanner.pdf.PdfGenerator;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class UserReportedListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private ArrayList<DocumentSnapshot> aProduct;
    private FragmentActivity mContext;

    public UserReportedListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> products) {
        super(context, R.layout.users_reported_list_item, products);
        mContext = context;
        aProduct = products;
    }

    @Override
    public int getCount() {
        return aProduct.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.users_reported_list_item, parent, false);
        }

        LinearLayout productItem = convertView.findViewById(R.id.users_reported_list_item);
        TextView userReportEmail = convertView.findViewById(R.id.user_report_email);
        TextView userReportedEmail = convertView.findViewById(R.id.user_reported_email);
        TextView status=convertView.findViewById(R.id.user_status);



        Button viewButton = convertView.findViewById(R.id.view_user_report_button);


        if (document != null) {
           UserReport product = document.toObject(UserReport.class);

            userReportEmail.setText(product.getUserReport());
            userReportedEmail.setText(product.getUserReported().getEmail());
            status.setText(product.getStatus().toString());





            viewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserReport userReport= document.toObject(UserReport.class);

                    FragmentTransition.to(SingleUsersReportedFragment.newInstance(userReport,document.getId()),mContext,true,R.id.list_layout_reports);

                }
            });



        }

        return convertView;
    }


}
