package com.example.eventplanner.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.priceList.SinglePackagePriceListFragment;
import com.example.eventplanner.fragments.priceList.SingleProductPriceListFragment;
import com.example.eventplanner.fragments.products.SingleProductFragment;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.pdf.PdfGenerator;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class PackagePriceListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private ArrayList<DocumentSnapshot> aProduct;
    private FragmentActivity mContext;

    public PackagePriceListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> products) {
        super(context, R.layout.package_price_list_item, products);
        mContext = context;
        aProduct = products;
    }

    @Override
    public int getCount() {
        return aProduct.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.package_price_list_item, parent, false);
        }

        LinearLayout productItem = convertView.findViewById(R.id.package_price_list_item);
        TextView productSerialNumber = convertView.findViewById(R.id.product_serial_number);
        TextView productName = convertView.findViewById(R.id.product_name);
        TextView productPrice = convertView.findViewById(R.id.product_price);
        TextView productDiscount = convertView.findViewById(R.id.product_discount);
        TextView productPriceWithDiscount = convertView.findViewById(R.id.product_price_with_discount);


        Button editButton = convertView.findViewById(R.id.edit_package_button);


        if (document != null) {
            Package product = document.toObject(Package.class);

            productSerialNumber.setText(String.valueOf(position + 1));
            productName.setText(product.getName());
            productPrice.setText(String.valueOf((product.getPrice())*(100/(100-product.getDiscount()))));
            productDiscount.setText(String.valueOf(product.getDiscount()) + "%");
            productPriceWithDiscount.setText(String.valueOf(product.getPrice()));




            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Package product = document.toObject(Package.class);

                    FragmentTransition.to(SinglePackagePriceListFragment.newInstance(product,document.getId()),mContext,true,R.id.list_layout_products);

                }
            });


        }

        return convertView;
    }


}
