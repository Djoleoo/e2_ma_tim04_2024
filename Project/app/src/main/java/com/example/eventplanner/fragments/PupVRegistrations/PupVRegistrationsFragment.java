package com.example.eventplanner.fragments.PupVRegistrations;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EventListAdapter;
import com.example.eventplanner.adapters.PupVRegistrationsListAdapter;
import com.example.eventplanner.databinding.FragmentPupVRegistrationsBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.CategoriesFragment;
import com.example.eventplanner.fragments.events.EventsFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.PUPVRegistration;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class PupVRegistrationsFragment extends Fragment {

    private PupVRegistrationsListAdapter adapter;
    private ArrayList<DocumentSnapshot> mRegistrationDocuments = new ArrayList<>();
    private ArrayList<Category> filterCategories = new ArrayList<>();
    private ArrayList<Event> eventFilters = new ArrayList<>();
    private ArrayList<DocumentSnapshot> filteredList = new ArrayList<>();
    private DatePicker datePicker;

    private FragmentPupVRegistrationsBinding binding;
    private static final String ARG_PARAM = "param";

    public PupVRegistrationsFragment() {
        // Required empty public constructor
    }

    public static PupVRegistrationsFragment newInstance(ArrayList<DocumentSnapshot> registrations) {
        PupVRegistrationsFragment fragment = new PupVRegistrationsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM,registrations);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRegistrationDocuments = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            filteredList.addAll(mRegistrationDocuments);
            adapter = new PupVRegistrationsListAdapter(getActivity(),mRegistrationDocuments);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPupVRegistrationsBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        selectCategories(getActivity());
        select(getActivity());

        Button filterButton = binding.filterButton;

        filterButton.setOnClickListener(v -> {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext(),R.style.FullScreenBottomSheetDialog);
            View dialogView = getLayoutInflater().inflate(R.layout.bottom_filter_sheet, null);
            RadioGroup categoriesRadioGroup = dialogView.findViewById(R.id.categories_radioGroup);
            for (Category category : filterCategories) {
                RadioButton radioButton = new RadioButton(getContext());
                radioButton.setText(category.getName());
                categoriesRadioGroup.addView(radioButton);
            }
            RadioGroup eventTypeRadioGroup = dialogView.findViewById(R.id.eventType_radioGroup);
            for(Event event : eventFilters){
                RadioButton radioButton = new RadioButton(getContext());
                radioButton.setText(event.getName());
                eventTypeRadioGroup.addView(radioButton);
            }

            DatePicker datePicker = dialogView.findViewById(R.id.datePicker);
            datePicker.setCalendarViewShown(true);
            datePicker.setSpinnersShown(false);

            // Initially set no date selected (you can hide this visually by customization if required)
            Calendar today = Calendar.getInstance();
            datePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH), 0, null);
            bottomSheetDialog.setContentView(dialogView);
            bottomSheetDialog.show();

            datePicker.setOnDateChangedListener((view, year, monthOfYear, dayOfMonth) -> {
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.set(year, monthOfYear, dayOfMonth);
                filterByDate(selectedDate.getTime());
                bottomSheetDialog.dismiss();
            });
        });

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        SearchView searchView = root.findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }
        });
        return root;
    }


    private void selectCategories(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            filterCategories.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                filterCategories.add(document.toObject(Category.class));
                            }
                        } else {
                            Toast.makeText(currentActivity,"Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private  void select(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("eventTypes").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            eventFilters.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Event event = document.toObject(Event.class);
                                eventFilters.add(event);
                            }
                        }else{
                            Toast.makeText(currentActivity, "Error getting events", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void filterByDate(Date selectedDate) {
        filteredList.clear();

        Calendar selectedCal = Calendar.getInstance();
        selectedCal.setTime(selectedDate);

        for (DocumentSnapshot document : mRegistrationDocuments) {
            PUPVRegistration registration = document.toObject(PUPVRegistration.class);
            if (registration != null && registration.getCreationTime() != null) {
                Calendar creationCal = Calendar.getInstance();
                creationCal.setTime(registration.getCreationTime());
                if (creationCal.get(Calendar.YEAR) == selectedCal.get(Calendar.YEAR) &&
                        creationCal.get(Calendar.MONTH) == selectedCal.get(Calendar.MONTH) &&
                        creationCal.get(Calendar.DAY_OF_MONTH) == selectedCal.get(Calendar.DAY_OF_MONTH)) {
                    filteredList.add(document);
                }
            }
        }
        adapter.updateList(filteredList);
    }

}