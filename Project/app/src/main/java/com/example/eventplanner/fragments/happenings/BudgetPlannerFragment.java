package com.example.eventplanner.fragments.happenings;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;

import java.util.ArrayList;
import java.util.List;

public class BudgetPlannerFragment extends Fragment {

    private EditText editTextServiceName;
    private Spinner spinnerServiceCategory;
    private EditText editTextAmount;
    private Button addButton;
    private TextView textViewTotalAmount;
    private LinearLayout containerAddedItems;

    private double totalAmount = 0;

    public static BudgetPlannerFragment newInstance() {
        return new BudgetPlannerFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_budget_planer, container, false);

        // Initialize views
        editTextServiceName = view.findViewById(R.id.edit_text_service_name);
        spinnerServiceCategory = view.findViewById(R.id.spinner_service_category);
        editTextAmount = view.findViewById(R.id.edit_text_amount);
        addButton = view.findViewById(R.id.button_add);
        textViewTotalAmount = view.findViewById(R.id.text_view_total_amount);
        containerAddedItems = view.findViewById(R.id.container_added_items);

        // Set up spinner with categories
        setUpSpinner();

        // Set up button click listener
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle add button click
                addBudgetItem();
            }
        });

        return view;
    }

    private void setUpSpinner() {
        // Dummy categories
        List<String> categories = new ArrayList<>();
        categories.add("Accommodation");
        categories.add("Food and catering");
        categories.add("Music and entertainment");
        categories.add("Security");
        categories.add("Other");

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinnerServiceCategory.setAdapter(adapter);
    }

    private void addBudgetItem() {
        // Retrieve inputs
        String serviceName = editTextServiceName.getText().toString();
        String category = spinnerServiceCategory.getSelectedItem().toString();
        String amountString = editTextAmount.getText().toString();

        // Check if amount is empty
        if (amountString.isEmpty()) {
            // Show an error message or handle empty amount
            return; // If amount is empty, do nothing
        }

        // Parse the amount
        double amount;
        try {
            amount = Double.parseDouble(amountString);
        } catch (NumberFormatException e) {
            // Show an error message or handle invalid amount
            return; // If amount cannot be parsed, do nothing
        }

        // Update total amount
        totalAmount += amount;
        textViewTotalAmount.setText("Total Budget: $" + totalAmount);

        // Create a TextView to display the added item
        TextView textViewItem = new TextView(requireContext());
        textViewItem.setTextAppearance(android.R.style.TextAppearance_Medium); // Set text appearance
        textViewItem.setText(serviceName + " - " + category + ": $" + amount);
        textViewItem.setTextSize(16); // Set text size in sp
        textViewItem.setTextColor(ContextCompat.getColor(requireContext(), R.color.black)); // Set text color
        textViewItem.setTypeface(Typeface.DEFAULT_BOLD); // Set text style

// Add margin to the text view
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(0, 0, 0, 8); // Set margin bottom to create space between items
        textViewItem.setLayoutParams(layoutParams);

        containerAddedItems.addView(textViewItem);
    }
}
