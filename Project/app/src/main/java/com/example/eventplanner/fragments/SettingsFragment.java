package com.example.eventplanner.fragments;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.HomeActivity;
import com.example.eventplanner.databinding.FragmentSettingsBinding;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class SettingsFragment extends Fragment {

    private FragmentSettingsBinding binding;
    private static String CHANNEL_1 = "Channel_1";
    private ArrayList<DocumentSnapshot> notifications = new ArrayList<>();
    private NotificationManager notificationManager;
    private int notificationID;
    private  String userId;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.POST_NOTIFICATIONS)!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.POST_NOTIFICATIONS},101);
            }
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentSettingsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        notificationManager = (NotificationManager) requireContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationID = 0;
        notifications.clear();
        TextView textView1 = binding.textView1;
        TextView textView2 = binding.textView2;

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Postavite email adresu u TextView elemente
            textView1.setText("Email: " + user.getEmail());
            textView2.setText("Uid: " + user.getUid());
            userId = user.getUid();

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRef =  db.collection("users").document(user.getUid());
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    User user = documentSnapshot.toObject(User.class);
                    if(user.getUserType().toString().equals("ADMIN")){
                        getAdminNotifications();
                    }else if(user.getUserType().toString().equals("PUP")){
                        getPUPNotifications();
                    }
                }
            });
        }

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void getAdminNotifications(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("notifications")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                               if(document.toObject(Notification.class).getReceiverId() != null && document.toObject(Notification.class).getReceiverId().toString().equals("PbEmjK2rj9YnSQeBmC9K98o6WSw1") && !document.toObject(Notification.class).getRead()){
                                   notifications.add(document);
                               }
                            }
                            if(!notifications.isEmpty()){
                                posalji();
                            }
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    public void getPUPNotifications(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("notifications")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if(document.toObject(Notification.class).getUserType()== Notification.UserType.PUP &&(document.toObject(Notification.class).getReceiverId() == null || document.toObject(Notification.class).getReceiverId().equals(userId)) && !document.toObject(Notification.class).getRead()){
                                    notifications.add(document);
                                }
                            }
                            if(!notifications.isEmpty()){
                                posalji();
                            }
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    private void posalji() {
        // Kreiraj notifikacije
        for (DocumentSnapshot notification : notifications) {
            createNotification(notification.toObject(Notification.class).getTitle(), notification.toObject(Notification.class).getText(),notification.getId());
        }
    }

    private void createNotification(String title, String text,String id) {
        notificationID++;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(requireContext(), CHANNEL_1)
                .setSmallIcon(R.drawable.ic_person)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        notificationManager.notify(notificationID, builder.build());
        executor.execute(()-> {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("notifications").document(id);
            docRef.update("read", true);
        });
    }


}
