package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Guest;
import com.example.eventplanner.model.Happening;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class GuestAdapter extends RecyclerView.Adapter<GuestAdapter.GuestViewHolder> {

    private List<Guest> guests;
    private Happening happening;
    private FirebaseFirestore db;
    private Context context;

    public GuestAdapter(List<Guest> guests, Happening happening, Context context) {
        this.guests = guests;
        this.happening = happening;
        this.context = context;
        this.db = FirebaseFirestore.getInstance();  // Initialize Firestore
    }

    @NonNull
    @Override
    public GuestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_guest, parent, false);
        return new GuestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GuestViewHolder holder, int position) {
        Guest guest = guests.get(position);
        holder.guestNameTextView.setText(guest.getName());
        holder.guestAgeTextView.setText(guest.getAge());
        holder.guestInvitedTextView.setText(guest.isInvited() ? "Invited: Yes" : "Invited: No");
        holder.guestAcceptedTextView.setText(guest.isHasAccepted() ? "Accepted: Yes" : "Accepted: No");
        holder.specialRequirementsTextView.setText(guest.getSpecialRequirements());

        // Handle Edit Button Click
        holder.editButton.setOnClickListener(v -> showEditGuestDialog(guest, position));

        // Handle Delete Button Click
        holder.deleteButton.setOnClickListener(v -> deleteGuest(position));
    }

    @Override
    public int getItemCount() {
        return guests.size();
    }

    public static class GuestViewHolder extends RecyclerView.ViewHolder {
        TextView guestNameTextView;
        TextView guestAgeTextView;
        TextView guestInvitedTextView;
        TextView guestAcceptedTextView;
        TextView specialRequirementsTextView;
        Button editButton;
        Button deleteButton;

        public GuestViewHolder(@NonNull View itemView) {
            super(itemView);
            guestNameTextView = itemView.findViewById(R.id.guest_name_text_view);
            guestAgeTextView = itemView.findViewById(R.id.guest_age_text_view);
            guestInvitedTextView = itemView.findViewById(R.id.guest_invited_text_view);
            guestAcceptedTextView = itemView.findViewById(R.id.guest_accepted_text_view);
            specialRequirementsTextView = itemView.findViewById(R.id.guest_special_requirements_text_view);
            editButton = itemView.findViewById(R.id.btn_edit_guest);
            deleteButton = itemView.findViewById(R.id.btn_delete_guest);
        }
    }

    private void deleteGuest(int position) {
        guests.remove(position);
        notifyItemRemoved(position);
        updateHappeningInFirestore();
        Toast.makeText(context, "Guest deleted", Toast.LENGTH_SHORT).show();
    }

    private void showEditGuestDialog(Guest guest, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.dialog_add_guest, null);
        builder.setView(dialogView);

        EditText guestNameEditText = dialogView.findViewById(R.id.guest_name_edit_text);
        Spinner ageSpinner = dialogView.findViewById(R.id.age_spinner);
        Spinner invitedSpinner = dialogView.findViewById(R.id.invited_spinner);
        Spinner acceptedSpinner = dialogView.findViewById(R.id.accepted_spinner);
        EditText specialRequirementsEditText = dialogView.findViewById(R.id.special_requirements_edit_text);

        guestNameEditText.setText(guest.getName());
        // Assuming the spinners are set up with correct entries and methods to select the right item
        // You would set the selected item here based on the guest's current details

        builder.setPositiveButton("Save", (dialog, which) -> {
            guest.setName(guestNameEditText.getText().toString());
            guest.setAge(ageSpinner.getSelectedItem().toString());
            guest.setInvited(invitedSpinner.getSelectedItem().toString().equals("Yes"));
            guest.setHasAccepted(acceptedSpinner.getSelectedItem().toString().equals("Yes"));
            guest.setSpecialRequirements(specialRequirementsEditText.getText().toString());

            notifyItemChanged(position);
            updateHappeningInFirestore();
            Toast.makeText(context, "Guest details updated", Toast.LENGTH_SHORT).show();
        });

        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void updateHappeningInFirestore() {
        db.collection("happenings").document(happening.getId())
                .set(happening)
                .addOnSuccessListener(aVoid -> {
                    // Successfully updated the Happening in Firestore
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(context, "Failed to update Happening.", Toast.LENGTH_SHORT).show();
                });
    }
}
