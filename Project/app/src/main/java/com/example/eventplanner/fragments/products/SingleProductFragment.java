package com.example.eventplanner.fragments.products;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleCategoryBinding;
import com.example.eventplanner.databinding.FragmentSingleProductBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.CategoriesPageFragment;
import com.example.eventplanner.fragments.categories.SingleCategoryFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.report.CompanyReport;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SingleProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SingleProductFragment extends Fragment {

    private FragmentSingleProductBinding binding;
    private Product mProduct;
    private String documentId;


    private ArrayList<SubCategory> subCategories= new ArrayList<>();


    private ArrayList<Event> eventTypes = new ArrayList<>();
    private ArrayList<Category> categories = new ArrayList<>();




    private static final String ARG_PRODUCT = "product";
    private static final String ARG_PRODUCT_ID = "productId";


    public SingleProductFragment() {
        // Required empty public constructor
    }

    public static SingleProductFragment newInstance(Product product,String documentId) {
        SingleProductFragment fragment = new SingleProductFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        args.putString(ARG_PRODUCT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_PRODUCT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSingleProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        categories.clear();
        eventTypes.clear();
        EditText productName = binding.productNameEdit;
        EditText productDescription = binding.productDescriptionEdit;
        EditText productCategory = binding.productCategoryEdit;
        EditText productSubcategory = binding.productSubcategoryEdit;
        Spinner productSubCategorySpinner=binding.productSubcategorySpinner;
        EditText productPrice = binding.productPriceEdit;
        EditText productDiscount = binding.productDiscountEdit;
        EditText productPriceWithDiscount = binding.productPriceWithDiscountEdit;
        EditText productEventType = binding.productEventTypeEdit;
        CheckBox productAvailable = binding.productAvailableCheck;
        CheckBox productVisible = binding.productVisibleEdit;
        ImageView imageView = binding.productImageSingle;
        Button editButton = binding.buttonEditProduct;
        Button submitButton = binding.buttonConfirmProduct;
        Button cancelButton = binding.buttonCancelProduct;
        Button addImageButton=binding.addImage;
        Button deleteImageButton=binding.deleteImage;
        Spinner eventTypeSpinner=binding.eventTypeSpinner;
        Button addEventType=binding.addEventtype;
        Button deleteEventType=binding.deleteEventtype;
        Button reportProduct=binding.buttonReportProduct;
        Button submitReport=binding.buttonSubmitReport;
        EditText reportReason=binding.productReportEdit;
        TextView reportReasonLabel=binding.productReportLabel;


        TextView companyEmail = root.findViewById(R.id.company_email);
        TextView companyName = root.findViewById(R.id.company_name);
        TextView companyAddress = root.findViewById(R.id.company_address);
        TextView companyNumber = root.findViewById(R.id.company_number);
        TextView companyDescription = root.findViewById(R.id.company_description);
        TextView monday = root.findViewById(R.id.workhours_monday);
        TextView tuesday = root.findViewById(R.id.workhours_tuesday);
        TextView wednesday = root.findViewById(R.id.workhours_wednesday);
        TextView thursday = root.findViewById(R.id.workhours_thursday);
        TextView friday = root.findViewById(R.id.workhours_friday);
        TextView saturday = root.findViewById(R.id.workhours_saturday);
        TextView sunday = root.findViewById(R.id.workhours_sunday);


        TextView userName = root.findViewById(R.id.user_email);


        submitButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);
        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.album);
        images.add(R.drawable.camera);
        images.add(R.drawable.photographing);


        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            User currentUser = documentSnapshot.toObject(User.class);
                            if (currentUser != null && currentUser.getUserType() == User.UserType.OD) {
                                editButton.setVisibility(View.GONE);
                            }
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Handle any errors here
                        Toast.makeText(getActivity(), "Failed to load user information", Toast.LENGTH_SHORT).show();
                    });
        }









        selectCategories(new AddProductFragment.OnCategoriesLoadedListener() {
            @Override
            public void onCategoriesLoaded() {
                // Kreiraj adapter nakon što su kategorije učitane
                //setupSpinner();
            }
        });

        selectSubCategories(new AddProductFragment.OnSubCategoriesLoadedListener() {
            @Override
            public void onSubCategoriesLoaded() {
                setupSubCategorySpinner();
            }
        });

        selectEventType(new AddProductFragment.OnEventTypeLoadedListener() {
            @Override
            public void onEventTypesLoaded() {
                setupEventTypeSpinner();
            }
        });

        if (mProduct != null) {
            productName.setText(mProduct.getName());
            productDescription.setText(mProduct.getDescription());
            productCategory.setText(mProduct.getCategory().getName());
            productSubcategory.setText(mProduct.getSubcategory().getName());
            productPrice.setText(String.valueOf(mProduct.getPrice()));
            productDiscount.setText(String.valueOf(mProduct.getDiscount()));
            productPriceWithDiscount.setText(String.valueOf(mProduct.getPriceWithDiscount()));

            companyEmail.setText(mProduct.getCompany().getEmail());
            companyAddress.setText(mProduct.getCompany().getAddress());
            companyName.setText(mProduct.getCompany().getName());
            companyDescription.setText(mProduct.getCompany().getDescription());
            companyNumber.setText(mProduct.getCompany().getNumber());
            userName.setText(mProduct.getUserEmail());

            monday.setText(mProduct.getCompany().getWorkHours().getMonday());
            tuesday.setText(mProduct.getCompany().getWorkHours().getTuesday());
            wednesday.setText(mProduct.getCompany().getWorkHours().getWednesday());
            thursday.setText(mProduct.getCompany().getWorkHours().getThursday());
            friday.setText(mProduct.getCompany().getWorkHours().getFriday());
            saturday.setText(mProduct.getCompany().getWorkHours().getSaturday());
            sunday.setText(mProduct.getCompany().getWorkHours().getSunday());


            if (mProduct.getEventType() != null) {
                StringBuilder eventTypeText = new StringBuilder();
                for (Event type : mProduct.getEventType()) {
                    eventTypeText.append(type).append("\n");
                }
                productEventType.setText(eventTypeText.toString());
            }

            productAvailable.setChecked(mProduct.isAvailable());
            productVisible.setChecked(mProduct.isVisible());
        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productName.setEnabled(true);
                productDescription.setEnabled(true);
                productPrice.setEnabled(true);
                productDiscount.setEnabled(true);
                productPriceWithDiscount.setEnabled(true);

                productEventType.setEnabled(false);
                productAvailable.setEnabled(true);
                productVisible.setEnabled(true);
                submitButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                editButton.setVisibility(View.GONE);

                reportProduct.setVisibility(View.GONE);
                submitReport.setVisibility(View.GONE);

                reportReason.setVisibility(View.GONE);
                reportReasonLabel.setVisibility(View.GONE);



            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productName.setEnabled(false);
                productDescription.setEnabled(false);
                productPrice.setEnabled(false);
                productDiscount.setEnabled(false);
                productPriceWithDiscount.setEnabled(false);
                productEventType.setEnabled(false);
                productAvailable.setEnabled(false);
                productVisible.setEnabled(false);
                submitButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
                addImageButton.setVisibility(View.GONE);
                deleteImageButton.setVisibility(View.GONE);
                productSubcategory.setVisibility(View.VISIBLE);
                productSubCategorySpinner.setVisibility(View.GONE);
                eventTypeSpinner.setVisibility(View.GONE);
                addEventType.setVisibility(View.GONE);
                deleteEventType.setVisibility(View.GONE);

                reportProduct.setVisibility(View.VISIBLE);
                submitReport.setVisibility(View.GONE);

                reportReason.setVisibility(View.GONE);
                reportReasonLabel.setVisibility(View.GONE);

            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selectedCategoryName = productCategory.getText().toString();
                Category selectedCategory = null;
                for (Category category : categories) {
                    if (category.getName().equals(selectedCategoryName)) {
                        selectedCategory = category;
                        break;
                    }
                }
                String selectedSubCategoryName = productSubcategory.getText().toString();
                SubCategory selectedSubCategory = null;
                for (SubCategory subCategory : subCategories) {
                    if (subCategory.getName().equals(selectedSubCategoryName)) {
                        selectedSubCategory = subCategory;
                        break;
                    }
                }
                String selectedEventName = productEventType.getText().toString();
                ArrayList<Event> selectedEvents=new ArrayList<>();
                Event selectedEvent = null;
                for (Event event : eventTypes) {
                    if (event.getName().equals(selectedEventName)) {
                        selectedEvent = event;
                        selectedEvents.add(selectedEvent);
                        break;
                    }
                }


                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                Product product = new Product(mProduct.getCategory(),mProduct.getSubcategory(),productName.getText().toString(),productDescription.getText().toString(),Double.parseDouble(productPrice.getText().toString()),Double.parseDouble(productDiscount.getText().toString()), mProduct.getGallerys(), mProduct.getEventTypes(),productAvailable.isChecked(),productVisible.isChecked(),currentUser.getEmail(),mProduct.getCompany());
                showEditConfirmationDialog(product);
            }
        });

        reportProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportProduct.setVisibility(View.GONE);
                submitReport.setVisibility(View.VISIBLE);

                reportReason.setVisibility(View.VISIBLE);
                reportReasonLabel.setVisibility(View.VISIBLE);

                reportReason.setEnabled(true);


                editButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                submitButton.setVisibility(View.GONE);
            }
        });



        submitReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*reportProduct.setVisibility(View.VISIBLE);
                submitReport.setVisibility(View.GONE);

                reportReason.setVisibility(View.GONE);
                reportReasonLabel.setVisibility(View.GONE);
                viewCompany.setVisibility(View.VISIBLE);


                editButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.GONE);
                submitButton.setVisibility(View.GONE);*/
                // Retrieve the current user's email
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                if (currentUser == null) {
                    Toast.makeText(getActivity(), "User not logged in", Toast.LENGTH_SHORT).show();
                    return;
                }
                String userReportEmail = currentUser.getEmail();

                // Get the report reason from EditText
                String reportReasonText = reportReason.getText().toString().trim();
                if (reportReasonText.isEmpty()) {
                    Toast.makeText(getActivity(), "Please enter a report reason", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Get the current time
                Date reportTime = new Date();

                // Create a CompanyReport object
                CompanyReport companyReport = new CompanyReport(
                        mProduct.getCompany(),  // Company reported
                        mProduct.getCompany().getUser(),     // User reported
                        userReportEmail,        // User report (email of the current user)
                        reportTime,             // Time of the report
                        reportReasonText,       // Reason for the report
                        CompanyReport.Status.REPORTED  // Initial status
                );

                // Save the CompanyReport to Firestore
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("companyReports")
                        .add(companyReport)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Toast.makeText(getActivity(), "Report submitted successfully", Toast.LENGTH_SHORT).show();

                                // Reset the report UI
                                reportProduct.setVisibility(View.VISIBLE);
                                submitReport.setVisibility(View.GONE);
                                reportReason.setVisibility(View.GONE);
                                reportReasonLabel.setVisibility(View.GONE);

                                editButton.setVisibility(View.VISIBLE);
                                cancelButton.setVisibility(View.GONE);
                                submitButton.setVisibility(View.GONE);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Failed to submit report", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });




        productPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Not needed
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    try {
                        // Attempt to parse the input as a decimal number
                        Double.parseDouble(s.toString());
                        productPrice.setText(s.toString());
                    } catch (NumberFormatException e) {
                        productPrice.setText("");
                    }
                }
            }
        });

// Method to validate the input as a decimal number




        return root;
    }



    //setup za spinnere


    /*private void setupSpinner() {
        Spinner categorySpinner = binding.categoryBtnProduct;
        ArrayAdapter<String> categoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getCategoryNames());
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryArrayAdapter);
    }*/

    private void setupSubCategorySpinner(){
        Spinner subCategorySpinner=binding.productSubcategorySpinner;
        ArrayAdapter<String> subCategoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getSubCategoryNames());
        subCategoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subCategorySpinner.setAdapter(subCategoryArrayAdapter);
    }

    private void setupEventTypeSpinner(){
        Spinner eventTypeSpinner=binding.eventTypeSpinner;
        ArrayAdapter<String> eventTypeArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getEventTypeNames());
        eventTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventTypeSpinner.setAdapter(eventTypeArrayAdapter);
    }
    private void selectCategories(AddProductFragment.OnCategoriesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            categories.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Category category = document.toObject(Category.class);
                                categories.add(category);
                            }
                            listener.onCategoriesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void selectSubCategories(AddProductFragment.OnSubCategoriesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("subCategories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            subCategories.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                SubCategory category = document.toObject(SubCategory.class);
                                subCategories.add(category);
                            }
                            listener.onSubCategoriesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void selectEventType(AddProductFragment.OnEventTypeLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("eventTypes").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            eventTypes.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Event event = document.toObject(Event.class);
                                eventTypes.add(event);
                            }
                            listener.onEventTypesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting events", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private ArrayList<String> getCategoryNames() {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
        }
        return categoryNames;
    }

    private ArrayList<String> getSubCategoryNames() {
        ArrayList<String> subCategoryNames = new ArrayList<>();
        for (SubCategory subCategory : subCategories) {
            subCategoryNames.add(subCategory.getName());

        }
        return subCategoryNames;
    }

    private ArrayList<String> getEventTypeNames() {
        ArrayList<String> eventTypeNames = new ArrayList<>();
        for (Event event : eventTypes) {
            eventTypeNames.add(event.getName());

        }
        return eventTypeNames;
    }

    interface OnCategoriesLoadedListener {
        void onCategoriesLoaded();
    }

    interface OnSubCategoriesLoadedListener {
        void onSubCategoriesLoaded();
    }

    interface OnEventTypeLoadedListener {
        void onEventTypesLoaded();
    }
    private void showEditConfirmationDialog(Product product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit Product");
        builder.setMessage("Are you sure you want to edit this product?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("products").document(documentId);
                docRef.set(product)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {

                                FragmentTransition.to(ProductsPageFragment.newInstance(),getActivity(),true,R.id.list_layout_products);
                                Toast.makeText(getActivity(), "Product updated!", Toast.LENGTH_SHORT).show();
                                //FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                //addBtn.setVisibility(View.VISIBLE);
                                //MaterialButtonToggleGroup toggleGroup = requireActivity().findViewById(R.id.toggle_group);
                                //toggleGroup.check(R.id.button_categories); // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }




}