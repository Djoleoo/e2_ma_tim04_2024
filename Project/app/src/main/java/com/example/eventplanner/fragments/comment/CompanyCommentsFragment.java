package com.example.eventplanner.fragments.comment;

import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CommentsAdapter;
import com.example.eventplanner.model.Comment;
import com.example.eventplanner.model.Notification;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CompanyCommentsFragment extends Fragment {

    private RecyclerView recyclerView;
    private CommentsAdapter adapter;
    private ArrayList<Comment> comments;
    private FirebaseFirestore db;
    private FirebaseAuth auth;

    private Button btnSortByDate;
    private Button btnSortByRating;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company_comments, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_comments);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        btnSortByDate = view.findViewById(R.id.btn_sort_by_date);
        btnSortByRating = view.findViewById(R.id.btn_sort_by_rating);

        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();

        loadComments();

        btnSortByDate.setOnClickListener(v -> sortCommentsByDate());
        btnSortByRating.setOnClickListener(v -> sortCommentsByRating());

        return view;
    }

    private void loadComments() {
        String currentUserEmail = auth.getCurrentUser().getEmail();

        // Query the companies collection to find the company where the user is working
        db.collection("companies")
                .whereEqualTo("user.email", currentUserEmail)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        String companyId = queryDocumentSnapshots.getDocuments().get(0).getString("email");

                        // Query the comments collection to get comments for the company
                        db.collection("comments")
                                .whereEqualTo("companyID", companyId)
                                .get()
                                .addOnSuccessListener(querySnapshot -> {
                                    comments = new ArrayList<>();
                                    for (com.google.firebase.firestore.DocumentSnapshot document : querySnapshot.getDocuments()) {
                                        Comment comment = document.toObject(Comment.class);
                                        comment.setId(document.getId());  // Set the document ID to the comment
                                        comments.add(comment);
                                    }
                                    adapter = new CommentsAdapter(comments, this::reportComment);
                                    recyclerView.setAdapter(adapter);
                                })
                                .addOnFailureListener(e -> Log.e("CompanyCommentsFragment", "Error loading comments", e));
                    }
                })
                .addOnFailureListener(e -> Log.e("CompanyCommentsFragment", "Error finding company", e));
    }

    private void sortCommentsByDate() {
        Collections.sort(comments, new Comparator<Comment>() {
            @Override
            public int compare(Comment c1, Comment c2) {
                return c2.getDate().compareTo(c1.getDate());  // Sort by date descending
            }
        });
        adapter.updateComments(comments);
    }
    private void sortCommentsByRating() {
        Collections.sort(comments, new Comparator<Comment>() {
            @Override
            public int compare(Comment c1, Comment c2) {
                // Parse the ratings to floats for proper comparison
                return Float.compare(Float.parseFloat(c2.getRating()), Float.parseFloat(c1.getRating()));  // Sort by rating descending
            }
        });
        adapter.updateComments(comments);
    }
    private void reportComment(Comment comment) {
        // Show a dialog to get the reason for the report
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Report Comment");

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("Report", (dialog, which) -> {
            String reason = input.getText().toString();
            String reportedBy = auth.getCurrentUser().getEmail();  // Get the current user's email

            // Update the comment with the report information
            db.collection("comments")
                    .document(comment.getId())  // Use the document ID
                    .update("reported", true, "reportReason", reason, "reportStatus", "reported", "reportedBy", reportedBy)
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(getContext(), "Comment reported successfully", Toast.LENGTH_SHORT).show();
                        notifyAdmin(comment, reason, reportedBy);
                    })
                    .addOnFailureListener(e -> Log.e("CompanyCommentsFragment", "Error reporting comment", e));
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    private void notifyAdmin(Comment comment, String reason, String reportedBy) {
        // Send notification to the admin
        Notification notification = new Notification(
                "Comment Reported",
                "A comment has been reported by " + reportedBy + ". Reason: " + reason,
                "aleksandarssrajer@gmail.com",
                false,
                Notification.UserType.ADMIN
        );

        db.collection("notifications")
                .add(notification)
                .addOnSuccessListener(documentReference -> Log.d("CompanyCommentsFragment", "Notification sent to admin"))
                .addOnFailureListener(e -> Log.e("CompanyCommentsFragment", "Error sending notification to admin", e));
    }
}
