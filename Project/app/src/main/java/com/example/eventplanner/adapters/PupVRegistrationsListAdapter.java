package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.PupVRegistrations.SinglePupVRegistrationFragment;
import com.example.eventplanner.model.PUPVRegistration;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class PupVRegistrationsListAdapter extends ArrayAdapter<DocumentSnapshot> implements Filterable {

    private ArrayList<DocumentSnapshot> mDocuments;
    private ArrayList<DocumentSnapshot> mDocumentsFull;
    private FragmentActivity mContext;

    public PupVRegistrationsListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> documents){
        super(context, R.layout.pupv_registration_list_item,documents);
        mContext = context;
        mDocuments = new ArrayList<>(documents);
        mDocumentsFull = new ArrayList<>(documents);

    }

    @Override
    public int getCount() {
        return mDocuments.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return mDocuments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        DocumentSnapshot document = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.pupv_registration_list_item,
                    parent, false);
        }
        LinearLayout eventItem = convertView.findViewById(R.id.event_list_item);
        TextView pupvName = convertView.findViewById(R.id.pupv_name);
        Button viewButton = convertView.findViewById(R.id.view_button);

        if(document != null){
            String pupvNameText = document.getString("companyName");
            pupvName.setText(pupvNameText);
        }

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PUPVRegistration registration = document.toObject(PUPVRegistration.class);
                FragmentTransition.to(SinglePupVRegistrationFragment.newInstance(registration, document.getId()),mContext,true,R.id.list_layout);
            }
        });

        return convertView;
    }

    public void updateList(ArrayList<DocumentSnapshot> newDocuments) {
        mDocuments.clear();
        mDocuments = newDocuments;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DocumentSnapshot> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mDocumentsFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (DocumentSnapshot document : mDocumentsFull) {
                    String companyName = document.getString("companyName").toLowerCase();
                    String companyEmail = document.getString("companyEmail").toLowerCase();

                    if (companyName.contains(filterPattern) || companyEmail.contains(filterPattern)) {
                        filteredList.add(document);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mDocuments.clear();
            mDocuments.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

}
