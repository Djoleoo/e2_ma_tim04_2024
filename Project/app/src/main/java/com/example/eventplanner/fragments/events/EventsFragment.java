package com.example.eventplanner.fragments.events;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategoryListAdapter;
import com.example.eventplanner.adapters.EventListAdapter;
import com.example.eventplanner.databinding.FragmentCategoriesBinding;
import com.example.eventplanner.databinding.FragmentEventsBinding;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class EventsFragment extends Fragment {

    private EventListAdapter adapter;
    private ArrayList<DocumentSnapshot> mEventDocuments = new ArrayList<>();

    private FragmentEventsBinding binding;

    private static final String ARG_PARAM = "param";

    public EventsFragment() {
        // Required empty public constructor
    }

    public static EventsFragment newInstance(ArrayList<DocumentSnapshot> eventDocuments) {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, eventDocuments);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEventDocuments = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new EventListAdapter(getActivity(), mEventDocuments);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        return root;
    }

}