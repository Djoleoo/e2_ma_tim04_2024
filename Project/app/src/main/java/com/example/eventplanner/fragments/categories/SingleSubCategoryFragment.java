package com.example.eventplanner.fragments.categories;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleSubCategoryBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.SubCategorySuggestion;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class SingleSubCategoryFragment extends Fragment {

    private FragmentSingleSubCategoryBinding binding;
    private SubCategory mSubCategory;
    private ArrayList<Category> categories = new ArrayList<>();
    private String documentId;
    private static final String ARG_SUBCATEGORY = "subcategory";
    private static final String ARG_EVENT_ID = "subCategoryId";
    ExecutorService executor = Executors.newSingleThreadExecutor();

    public SingleSubCategoryFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SingleSubCategoryFragment newInstance(SubCategory subCategory,String documentId) {
        SingleSubCategoryFragment fragment = new SingleSubCategoryFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_SUBCATEGORY, subCategory);
        args.putString(ARG_EVENT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSubCategory = getArguments().getParcelable(ARG_SUBCATEGORY);
            documentId = getArguments().getString(ARG_EVENT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSingleSubCategoryBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        EditText subCategoryName = binding.subCategoryName;
        TextView subCategoryDescription = binding.subcategoryDescription;
        Button editButton = binding.buttonEdit;
        Button submitButton = binding.buttonConfirm;
        Button cancelButton = binding.buttonCancel;

        submitButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);
        Spinner categorySpinner = binding.categoryBtn;
        selectCategories(new OnCategoriesLoadedListener() {
            @Override
            public void onCategoriesLoaded() {
                setupSpinner();
            }
        });
        if(mSubCategory != null) {
            subCategoryName.setText(mSubCategory.getName());
            subCategoryDescription.setText(mSubCategory.getDescription());
        }
        Spinner typeSpinner = binding.typeButton;
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.subCategory_role));
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(arrayAdapter);

        if(mSubCategory.getStatus().toString().equalsIgnoreCase("Product")){
            int selectedIndex = arrayAdapter.getPosition("Product");
            typeSpinner.setSelection(selectedIndex);
        }else if(mSubCategory.getStatus().toString().equalsIgnoreCase("Service")){
            int selectedIndex = arrayAdapter.getPosition("Service");
            typeSpinner.setSelection(selectedIndex);
        }
        typeSpinner.setEnabled(false);

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subCategoryName.setEnabled(true);
                typeSpinner.setEnabled(true);
                categorySpinner.setEnabled(true);
                subCategoryDescription.setEnabled(true);
                submitButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                editButton.setVisibility(View.GONE);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subCategoryName.setEnabled(false);
                subCategoryDescription.setEnabled(false);
                typeSpinner.setEnabled(false);
                categorySpinner.setEnabled(false);
                submitButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedCategoryName = (String) categorySpinner.getSelectedItem();
                Category selectedCategory = null;
                for (Category category : categories) {
                    if (category.getName().equals(selectedCategoryName)) {
                        selectedCategory = category;
                        break;
                    }
                }
                String selectedStatus = (String) typeSpinner.getSelectedItem();
                SubCategory.Status status = SubCategory.Status.valueOf(selectedStatus.toUpperCase());
                SubCategory subCategory = new SubCategory(subCategoryName.getText().toString(),subCategoryDescription.getText().toString(),selectedCategory,status);
                showEditConfirmationDialog(subCategory);
                subCategoryName.setEnabled(false);
                subCategoryDescription.setEnabled(false);
                submitButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
            }
        });
        return root;
    }

    private void showEditConfirmationDialog(SubCategory subCategory) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit SubCategory");
        builder.setMessage("Are you sure you want to edit this subCategory?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("subCategories").document(documentId);
                docRef.set(subCategory)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                executor.execute(()->{
                                    Notification notification = new Notification("SubCategory change!", "There was a SubCategory change ,check it out!", null, false, Notification.UserType.PUP);
                                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                                    db.collection("notifications").add(notification);
                                });
                                FragmentTransition.to(CategoriesPageFragment.newInstance(),getActivity(),true,R.id.list_layout);
                                Toast.makeText(getActivity(), "SubCategory updated!", Toast.LENGTH_SHORT).show();
                                FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                addBtn.setVisibility(View.VISIBLE);
                                MaterialButtonToggleGroup toggleGroup = requireActivity().findViewById(R.id.toggle_group);
                                toggleGroup.check(R.id.button_categories); // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void selectCategories(OnCategoriesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            categories.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Category category = document.toObject(Category.class);
                                categories.add(category);
                            }
                            listener.onCategoriesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private ArrayList<String> getCategoryNames() {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
        }
        return categoryNames;
    }

    private void setupSpinner() {
        ArrayList<String>categoryNames = getCategoryNames();
        Spinner categorySpinner = binding.categoryBtn;
        ArrayAdapter<String> categoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                categoryNames);
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryArrayAdapter);
        int index = categoryNames.indexOf(mSubCategory.getCategory().getName());
        // Postavlja izabrani element u spinner na osnovu pronađenog indeksa
        if (index != -1) {
            categorySpinner.setSelection(index);
        }
        categorySpinner.setEnabled(false);
    }

    interface OnCategoriesLoadedListener {
        void onCategoriesLoaded();
    }


}