package com.example.eventplanner.fragments.products;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleCategoryBinding;
import com.example.eventplanner.databinding.FragmentSingleProductBinding;
import com.example.eventplanner.databinding.FragmentSingleServiceBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.SingleCategoryFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.report.CompanyReport;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SingleProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SingleServiceFragment extends Fragment {

    private FragmentSingleServiceBinding binding;
    private Service mService;
    private String documentId;

    private ArrayList<Category> categories = new ArrayList<>();
    private ArrayList<SubCategory> subCategories= new ArrayList<>();
    private ArrayList<Event> eventTypes = new ArrayList<>();
    private static final String ARG_SERVICE_ID = "serviceId";


    private static final String ARG_PRODUCT = "product";


    public SingleServiceFragment() {
        // Required empty public constructor
    }

    public static SingleServiceFragment newInstance(Service service,String documentId) {
        SingleServiceFragment fragment = new SingleServiceFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, service);
        args.putString(ARG_SERVICE_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mService = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_SERVICE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSingleServiceBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        EditText serviceName = binding.serviceNameEdit;
        EditText serviceDescription = binding.serviceDescriptionEdit;
        EditText serviceCategory = binding.serviceCategoryEdit;
        EditText serviceSubcategory = binding.serviceSubcategoryEdit;
        Spinner serviceSubCategorySpinner=binding.serviceSubcategorySpinner;
        EditText servicePrice = binding.servicePriceEdit;
        EditText serviceDiscount = binding.serviceDiscountEdit;
        EditText servicePriceWithDiscount = binding.servicePriceWithDiscountEdit;
        ImageView imageView=binding.serviceImageSingle;
        EditText serviceEventType = binding.serviceEventTypeEdit;
        CheckBox serviceAvailable = binding.serviceAvailableCheck;
        CheckBox serviceVisible = binding.serviceVisibleEdit;
        EditText serviceLocation = binding.serviceLocationEdit;
        EditText serviceHours = binding.serviceHoursEdit;
        EditText serviceSpecificity = binding.serviceSpecificityEdit;
        EditText servicePersons = binding.servicePersonsEdit;
        CheckBox serviceAutomaticAcceptance = binding.serviceAutomaticAcceptanceEdit;
        Spinner eventTypeSpinner=binding.eventTypeSpinner;
        Button addEventType=binding.addEventtype;
        Button deleteEventType=binding.deleteEventtype;
        Spinner personSpinner=binding.personsSpinner;
        Button addPerson=binding.addPerson;
        Button deletePerson=binding.deletePerson;

        EditText serviceDeadlineReservation = binding.serviceDeadlineReservationEdit;
        EditText serviceDeadlineCancel = binding.serviceDeadlineCancelEdit;

        Button editButton = binding.buttonEditProduct;
        Button submitButton = binding.buttonConfirmProduct;
        Button cancelButton = binding.buttonCancelProduct;

        Button addImageButton=binding.addImageService;
        Button deleteImageButton=binding.deleteImageService;

        submitButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);
        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.album);
        images.add(R.drawable.camera);
        images.add(R.drawable.photographing);


        //new
        Button reportProduct=binding.buttonReportProduct;
        Button submitReport=binding.buttonSubmitReport;
        EditText reportReason=binding.productReportEdit;
        TextView reportReasonLabel=binding.productReportLabel;


        TextView companyEmail = root.findViewById(R.id.company_email);
        TextView companyName = root.findViewById(R.id.company_name);
        TextView companyAddress = root.findViewById(R.id.company_address);
        TextView companyNumber = root.findViewById(R.id.company_number);
        TextView companyDescription = root.findViewById(R.id.company_description);
        TextView monday = root.findViewById(R.id.workhours_monday);
        TextView tuesday = root.findViewById(R.id.workhours_tuesday);
        TextView wednesday = root.findViewById(R.id.workhours_wednesday);
        TextView thursday = root.findViewById(R.id.workhours_thursday);
        TextView friday = root.findViewById(R.id.workhours_friday);
        TextView saturday = root.findViewById(R.id.workhours_saturday);
        TextView sunday = root.findViewById(R.id.workhours_sunday);


        TextView userName = root.findViewById(R.id.user_email);




        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            User currentUser = documentSnapshot.toObject(User.class);
                            if (currentUser != null && currentUser.getUserType() == User.UserType.OD) {
                                editButton.setVisibility(View.GONE);
                            }
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Handle any errors here
                        Toast.makeText(getActivity(), "Failed to load user information", Toast.LENGTH_SHORT).show();
                    });
        }


        if (mService != null) {
            serviceName.setText(mService.getName());
            serviceDescription.setText(mService.getDescription());
            serviceCategory.setText(mService.getCategory().getName());
            serviceSubcategory.setText(mService.getSubcategory().getName());
            servicePrice.setText(String.valueOf(mService.getPricePerHour()));
            serviceDiscount.setText(String.valueOf(mService.getDiscount()));
            servicePriceWithDiscount.setText(String.valueOf(mService.getPriceTotal()));
            serviceSpecificity.setText(mService.getSpecificity());
            serviceHours.setText(String.valueOf(mService.getHours()));
            serviceLocation.setText(mService.getLocation());
            serviceDeadlineReservation.setText(String.valueOf(mService.getDeadlineForReservation()));
            serviceDeadlineCancel.setText(String.valueOf(mService.getDeadlineForCancel()));

            //new
            companyEmail.setText(mService.getCompany().getEmail());
            companyAddress.setText(mService.getCompany().getAddress());
            companyName.setText(mService.getCompany().getName());
            companyDescription.setText(mService.getCompany().getDescription());
            companyNumber.setText(mService.getCompany().getNumber());
            userName.setText(mService.getUserEmail());

            monday.setText(mService.getCompany().getWorkHours().getMonday());
            tuesday.setText(mService.getCompany().getWorkHours().getTuesday());
            wednesday.setText(mService.getCompany().getWorkHours().getWednesday());
            thursday.setText(mService.getCompany().getWorkHours().getThursday());
            friday.setText(mService.getCompany().getWorkHours().getFriday());
            saturday.setText(mService.getCompany().getWorkHours().getSaturday());
            sunday.setText(mService.getCompany().getWorkHours().getSunday());


            if (mService.getEventTypes() != null) {
                StringBuilder eventTypeText = new StringBuilder();
                for (Event type : mService.getEventTypes()) {
                    eventTypeText.append(type).append("\n");
                }
                serviceEventType.setText(eventTypeText.toString());
            }

            if (mService.getPersons() != null) {
                StringBuilder personsText = new StringBuilder();
                for (String person : mService.getPersons()) {
                    personsText.append(person).append("\n");
                }

                servicePersons.setText(personsText.toString());
            }

            serviceAvailable.setChecked(mService.isAvailable());
            serviceVisible.setChecked(mService.isVisible());
            serviceAutomaticAcceptance.setChecked(mService.isAutomaticAcceptance());

        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceName.setEnabled(true);
                serviceDescription.setEnabled(true);

                servicePrice.setEnabled(true);
                serviceDiscount.setEnabled(true);
                servicePriceWithDiscount.setEnabled(true);
                serviceSpecificity.setEnabled(true);
                serviceHours.setEnabled(true);
                serviceLocation.setEnabled(true);
                serviceDeadlineReservation.setEnabled(true);
                serviceDeadlineCancel.setEnabled(true);


                serviceAvailable.setEnabled(true);
                serviceVisible.setEnabled(true);
                serviceAutomaticAcceptance.setEnabled(true);



                submitButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                editButton.setVisibility(View.GONE);


                reportProduct.setVisibility(View.GONE);
                submitReport.setVisibility(View.GONE);

                reportReason.setVisibility(View.GONE);
                reportReasonLabel.setVisibility(View.GONE);




            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceName.setEnabled(false);
                serviceDescription.setEnabled(false);
                serviceSubcategory.setEnabled(false);
                servicePrice.setEnabled(false);
                serviceDiscount.setEnabled(false);
                servicePriceWithDiscount.setEnabled(false);
                serviceSpecificity.setEnabled(false);
                serviceHours.setEnabled(false);
                serviceLocation.setEnabled(false);
                serviceDeadlineReservation.setEnabled(false);
                serviceDeadlineCancel.setEnabled(false);

                serviceEventType.setEnabled(false);
                servicePersons.setEnabled(false);
                serviceAvailable.setEnabled(false);
                serviceVisible.setEnabled(false);
                serviceAutomaticAcceptance.setEnabled(false);
                submitButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
                addImageButton.setVisibility(View.GONE);
                deleteImageButton.setVisibility(View.GONE);
                serviceSubCategorySpinner.setVisibility(View.GONE);
                serviceSubcategory.setVisibility(View.VISIBLE);
                eventTypeSpinner.setVisibility(View.GONE);
                addEventType.setVisibility(View.GONE);
                deleteEventType.setVisibility(View.GONE);
                personSpinner.setVisibility(View.GONE);
                addPerson.setVisibility(View.GONE);
                deletePerson.setVisibility(View.GONE);

                reportProduct.setVisibility(View.VISIBLE);
                submitReport.setVisibility(View.GONE);

                reportReason.setVisibility(View.GONE);
                reportReasonLabel.setVisibility(View.GONE);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Service service = new Service(mService.getCategory(),mService.getSubcategory(),serviceName.getText().toString(),serviceDescription.getText().toString(),mService.getGallery(),serviceSpecificity.getText().toString(),Double.parseDouble(servicePrice.getText().toString()),Integer.parseInt(serviceHours.getText().toString()),serviceLocation.getText().toString(),Double.parseDouble(serviceDiscount.getText().toString()),mService.getPersons(),mService.getEventTypes(),Integer.parseInt(serviceDeadlineReservation.getText().toString()),Integer.parseInt(serviceDeadlineCancel.getText().toString()),serviceAutomaticAcceptance.isChecked(),serviceAvailable.isChecked(),serviceVisible.isChecked(),mService.getUserEmail(),mService.getCompany());
                showEditConfirmationDialog(service);
            }
        });

        reportProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportProduct.setVisibility(View.GONE);
                submitReport.setVisibility(View.VISIBLE);

                reportReason.setVisibility(View.VISIBLE);
                reportReasonLabel.setVisibility(View.VISIBLE);

                reportReason.setEnabled(true);


                editButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                submitButton.setVisibility(View.GONE);
            }
        });

        submitReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                if (currentUser == null) {
                    Toast.makeText(getActivity(), "User not logged in", Toast.LENGTH_SHORT).show();
                    return;
                }
                String userReportEmail = currentUser.getEmail();

                // Get the report reason from EditText
                String reportReasonText = reportReason.getText().toString().trim();
                if (reportReasonText.isEmpty()) {
                    Toast.makeText(getActivity(), "Please enter a report reason", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Get the current time
                Date reportTime = new Date();

                // Create a CompanyReport object
                CompanyReport companyReport = new CompanyReport(
                        mService.getCompany(),  // Company reported
                        mService.getCompany().getUser(),     // User reported
                        userReportEmail,        // User report (email of the current user)
                        reportTime,             // Time of the report
                        reportReasonText,       // Reason for the report
                        CompanyReport.Status.REPORTED  // Initial status
                );

                // Save the CompanyReport to Firestore
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("companyReports")
                        .add(companyReport)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Toast.makeText(getActivity(), "Report submitted successfully", Toast.LENGTH_SHORT).show();

                                // Reset the report UI
                                reportProduct.setVisibility(View.VISIBLE);
                                submitReport.setVisibility(View.GONE);
                                reportReason.setVisibility(View.GONE);
                                reportReasonLabel.setVisibility(View.GONE);

                                editButton.setVisibility(View.VISIBLE);
                                cancelButton.setVisibility(View.GONE);
                                submitButton.setVisibility(View.GONE);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Failed to submit report", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });




        return root;
    }

    private void showEditConfirmationDialog(Service service) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit Service");
        builder.setMessage("Are you sure you want to edit this service?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("services").document(documentId);
                docRef.set(service)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {

                                FragmentTransition.to(ProductsPageFragment.newInstance(),getActivity(),true,R.id.list_layout_products);
                                Toast.makeText(getActivity(), "Service updated!", Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}