package com.example.eventplanner.fragments.employeeManagement;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EmployeeListAdapter;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;

public class EmployeeListFragment extends Fragment {

    private RecyclerView recyclerView;
    private EmployeeListAdapter adapter;
    private ArrayList<User> userList;

    private String companyId;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = auth.getCurrentUser();

    public static EmployeeListFragment newInstance() {
        return new EmployeeListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee_list, container, false);
        recyclerView = view.findViewById(R.id.employee_recycler_view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkLoggedInUserAndFetchData();
    }

    private void checkLoggedInUserAndFetchData() {
        if (currentUser != null) {
            String currentUserEmail = currentUser.getEmail();
            FirebaseFirestore db = FirebaseFirestore.getInstance();

            // Find the company where the current user's email matches the `user.email` field
            db.collection("companies")
                    .whereEqualTo("user.email", currentUserEmail)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful() && !task.getResult().isEmpty()) {
                            DocumentSnapshot companyDocument = task.getResult().getDocuments().get(0);
                            companyId = companyDocument.getString("email"); // Assuming the company document has an `email` field

                            fetchUsersByCompanyId(); // Proceed to fetch users
                        } else {
                            System.out.println("No company found for the logged-in user.");
                        }
                    }).addOnFailureListener(e -> {
                        System.out.println("Error finding company: " + e.getMessage());
                    });
        } else {
            System.out.println("No user is currently logged in.");
        }
    }

    private void fetchUsersByCompanyId() {
        if (companyId != null) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            userList = new ArrayList<>();

            // Query to fetch users with the same companyId
            db.collection("users")
                    .whereEqualTo("companyId", companyId)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                User user = document.toObject(User.class);
                                userList.add(user);
                            }
                            setupRecyclerView();
                        } else {
                            System.out.println("Error getting documents: " + task.getException());
                        }
                    }).addOnFailureListener(e -> {
                        System.out.println("Error fetching users: " + e.getMessage());
                    });
        } else {
            System.out.println("No company ID available to fetch users.");
        }
    }

    private void setupRecyclerView() {
        adapter = new EmployeeListAdapter(requireContext(), userList);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerView.setAdapter(adapter);
    }
}
