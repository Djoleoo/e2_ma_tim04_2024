package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.SingleProductFragment;
import com.example.eventplanner.model.Favorites;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class ProductListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private ArrayList<DocumentSnapshot> aProduct;
    private FragmentActivity mContext;
    private FirebaseFirestore db;
    private User currentUser;
    private boolean isUserLoaded = false;

    public ProductListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> products) {
        super(context, R.layout.product_list_item);
        mContext = context;
        aProduct = products;
        db = FirebaseFirestore.getInstance();
        loadCurrentUser();
    }

    private void loadCurrentUser() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            currentUser = documentSnapshot.toObject(User.class);
                            isUserLoaded = true;
                            notifyDataSetChanged();  // Reload the list once the user is loaded
                        }
                    })
                    .addOnFailureListener(e -> {
                        isUserLoaded = true;  // Set to true to allow the list to be rendered
                        // Handle any errors here
                    });
        } else {
            isUserLoaded = true;  // Set to true to allow the list to be rendered
        }
    }

    @Override
    public int getCount() {
        return isUserLoaded ? aProduct.size() : 0;  // Only render items if the user is loaded
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_list_item,
                    parent, false);
        }

        LinearLayout productItem = convertView.findViewById(R.id.product_list_item);
        TextView productName = convertView.findViewById(R.id.product_name);
        TextView productAvailability = convertView.findViewById(R.id.product_availability);
        Button viewButton = convertView.findViewById(R.id.view_product_button);
        Button addToFavorites = convertView.findViewById(R.id.product_to_favorites_button);
        ImageView imageView = convertView.findViewById(R.id.product_image);
        Button deleteButton = convertView.findViewById(R.id.delete_product_button);

        if (document != null) {
            productName.setText(document.getString("name"));
            Product product = document.toObject(Product.class);

            // Set availability text and color
            if (product.isAvailable()) {
                productAvailability.setText("Available");
                productAvailability.setTextColor(getContext().getResources().getColor(android.R.color.holo_green_dark));
            } else {
                productAvailability.setText("Not Available");
                productAvailability.setTextColor(getContext().getResources().getColor(android.R.color.holo_red_dark));
            }

            // Check if the product is already in the favorites and update the button text
            checkIfProductIsFavorite(document.getId(), addToFavorites);
        }

        viewButton.setOnClickListener(v -> {
            Product product = document.toObject(Product.class);
            FragmentTransition.to(SingleProductFragment.newInstance(product, document.getId()), mContext, true, R.id.list_layout_products);
        });

        addToFavorites.setOnClickListener(v -> {
            Product product = document.toObject(Product.class);
            if (product != null) {
                toggleFavoriteStatus(document.getId(), product, addToFavorites);
            }
        });

        if (currentUser != null && currentUser.getUserType() == User.UserType.OD) {
            addToFavorites.setVisibility(View.VISIBLE);
        } else {
            addToFavorites.setVisibility(View.GONE);
        }

        deleteButton.setOnClickListener(v -> showDeleteConfirmationDialog(document));

        return convertView;
    }


    private void checkIfProductIsFavorite(String productId, Button addToFavorites) {
        if (currentUser == null) {
            Log.d("Current user", "Je null mamu mu jebem");
            return;
        }

        db.collection("favorites").whereEqualTo("user.id", currentUser.getId())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    Log.d("QUEERY", "QUERY");
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot favoriteDoc = queryDocumentSnapshots.getDocuments().get(0);
                        Favorites favorites = favoriteDoc.toObject(Favorites.class);

                        if (favorites != null && favorites.getFavoriteProducts().stream().anyMatch(p -> p.getId().equals(productId))) {
                            addToFavorites.setText("Remove from favorites");
                            Log.d("IIIIIFFFF", "IFFFFFFFFFFFFFF");
                        } else {
                            addToFavorites.setText("Favorite");
                            Log.d("ELSEEEEEEEEE", "ELSEEEEEEEEE");
                        }
                    } else {
                        addToFavorites.setText("Favorite");
                    }
                })
                .addOnFailureListener(e -> {
                    Log.d("FAIL LOAD FAVORITE", "FAIL FAIL FAIL");
                });
    }

    private void toggleFavoriteStatus(String productId, Product product, Button addToFavorites) {
        if (currentUser == null) {
            Toast.makeText(mContext, "User not loaded. Please try again.", Toast.LENGTH_SHORT).show();
            return;
        }

        db.collection("favorites").whereEqualTo("user.id", currentUser.getId())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot favoriteDoc = queryDocumentSnapshots.getDocuments().get(0);
                        Favorites favorites = favoriteDoc.toObject(Favorites.class);

                        if (favorites != null) {
                            if (favorites.getFavoriteProducts().stream().anyMatch(p -> p.getId().equals(productId))) {
                                favorites.removeFavoriteProductById(productId);
                                addToFavorites.setText("Favorite");
                            } else {
                                product.setId(productId); // Ensure product has the correct ID
                                favorites.addFavoriteProduct(product);
                                addToFavorites.setText("Remove from favorites");
                            }

                            db.collection("favorites").document(favoriteDoc.getId())
                                    .set(favorites)
                                    .addOnSuccessListener(aVoid -> {
                                        Toast.makeText(mContext, "Favorites updated", Toast.LENGTH_SHORT).show();
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(mContext, "Failed to update favorites", Toast.LENGTH_SHORT).show();
                                    });
                        }
                    } else {
                        // No Favorites entry exists, create one
                        Favorites newFavorites = new Favorites();
                        newFavorites.setUser(currentUser);
                        product.setId(productId); // Ensure product has the correct ID
                        newFavorites.addFavoriteProduct(product);

                        db.collection("favorites").add(newFavorites)
                                .addOnSuccessListener(documentReference -> {
                                    addToFavorites.setText("Remove from favorites");
                                    Toast.makeText(mContext, "Added to favorites", Toast.LENGTH_SHORT).show();
                                })
                                .addOnFailureListener(e -> {
                                    Toast.makeText(mContext, "Failed to add to favorites", Toast.LENGTH_SHORT).show();
                                });
                    }
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(mContext, "Failed to load favorites", Toast.LENGTH_SHORT).show();
                });
    }

    private void showDeleteConfirmationDialog(DocumentSnapshot document) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete product");
        builder.setMessage("Are you sure you want to delete this product?");
        builder.setPositiveButton("Yes", (dialog, which) -> {
            db.collection("products").document(document.getId()).delete()
                    .addOnSuccessListener(aVoid -> {
                        aProduct.remove(document);
                        notifyDataSetChanged();
                    }).addOnFailureListener(e -> {
                        Toast.makeText(getContext(), "Failed to delete product", Toast.LENGTH_SHORT).show();
                    });
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
