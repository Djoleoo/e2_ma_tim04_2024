package com.example.eventplanner.fragments.reportedUsers;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentUsersReportedPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.PackagesFragment;
import com.example.eventplanner.fragments.products.ProductsFragment;
import com.example.eventplanner.fragments.products.ProductsPageFragment;
import com.example.eventplanner.fragments.products.ServicesFragment;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UsersReportedPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UsersReportedPageFragment extends Fragment {


    public static ArrayList<DocumentSnapshot> mUserReportsDocuments = new ArrayList<>();
    public static ArrayList<DocumentSnapshot> mCompanyReportsDocuments = new ArrayList<>();

    public User user = new User();


    private FragmentUsersReportedPageBinding binding;



    public UsersReportedPageFragment() {
        // Required empty public constructor
    }


    public static UsersReportedPageFragment newInstance() {
        UsersReportedPageFragment fragment = new UsersReportedPageFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();

        binding = FragmentUsersReportedPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        findUserByUserEmail(currentUser.getEmail(), new UsersReportedPageFragment.OnUserLoadedListener() {
            @Override
            public void onUserLoaded(User company) {
                user = company;
                Log.d("ProductsPageFragment", "User loaded: " + user.getEmail());
                selectUserReports(currentActivity);
            }
        });



        int checkedButtonId = binding.toggleGroupReports.getCheckedButtonId();
        if(checkedButtonId == R.id.button_user_reports){
            FragmentTransition.to(UsersReportedFragment.newInstance(mUserReportsDocuments),currentActivity,true,R.id.list_layout_products);
        }
        else if (checkedButtonId == R.id.button_company_reports) {
            FragmentTransition.to(ReportCompanyFragment.newInstance(mCompanyReportsDocuments), currentActivity, true, R.id.list_layout_products);
        }








        Button userReportsBtn = binding.buttonUserReports;
        Button companyReportsBtn = binding.buttonCompanyReports;





        userReportsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectUserReports(currentActivity);

            }
        });
        companyReportsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCompanyReports(currentActivity);

            }
        });


        return root;
    }


    private void selectUserReports(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(user.getUserType()== User.UserType.ADMIN) {
            db.collection("userReports").get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mUserReportsDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mUserReportsDocuments.add(document);
                                }
                                FragmentTransition.to(UsersReportedFragment.newInstance(mUserReportsDocuments), currentActivity, true, R.id.list_layout_reports);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
    }

    private void selectCompanyReports(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(user.getUserType()== User.UserType.ADMIN) {
            db.collection("companyReports").get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mCompanyReportsDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mCompanyReportsDocuments.add(document);
                                }
                                FragmentTransition.to(ReportCompanyFragment.newInstance(mCompanyReportsDocuments), currentActivity, true, R.id.list_layout_reports);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting services", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
    }

    private void findUserByUserEmail(String email, UsersReportedPageFragment.OnUserLoadedListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                user= document.toObject(User.class);
                                listener.onUserLoaded(user);
                                return;
                            }
                        } else {
                            Toast.makeText(getActivity(), "Error getting user", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    interface OnUserLoadedListener {
        void onUserLoaded(User user);
    }



    public void onDestroyView() {

        super.onDestroyView();



    }
}