package com.example.eventplanner.fragments.registration;

import android.app.AlertDialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEmployeeRegistrationBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.LoginFragment;
import com.example.eventplanner.model.Availability;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class EmployeeRegistrationFragment extends Fragment {

    private static final String TAG = "EmployeeRegFragment";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private FragmentEmployeeRegistrationBinding binding;
    private String companyId;

    private Company company;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = auth.getCurrentUser();

    private String userType;

    public void checkLoggedInUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            String email = currentUser.getEmail();
            String uid = currentUser.getUid();
            boolean isEmailVerified = currentUser.isEmailVerified();

            getUserType();
            getUserCompanyId();
            findCompanyForLoggedInUser();

            System.out.println("Logged in user email: " + email);
            System.out.println("User ID: " + uid);
            System.out.println("Is email verified: " + isEmailVerified);

        } else {
            System.out.println("No user is currently logged in.");
        }
    }

    public void getUserType() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            String uid = currentUser.getUid();

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("users").document(uid);

            docRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    userType = documentSnapshot.getString("userType");
                    // Now you have the userType
                    System.out.println("User Type: " + userType);
                } else {
                    System.out.println("No such document exists!");
                }
            }).addOnFailureListener(e -> {
                System.out.println("Error retrieving document: " + e.getMessage());
            });
        } else {
            System.out.println("No user is currently logged in.");
        }
    }

    private void findCompanyForLoggedInUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            String userEmail = currentUser.getEmail();

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("companies")
                    .whereEqualTo("user.email", userEmail)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            if (!task.getResult().isEmpty()) {
                                DocumentSnapshot document = task.getResult().getDocuments().get(0);
                                company = document.toObject(Company.class);

                                System.out.println("Company Name: " + company.getName());

                                // After successfully fetching the company data, setup checkboxes
                                setupWorkHoursCheckboxes();
                            } else {
                                System.out.println("No company found for the logged-in user.");
                            }
                        } else {
                            System.out.println("Error getting documents: " + task.getException());
                        }
                    });
        } else {
            System.out.println("No user is currently logged in.");
        }
    }

    private void setupWorkHoursCheckboxes() {
        // Assuming company has been successfully fetched
        if (company != null && company.getWorkHours() != null) {
            setupCheckboxWithTextField(binding.checkboxMondayDefaultTime, binding.editTextMondayWorkingHours, company.getWorkHours().getMonday());
            setupCheckboxWithTextField(binding.checkboxTuesdayDefaultTime, binding.editTextTuesdayWorkingHours, company.getWorkHours().getTuesday());
            setupCheckboxWithTextField(binding.checkboxWednesdayDefaultTime, binding.editTextWednesdayWorkingHours, company.getWorkHours().getWednesday());
            setupCheckboxWithTextField(binding.checkboxThursdayDefaultTime, binding.editTextThursdayWorkingHours, company.getWorkHours().getThursday());
            setupCheckboxWithTextField(binding.checkboxFridayDefaultTime, binding.editTextFridayWorkingHours, company.getWorkHours().getFriday());
            setupCheckboxWithTextField(binding.checkboxSaturdayDefaultTime, binding.editTextSaturdayWorkingHours, company.getWorkHours().getSaturday());
            setupCheckboxWithTextField(binding.checkboxSundayDefaultTime, binding.editTextSundayWorkingHours, company.getWorkHours().getSunday());
        }
    }

    private void setupCheckboxWithTextField(CheckBox checkBox, final TextInputEditText textField, String defaultTime) {
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            textField.setEnabled(!isChecked);
            if (isChecked) {
                textField.setText(defaultTime != null ? defaultTime : "09:00-17:00");
            }
        });
    }





    public void getUserCompanyId() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            String uid = currentUser.getUid();

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("users").document(uid);

            docRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    companyId = documentSnapshot.getString("companyId");
                    System.out.println("Compnay Id: " + companyId);
                } else {
                    System.out.println("No such document exists!");
                }
            }).addOnFailureListener(e -> {
                System.out.println("Error retrieving document: " + e.getMessage());
            });
        } else {
            System.out.println("No user is currently logged in.");
        }
    }


    public EmployeeRegistrationFragment() {
        checkLoggedInUser();
    }

    public static EmployeeRegistrationFragment newInstance(String param1, String param2) {
        EmployeeRegistrationFragment fragment = new EmployeeRegistrationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private void setupCheckboxWithTextField(CheckBox checkBox, final TextInputEditText textField) {
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                textField.setEnabled(!isChecked);
                if (isChecked) {
                    textField.setText("09:00-17:00");
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CheckBox checkBoxMonday = view.findViewById(R.id.checkboxMondayDefaultTime);
        TextInputEditText editTextMondayWorkingHours = view.findViewById(R.id.editTextMondayWorkingHours);

        CheckBox checkBoxTuesday = view.findViewById(R.id.checkboxTuesdayDefaultTime);
        TextInputEditText editTextTuesdayWorkingHours = view.findViewById(R.id.editTextTuesdayWorkingHours);

        CheckBox checkBoxWednesday = view.findViewById(R.id.checkboxWednesdayDefaultTime);
        TextInputEditText editTextWednesdayWorkingHours = view.findViewById(R.id.editTextWednesdayWorkingHours);

        CheckBox checkBoxThursday = view.findViewById(R.id.checkboxThursdayDefaultTime);
        TextInputEditText editTextThursdayWorkingHours = view.findViewById(R.id.editTextThursdayWorkingHours);

        CheckBox checkBoxFriday = view.findViewById(R.id.checkboxFridayDefaultTime);
        TextInputEditText editTextFridayWorkingHours = view.findViewById(R.id.editTextFridayWorkingHours);

        CheckBox checkBoxSaturday = view.findViewById(R.id.checkboxSaturdayDefaultTime);
        TextInputEditText editTextSaturdayWorkingHours = view.findViewById(R.id.editTextSaturdayWorkingHours);

        CheckBox checkBoxSunday = view.findViewById(R.id.checkboxSundayDefaultTime);
        TextInputEditText editTextSundayWorkingHours = view.findViewById(R.id.editTextSundayWorkingHours);

        setupCheckboxWithTextField(checkBoxMonday, editTextMondayWorkingHours);
        setupCheckboxWithTextField(checkBoxTuesday, editTextTuesdayWorkingHours);
        setupCheckboxWithTextField(checkBoxWednesday, editTextWednesdayWorkingHours);
        setupCheckboxWithTextField(checkBoxThursday, editTextThursdayWorkingHours);
        setupCheckboxWithTextField(checkBoxFriday, editTextFridayWorkingHours);
        setupCheckboxWithTextField(checkBoxSaturday, editTextSaturdayWorkingHours);
        setupCheckboxWithTextField(checkBoxSunday, editTextSundayWorkingHours);

        binding.buttonActivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Activate button clicked");
                if (validateInputs()) {
                    saveUserToFirebase();
                }
            }
        });
    }

    private boolean validateInputs() {
        String email = binding.editTextEmail.getText().toString();
        String password = binding.editTextPassword.getText().toString();
        String confirmPassword = binding.editTextConfirmPassword.getText().toString();
        String firstName = binding.editTextFirstName.getText().toString();
        String lastName = binding.editTextLastName.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Log.d(TAG, "Invalid email");
            Toast.makeText(getActivity(), "Please enter a valid email address", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.isEmpty() || confirmPassword.isEmpty() || !password.equals(confirmPassword)) {
            Log.d(TAG, "Passwords do not match");
            Toast.makeText(getActivity(), "Passwords do not match", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (firstName.isEmpty() || !firstName.matches("[a-zA-Z]+")) {
            Log.d(TAG, "Invalid first name");
            Toast.makeText(getActivity(), "Please enter a valid first name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (lastName.isEmpty() || !lastName.matches("[a-zA-Z]+")) {
            Log.d(TAG, "Invalid last name");
            Toast.makeText(getActivity(), "Please enter a valid last name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!validateWorkingHours()) {
            return false;
        }

        return true;
    }

    private boolean validateWorkingHours() {
        if (!isValidTimeFormat(binding.editTextMondayWorkingHours.getText().toString()) && !binding.checkboxMondayDefaultTime.isChecked()) {
            Toast.makeText(getActivity(), "Please set valid working hours for Monday", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!isValidTimeFormat(binding.editTextTuesdayWorkingHours.getText().toString()) && !binding.checkboxTuesdayDefaultTime.isChecked()) {
            Toast.makeText(getActivity(), "Please set valid working hours for Tuesday", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!isValidTimeFormat(binding.editTextWednesdayWorkingHours.getText().toString()) && !binding.checkboxWednesdayDefaultTime.isChecked()) {
            Toast.makeText(getActivity(), "Please set valid working hours for Wednesday", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!isValidTimeFormat(binding.editTextThursdayWorkingHours.getText().toString()) && !binding.checkboxThursdayDefaultTime.isChecked()) {
            Toast.makeText(getActivity(), "Please set valid working hours for Thursday", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!isValidTimeFormat(binding.editTextFridayWorkingHours.getText().toString()) && !binding.checkboxFridayDefaultTime.isChecked()) {
            Toast.makeText(getActivity(), "Please set valid working hours for Friday", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!isValidTimeFormat(binding.editTextSaturdayWorkingHours.getText().toString()) && !binding.checkboxSaturdayDefaultTime.isChecked()) {
            Toast.makeText(getActivity(), "Please set valid working hours for Saturday", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!isValidTimeFormat(binding.editTextSundayWorkingHours.getText().toString()) && !binding.checkboxSundayDefaultTime.isChecked()) {
            Toast.makeText(getActivity(), "Please set valid working hours for Sunday", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean isValidTimeFormat(String time) {
        // Regex pattern for HH:MM-HH:MM
        String timePattern = "^([01]\\d|2[0-3]):([0-5]\\d)-([01]\\d|2[0-3]):([0-5]\\d)$";
        return time.matches(timePattern);
    }

    private void saveUserToFirebase() {
        String email = binding.editTextEmail.getText().toString();
        String password = binding.editTextPassword.getText().toString();
        String firstName = binding.editTextFirstName.getText().toString();
        String lastName = binding.editTextLastName.getText().toString();
        String address = binding.editTextAddress.getText().toString();
        String phoneNumber = binding.editTextPhoneNumber.getText().toString();

        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setTitle("Confirm Registration");
        builder.setMessage("Are you sure you want to finish registration?");
        builder.setPositiveButton("Yes", (dialog, which) -> {
            auth.createUserWithEmailAndPassword(email, password)
                    .addOnSuccessListener(authResult -> {
                        FirebaseUser firebaseUser = authResult.getUser();
                        String userId = firebaseUser != null ? firebaseUser.getUid() : db.collection("users").document().getId();

                        if (firebaseUser != null) {
                            firebaseUser.sendEmailVerification()
                                    .addOnCompleteListener(verificationTask -> {
                                        if (verificationTask.isSuccessful()) {
                                            User user = new User(firstName, lastName, email, password, phoneNumber, address, User.UserType.PUPZ, "https://www.example.com/default-profile-pic.jpg", company.getEmail());
                                            user.setId(userId);
                                            user.setEnabled(false);

                                            db.collection("users").document(userId).set(user)
                                                    .addOnSuccessListener(aVoid -> {
                                                        saveAvailability(email);
                                                        Toast.makeText(getActivity(), "User registered successfully! Verification email sent.", Toast.LENGTH_SHORT).show();
                                                        FragmentTransition.to(LoginFragment.newInstance(), getActivity(), true, R.id.loginCard);
                                                    })
                                                    .addOnFailureListener(e -> Toast.makeText(getActivity(), "Failed to register user: " + e.getMessage(), Toast.LENGTH_SHORT).show());
                                        } else {
                                            Toast.makeText(getActivity(), "Failed to send verification email: " + verificationTask.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(e -> Toast.makeText(getActivity(), "Failed to create user: " + e.getMessage(), Toast.LENGTH_SHORT).show());
        });

        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void saveAvailability(String userId) {
        String[] mondayTimes = parseHours(binding.editTextMondayWorkingHours.getText().toString());
        String[] tuesdayTimes = parseHours(binding.editTextTuesdayWorkingHours.getText().toString());
        String[] wednesdayTimes = parseHours(binding.editTextWednesdayWorkingHours.getText().toString());
        String[] thursdayTimes = parseHours(binding.editTextThursdayWorkingHours.getText().toString());
        String[] fridayTimes = parseHours(binding.editTextFridayWorkingHours.getText().toString());
        String[] saturdayTimes = parseHours(binding.editTextSaturdayWorkingHours.getText().toString());
        String[] sundayTimes = parseHours(binding.editTextSundayWorkingHours.getText().toString());

        // Set the start and end dates for the availability period
        String startDate = "2024-08-01";
        String endDate = "2024-08-31";

        Availability availability = new Availability(
                userId,
                mondayTimes[0], mondayTimes[1],
                tuesdayTimes[0], tuesdayTimes[1],
                wednesdayTimes[0], wednesdayTimes[1],
                thursdayTimes[0], thursdayTimes[1],
                fridayTimes[0], fridayTimes[1],
                saturdayTimes[0], saturdayTimes[1],
                sundayTimes[0], sundayTimes[1],
                startDate, endDate
        );

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("availability").document(userId).set(availability)
                .addOnSuccessListener(aVoid -> Toast.makeText(getActivity(), "Availability saved successfully!", Toast.LENGTH_SHORT).show())
                .addOnFailureListener(e -> Toast.makeText(getActivity(), "Failed to save availability: " + e.getMessage(), Toast.LENGTH_SHORT).show());
    }

    private String[] parseHours(String hours) {
        try {
            String[] parts = hours.split("-");
            String startTime = parts[0].trim();
            String endTime = parts[1].trim();
            return new String[]{startTime, endTime};
        } catch (Exception e) {
            return new String[]{"09:00", "17:00"}; // Default times if parsing fails
        }
    }



    private void uploadProfilePicture(Uri imageUri) {
        if (imageUri != null) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageReference = storage.getReference().child("profile_pictures/" + System.currentTimeMillis() + ".jpg");

            storageReference.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String profilePictureUrl = uri.toString();
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Failed to upload profile picture: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private String getWorkingHours() {
        StringBuilder workingHours = new StringBuilder();
        workingHours.append("Monday: ").append(binding.editTextMondayWorkingHours.getText().toString()).append("\n");
        workingHours.append("Tuesday: ").append(binding.editTextTuesdayWorkingHours.getText().toString()).append("\n");
        workingHours.append("Wednesday: ").append(binding.editTextWednesdayWorkingHours.getText().toString()).append("\n");
        workingHours.append("Thursday: ").append(binding.editTextThursdayWorkingHours.getText().toString()).append("\n");
        workingHours.append("Friday: ").append(binding.editTextFridayWorkingHours.getText().toString()).append("\n");
        workingHours.append("Saturday: ").append(binding.editTextSaturdayWorkingHours.getText().toString()).append("\n");
        workingHours.append("Sunday: ").append(binding.editTextSundayWorkingHours.getText().toString()).append("\n");
        return workingHours.toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeRegistrationBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }
}
