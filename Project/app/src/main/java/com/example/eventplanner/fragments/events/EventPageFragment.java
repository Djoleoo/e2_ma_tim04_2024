package com.example.eventplanner.fragments.events;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentCategoriesPageBinding;
import com.example.eventplanner.databinding.FragmentEventPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class EventPageFragment extends Fragment {

    private ArrayList<DocumentSnapshot> mEventDocuments = new ArrayList<>();
    private FragmentEventPageBinding binding;

    public EventPageFragment() {
        // Required empty public constructor
    }


    public static EventPageFragment newInstance() {
        EventPageFragment fragment = new EventPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentActivity currentActivity = getActivity();
        binding = FragmentEventPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        FloatingActionButton addBtn = binding.floatingActionButton;
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBtn.setVisibility(View.GONE);
                FragmentTransition.to(AddEventFragment.newInstance(),getActivity(),true,R.id.list_layout);
            }
        });
        select(currentActivity);
        return root;
    }
    private  void select(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("eventTypes").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mEventDocuments.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Event event = document.toObject(Event.class);
                                mEventDocuments.add(document);
                            }
                            FragmentTransition.to(EventsFragment.newInstance(mEventDocuments),currentActivity,true,R.id.list_layout);
                        }else{
                            Toast.makeText(newInstance().getActivity(), "Error getting events", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }
    /*
    private void prepareEvent(ArrayList<Event> events){
        ArrayList<Category> categories = new ArrayList<Category>();
        Category category1 = new Category(1L, "KATEGORIJA 1", "Opis kategorije 1");
        Category category2 = new Category(2L, "KATEGORIJA 2", "Opis kategorije 2");
        categories.add(category1);
        categories.add(category2);

        ArrayList<SubCategory> subCategories1 = new ArrayList<>();
        subCategories1.add(new SubCategory( "SUBKATEGORIJA 1", "Opis podkategorije 1", categories.get(0), SubCategory.Status.SERVICE));
        subCategories1.add(new SubCategory( "SUBKATEGORIJA 2", "Opis podkategorije 2", categories.get(0), SubCategory.Status.PRODUCT));

        ArrayList<SubCategory> subCategories2 = new ArrayList<>();
        subCategories2.add(new SubCategory( "SUBKATEGORIJA 3", "Opis podkategorije 3", categories.get(1), SubCategory.Status.SERVICE));
        subCategories2.add(new SubCategory( "SUBKATEGORIJA 4", "Opis podkategorije 4", categories.get(1), SubCategory.Status.PRODUCT));


        events.add(new Event( "Event 1", "Description 1", subCategories1,));
        events.add(new Event( "Event 2", "Description 2", subCategories2));

    }

     */

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}