package com.example.eventplanner.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.ActivityHomeBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;

public class HomeActivity extends AppCompatActivity {

    private ActivityHomeBinding binding;
    private Toolbar toolbar;
    private NavController navController;

    private AppBarConfiguration mAppBarConfiguration;
    private static String CHANNEL_1 = "Channel_1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        toolbar = binding.toolbar;
        setSupportActionBar(toolbar);

        navController = Navigation.findNavController(this,R.id.fragment_nav_content_main);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_settings, R.id.nav_categories, R.id.nav_events, R.id.nav_products,
                R.id.nav_happenings, R.id.nav_employee_management, R.id.nav_registrations, R.id.nav_price_list,
                R.id.nav_report_user, R.id.nav_reported_comments, R.id.nav_notifications, R.id.nav_reservations,
                R.id.userAccount, R.id.nav_leave_comment, R.id.nav_company_comments)
                .build();





        NavigationUI.setupWithNavController(binding.toolbar,navController);
        NavigationUI.setupWithNavController(binding.bottomNavigationView,navController);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);

        createNotificationChannel("Channel_1 ", "First channel for notifications !",
                CHANNEL_1, NotificationManager.IMPORTANCE_DEFAULT);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        int id = item.getItemId();
        if(id == R.id.nav_settings){
            Toast.makeText(this,"Go to settings",Toast.LENGTH_SHORT).show();
        }else if (id == R.id.logout) {
            // Dodajte kod za odjavu korisnika
            FirebaseAuth.getInstance().signOut();
            // Dodatno možete dodati i logiku za preusmeravanje korisnika na početni ekran ili login ekran
            // Na primer, možete pokrenuti novu aktivnost ili fragment
            Intent intent = new Intent(this, LoginAndRegistrationActivity.class);
            startActivity(intent);
            finish(); // Zatvorite trenutnu aktivnost ili fragment
            return true;
        }

        navController = Navigation.findNavController(this, R.id.fragment_nav_content_main);
        return NavigationUI.onNavDestinationSelected(item, navController) || super.onOptionsItemSelected(item);
    }

    private void createNotificationChannel(CharSequence name, String description, String channel_id, int importance) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        NotificationChannel channel = new NotificationChannel(channel_id, name, importance);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public boolean onSupportNavigateUp() {
        navController = Navigation.findNavController(this, R.id.fragment_nav_content_main);
        FloatingActionButton addButton = findViewById(R.id.floating_action_button);
        addButton.setVisibility(View.VISIBLE);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }
}