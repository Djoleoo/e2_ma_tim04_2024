package com.example.eventplanner.fragments.comment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Comment;
import com.example.eventplanner.model.Notification;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.Date;

public class LeaveCommentFragment extends Fragment {

    private TextView tvCompanyName;
    private RatingBar ratingBar;
    private EditText etComment;
    private Button btnSubmitComment;

    private FirebaseAuth auth;
    private FirebaseFirestore db;
    private String companyId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leave_comment, container, false);

        tvCompanyName = view.findViewById(R.id.tv_company_name);
        ratingBar = view.findViewById(R.id.ratingBar);
        etComment = view.findViewById(R.id.et_comment);
        btnSubmitComment = view.findViewById(R.id.btn_submit_comment);

        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        // Get the current user's email (OD's email)
        String currentUserEmail = auth.getCurrentUser().getEmail();

        // Query the comments collection to check if the current user can leave a review
        db.collection("comments")
                .whereEqualTo("userID", currentUserEmail)
                .whereEqualTo("ableToLeaveReview", true)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        // Assuming there's only one relevant comment
                        DocumentReference commentRef = queryDocumentSnapshots.getDocuments().get(0).getReference();
                        Comment comment = queryDocumentSnapshots.getDocuments().get(0).toObject(Comment.class);

                        // Check if the current date is within the allowed 5-day period
                        Date reviewDeadline = comment.getDate(); // This is the date set as 5 days from the cancellation
                        Date currentDate = Calendar.getInstance().getTime();

                        if (currentDate.before(reviewDeadline)) {
                            // Display the company name
                            tvCompanyName.setText(comment.getCompanyID());

                            // Set the companyId for use later
                            companyId = comment.getCompanyID();

                            // Set up the submit button
                            btnSubmitComment.setOnClickListener(v -> {
                                String commentText = etComment.getText().toString().trim();
                                float ratingValue = ratingBar.getRating();

                                if (!commentText.isEmpty() && ratingValue > 0) {
                                    // Update the comment with the rating and text
                                    commentRef.update("rating", String.valueOf(ratingValue));
                                    commentRef.update("text", commentText);
                                    commentRef.update("ableToLeaveReview", false); // Disable further reviews

                                    // Notify the company (PUP-V)
                                    createNotificationForPUPV(comment.getCompanyID());

                                    Toast.makeText(getContext(), "Comment submitted successfully!", Toast.LENGTH_SHORT).show();

                                    // Optionally, navigate away or close the fragment
                                    getActivity().onBackPressed();
                                } else {
                                    Toast.makeText(getContext(), "Please provide a rating and comment", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                            Toast.makeText(getContext(), "The period to leave a review has expired.", Toast.LENGTH_SHORT).show();
                            // Optionally, navigate away or close the fragment
                            getActivity().onBackPressed();
                        }
                    } else {
                        Toast.makeText(getContext(), "You have no pending reviews.", Toast.LENGTH_SHORT).show();
                        // Optionally, navigate away or close the fragment
                        getActivity().onBackPressed();
                    }
                })
                .addOnFailureListener(e -> Log.e("LeaveCommentFragment", "Error querying comments", e));

        return view;
    }

    private void createNotificationForPUPV(String companyId) {
        // Create and send a notification to PUP-V that the OD has left a comment
        Notification notification = new Notification(
                "New Review",
                "A new review has been left for your company.",
                companyId,  // Assuming companyId can be used to identify the PUP-V user
                false,  // Mark as unread
                Notification.UserType.PUP  // User type
        );

        db.collection("notifications")
                .add(notification)
                .addOnSuccessListener(documentReference -> {
                    Log.d("LeaveCommentFragment", "Notification for PUP-V created successfully.");
                })
                .addOnFailureListener(e -> {
                    Log.e("LeaveCommentFragment", "Error creating notification for PUP-V", e);
                });
    }
}