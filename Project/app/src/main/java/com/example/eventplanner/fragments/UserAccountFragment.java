package com.example.eventplanner.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class UserAccountFragment extends Fragment {

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText emailEditText;
    private EditText phoneNumberEditText;
    private EditText addressEditText;
    private ImageView profileImageView;
    private Button editButton;
    private Button changePasswordButton;

    private User currentUser;
    private FirebaseFirestore db;
    private boolean isEditing = false;
    private Button companyInfoButton;

    public static UserAccountFragment newInstance() {
        return new UserAccountFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_account, container, false);

        firstNameEditText = rootView.findViewById(R.id.user_first_name);
        lastNameEditText = rootView.findViewById(R.id.user_last_name);
        emailEditText = rootView.findViewById(R.id.user_email);
        phoneNumberEditText = rootView.findViewById(R.id.user_phone_number);
        addressEditText = rootView.findViewById(R.id.user_address);
        profileImageView = rootView.findViewById(R.id.user_profile_image);
        editButton = rootView.findViewById(R.id.edit_button);
        changePasswordButton = rootView.findViewById(R.id.change_password_button);
        companyInfoButton = rootView.findViewById(R.id.company_info_button);

        db = FirebaseFirestore.getInstance();
        loadCurrentUser();

        editButton.setOnClickListener(v -> toggleEditMode());
        changePasswordButton.setOnClickListener(v -> showChangePasswordDialog());

        return rootView;
    }

    private void loadCurrentUser() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        currentUser = documentSnapshot.toObject(User.class);
                        currentUser.setId(firebaseUser.getUid());
                        populateUserDetails();

                        if (currentUser.getUserType().equals(User.UserType.PUP)) {
                            companyInfoButton.setVisibility(View.VISIBLE);
                            companyInfoButton.setOnClickListener(v -> showCompanyInfoDialog());
                        }
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(getActivity(), "Failed to load user data", Toast.LENGTH_SHORT).show();
                    });
        }
    }

    private void populateUserDetails() {
        if (currentUser != null) {
            firstNameEditText.setText(currentUser.getFirstName());
            lastNameEditText.setText(currentUser.getLastName());
            emailEditText.setText(currentUser.getEmail());
            phoneNumberEditText.setText(currentUser.getPhoneNumber());
            addressEditText.setText(currentUser.getAddress());
            // Load profile image using a library like Glide or Picasso if you want to display the image
        }
    }

    private void showCompanyInfoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_company_info, null);
        builder.setView(dialogView);

        // Find dialog views
        EditText companyNameEditText = dialogView.findViewById(R.id.company_name);
        EditText companyDescriptionEditText = dialogView.findViewById(R.id.company_description);
        EditText companyLocationEditText = dialogView.findViewById(R.id.company_location);
        EditText companyPhoneNumberEditText = dialogView.findViewById(R.id.company_phone_number);

        // Load company information
        db.collection("companies").document(currentUser.getCompanyId()).get()
                .addOnSuccessListener(documentSnapshot -> {
                    Company company = documentSnapshot.toObject(Company.class);
                    if (company != null) {
                        companyNameEditText.setText(company.getName());
                        companyDescriptionEditText.setText(company.getDescription());
                        companyLocationEditText.setText(company.getAddress());
                        companyPhoneNumberEditText.setText(company.getNumber());
                    }
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(getActivity(), "Failed to load company data", Toast.LENGTH_SHORT).show();
                });

        builder.setPositiveButton("Save", (dialog, which) -> {
            // Update company information
            String updatedDescription = companyDescriptionEditText.getText().toString();
            String updatedLocation = companyLocationEditText.getText().toString();
            String updatedPhoneNumber = companyPhoneNumberEditText.getText().toString();

            db.collection("companies").document(currentUser.getCompanyId())
                    .update("description", updatedDescription,
                            "address", updatedLocation,
                            "number", updatedPhoneNumber)
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(getActivity(), "Company details updated", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(getActivity(), "Failed to update company details", Toast.LENGTH_SHORT).show();
                    });
        });

        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void toggleEditMode() {
        if (isEditing) {
            saveUserDetails();
        } else {
            firstNameEditText.setEnabled(true);
            lastNameEditText.setEnabled(true);
            phoneNumberEditText.setEnabled(true);
            addressEditText.setEnabled(true);
            editButton.setText("Save");
            isEditing = true;
        }
    }

    private void saveUserDetails() {
        String firstName = firstNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();
        String phoneNumber = phoneNumberEditText.getText().toString();
        String address = addressEditText.getText().toString();

        currentUser.setFirstName(firstName);
        currentUser.setLastName(lastName);
        currentUser.setPhoneNumber(phoneNumber);
        currentUser.setAddress(address);

        Log.d("POKUSAJ ID MENJANJE", currentUser.getId());

        db.collection("users").document(currentUser.getId()).set(currentUser)
                .addOnSuccessListener(aVoid -> {
                    Toast.makeText(getActivity(), "User details updated", Toast.LENGTH_SHORT).show();
                    firstNameEditText.setEnabled(false);
                    lastNameEditText.setEnabled(false);
                    phoneNumberEditText.setEnabled(false);
                    addressEditText.setEnabled(false);
                    editButton.setText("Edit");
                    isEditing = false;
                })
                .addOnFailureListener(e -> Toast.makeText(getActivity(), "Failed to update user details", Toast.LENGTH_SHORT).show());
    }

    private void showChangePasswordDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_change_password, null);
        builder.setView(dialogView);

        EditText currentPasswordEditText = dialogView.findViewById(R.id.current_password);
        EditText newPasswordEditText = dialogView.findViewById(R.id.new_password);
        EditText confirmPasswordEditText = dialogView.findViewById(R.id.confirm_password);

        builder.setPositiveButton("Change", (dialog, which) -> {
            String currentPassword = currentPasswordEditText.getText().toString();
            String newPassword = newPasswordEditText.getText().toString();
            String confirmPassword = confirmPasswordEditText.getText().toString();

            if (!newPassword.equals(confirmPassword)) {
                Toast.makeText(getActivity(), "New passwords do not match", Toast.LENGTH_SHORT).show();
                return;
            }

            // Implement password change logic here
            changePassword(currentPassword, newPassword);
        });

        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void changePassword(String currentPassword, String newPassword) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            firebaseUser.updatePassword(newPassword)
                    .addOnSuccessListener(aVoid -> {
                        // Password updated in Firebase Authentication
                        currentUser.setPassword(newPassword); // Update the password in the User object

                        // Now update the User object in Firestore
                        db.collection("users").document(currentUser.getId())
                                .set(currentUser)
                                .addOnSuccessListener(aVoid1 -> {
                                    Toast.makeText(getActivity(), "Password changed and user updated successfully", Toast.LENGTH_SHORT).show();
                                })
                                .addOnFailureListener(e -> {
                                    Toast.makeText(getActivity(), "Failed to update user in database", Toast.LENGTH_SHORT).show();
                                });
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(getActivity(), "Failed to change password", Toast.LENGTH_SHORT).show();
                    });
        }
    }
}
