package com.example.eventplanner.fragments.PupVRegistrations;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.StringListAdapter;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.LoginFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.PUPVRegistration;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SinglePupVRegistrationFragment extends Fragment {
    private static final String ARG_REG = "registration";
    private static final String ARG_REG_ID = "registrationId";

    private ArrayList<Event> eventTypes = new ArrayList<>();
    private ArrayList<Category> categories = new ArrayList<>();
    private ArrayList<String> eventTypeNames = new ArrayList<>();
    private ArrayList<String> categoryNames = new ArrayList<>();
    private FirebaseAuth auth;
    ExecutorService executor = Executors.newSingleThreadExecutor();

    private PUPVRegistration registration;
    private String documentId;

    public SinglePupVRegistrationFragment() {
        // Required empty public constructor
    }
    public static SinglePupVRegistrationFragment newInstance(PUPVRegistration registration, String documentId) {
        SinglePupVRegistrationFragment fragment = new SinglePupVRegistrationFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_REG,registration);
        args.putString(ARG_REG_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            registration = getArguments().getParcelable(ARG_REG);
            documentId = getArguments().getString(ARG_REG_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        auth = FirebaseAuth.getInstance();
        categories.clear();
        eventTypes.clear();
        View rootView = inflater.inflate(R.layout.fragment_single_pup_v_registration, container, false);
        TextView companyEmail = rootView.findViewById(R.id.company_email);
        TextView companyName = rootView.findViewById(R.id.company_name);
        TextView companyAddress = rootView.findViewById(R.id.company_address);
        TextView companyNumber = rootView.findViewById(R.id.company_number);
        TextView companyDescription = rootView.findViewById(R.id.company_description);
        TextView monday = rootView.findViewById(R.id.workhours_monday);
        TextView tuesday = rootView.findViewById(R.id.workhours_tuesday);
        TextView wednesday = rootView.findViewById(R.id.workhours_wednesday);
        TextView thursday = rootView.findViewById(R.id.workhours_thursday);
        TextView friday = rootView.findViewById(R.id.workhours_friday);
        TextView saturday = rootView.findViewById(R.id.workhours_saturday);
        TextView sunday = rootView.findViewById(R.id.workhours_sunday);


        TextView userName = rootView.findViewById(R.id.user_name);
        TextView userEmail = rootView.findViewById(R.id.user_email);
        TextView userAddress = rootView.findViewById(R.id.user_address);
        TextView userNumber = rootView.findViewById(R.id.user_number);

        Button acceptButton = rootView.findViewById(R.id.button_accept);
        Button declineButton = rootView.findViewById(R.id.button_decline);
        ListView eventsListView = rootView.findViewById(R.id.event_list_view);
        ListView categoresListView = rootView.findViewById(R.id.categories_list_view);
        getCategories(registration.getCategories(),categoresListView);
        getEvents(registration.getEventTypes(),eventsListView);


        companyEmail.setText(registration.getCompanyEmail());
        companyAddress.setText(registration.getCompanyAddress());
        companyName.setText(registration.getCompanyName());
        companyDescription.setText(registration.getCompanyDescription());
        companyNumber.setText(registration.getCompanyNumber());
        userName.setText(registration.getUser().getFirstName());
        userEmail.setText(registration.getUser().getEmail());
        userAddress.setText(registration.getUser().getAddress());
        userNumber.setText(registration.getUser().getPhoneNumber());
        monday.setText(registration.getWorkHours().getMonday());
        tuesday.setText(registration.getWorkHours().getTuesday());
        wednesday.setText(registration.getWorkHours().getWednesday());
        thursday.setText(registration.getWorkHours().getThursday());
        friday.setText(registration.getWorkHours().getFriday());
        saturday.setText(registration.getWorkHours().getSaturday());
        sunday.setText(registration.getWorkHours().getSunday());

        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRejectionDialog();
            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmationDialog();
            }
        });


        return rootView;
    }

    private void showConfirmationDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setTitle("Accept registration");

        builder.setMessage("Are you sure you want to except this registration?");
        builder.setPositiveButton("Yes",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                auth.createUserWithEmailAndPassword(registration.getUser().getEmail(),registration.getUser().getPassword())
                        .addOnSuccessListener(authResult -> {
                            FirebaseUser firebaseUser = authResult.getUser();
                            String userId = firebaseUser.getUid();
                            FirebaseFirestore.getInstance().collection("users").document(userId)
                                    .set(registration.getUser())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void unused) {
                                            Toast.makeText(requireContext(),"Registration successful",Toast.LENGTH_SHORT).show();
                                            FirebaseFirestore.getInstance().collection("pupvRegistrationRequests")
                                                    .document(documentId) // Pretpostavljam da registration ima requestId
                                                    .delete();
                                            executor.execute(()->{
                                                auth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful()){
                                                            Toast.makeText(requireContext(),"Email sent",Toast.LENGTH_SHORT).show();
                                                        }
                                                        else {
                                                            Toast.makeText(requireContext(),"Email sending failed!",Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                                Company newCompany = new Company(registration.getCompanyEmail(), registration.getCompanyName(), registration.getCompanyAddress(), registration.getCompanyNumber(), registration.getCompanyDescription(), registration.getWorkHours(), registration.getCategories(), registration.getEventTypes(), registration.getUser());                                                FirebaseFirestore.getInstance().collection("companies")
                                                        .document()
                                                        .set(newCompany);
                                            });
                                            FragmentTransition.to(PupVRegistrationsPageFragment.newInstance(),getActivity(),true,R.id.list_layout);

                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(requireContext(),"Greska priliko rada sam firestore-om",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        });
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void getCategories(ArrayList<String> categoriesIds,ListView categoryListView) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories")
                .whereIn(FieldPath.documentId(), categoriesIds)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Category category = document.toObject(Category.class);
                                Log.d("JESI UZEO KATEGORIJU ", "KATEGORIJA:" + category.getName());
                                if (category != null) {
                                    categories.add(category);
                                }
                            }
                        } else {
                            // Handle errors here
                        }
                        for(Category category : categories) {
                            categoryNames.add(category.getName());
                            Log.d("Category names", "Category NAMES :" + category.getName());
                        }
                        StringListAdapter categoriesAdapter = new StringListAdapter(requireContext(),categoryNames);
                        categoryListView.setAdapter(categoriesAdapter);
                    }
                });
    }

    private void showRejectionDialog() {
        // Inflate the dialog layout
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_decline_registration, null);

        // Create the AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(dialogView)
                .setTitle("Registration Rejection")
                .setPositiveButton("Submit", (dialog, id) -> {
                    EditText rejectionReasonEditText = dialogView.findViewById(R.id.rejection_reason);
                    String rejectionReason = rejectionReasonEditText.getText().toString();
                    String email = registration.getUser().getEmail().toString();

                    if (email != null && !email.isEmpty()) {
                        Log.d("Email", email);

                        // Kreiranje email intent-a
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse("mailto:"));
                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Registration declined");
                        intent.putExtra(Intent.EXTRA_TEXT, rejectionReason);
                        // Pokretanje email aplikacije
                        try {
                            startActivity(Intent.createChooser(intent, "Send mail..."));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(getContext(), "No email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "User email is null or empty", Toast.LENGTH_SHORT).show();
                    }

                })
                .setNegativeButton("Cancel", (dialog, id) -> {
                    // User cancelled the dialog
                    dialog.dismiss();
                });

        // Show the dialog
        builder.create().show();
    }



    private void getEvents(ArrayList<String>eventIds,ListView eventListView) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("eventTypes")
                .whereIn(FieldPath.documentId(), eventIds)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                Event event = document.toObject(Event.class);
                                Log.d("JESI UZEO EVENTOVE ", "EVENT:" + event.getName());
                                if (event != null) {
                                    eventTypes.add(event);
                                }
                            }
                        } else {
                            // Handle errors here
                        }
                        for (Event event : eventTypes) {
                            eventTypeNames.add(event.getName());
                            Log.d("Event names", "EVENT NAMES :" + event.getName());
                        }
                        StringListAdapter eventTypeAdapter = new StringListAdapter(requireContext(),eventTypeNames);
                        eventListView.setAdapter(eventTypeAdapter);
                    }
                });
    }



}