package com.example.eventplanner.fragments.priceList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleServicePriceListBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Service;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SingleServicePriceListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SingleServicePriceListFragment extends Fragment {

    private FragmentSingleServicePriceListBinding binding;
    private Service mService;
    private String documentId;

    private static final String ARG_PRODUCT = "product";
    private static final String ARG_PRODUCT_ID = "productId";

    public SingleServicePriceListFragment() {
        // Required empty public constructor
    }

   
    public static SingleServicePriceListFragment newInstance(Service product, String documentId) {
        SingleServicePriceListFragment fragment = new SingleServicePriceListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        args.putString(ARG_PRODUCT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mService = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_PRODUCT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSingleServicePriceListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        EditText productName = binding.productNameEdit;
        EditText productPrice = binding.productPriceEdit;
        EditText productDiscount = binding.productDiscountEdit;
        EditText productHours=binding.serviceHoursEdit;
        Button submitButton = binding.buttonEditProduct;

        if (mService != null) {
            productName.setText(mService.getName());
            productPrice.setText(String.valueOf(mService.getPricePerHour()*mService.getHours()));
            productDiscount.setText(String.valueOf(mService.getDiscount()));
            productHours.setText(String.valueOf(mService.getHours()));

        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                Service service = new Service(mService.getCategory(),mService.getSubcategory(),mService.getName(),mService.getDescription(),mService.getGallery(),mService.getSpecificity(),Double.parseDouble(productPrice.getText().toString()),Integer.parseInt(productHours.getText().toString()),mService.getLocation(),Double.parseDouble(productDiscount.getText().toString()),mService.getPersons(),mService.getEventTypes(),mService.getDeadlineForReservation(),mService.getDeadlineForCancel(),mService.isAutomaticAcceptance(),mService.isAvailable(),mService.isVisible(),mService.getUserEmail(),mService.getCompany());
                showEditConfirmationDialog(service);
            }
        });

        productPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Not needed
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    try {
                        // Attempt to parse the input as a decimal number
                        Double.parseDouble(s.toString());
                        productPrice.setText(s.toString());
                    } catch (NumberFormatException e) {
                        productPrice.setText("");
                    }
                }
            }
        });


        return root;
    }

    private void showEditConfirmationDialog(Service product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit Product");
        builder.setMessage("Are you sure you want to edit this product?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("services").document(documentId);
                docRef.set(product)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {

                                FragmentTransition.to(PriceListPageFragment.newInstance(),getActivity(),true,R.id.list_layout_products);
                                Toast.makeText(getActivity(), "Service updated!", Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}