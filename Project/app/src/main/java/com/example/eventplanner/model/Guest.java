package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Guest implements Parcelable {
    private String name;
    private String age;  // Changed from int to String
    private boolean isInvited;
    private boolean hasAccepted;
    private String specialRequirements;

    public Guest(String name, String age, boolean isInvited, boolean hasAccepted, String specialRequirements) {
        this.name = name;
        this.age = age;  // Changed from int to String
        this.isInvited = isInvited;
        this.hasAccepted = hasAccepted;
        this.specialRequirements = specialRequirements;
    }

    public Guest() {}

    protected Guest(Parcel in) {
        name = in.readString();
        age = in.readString();  // Changed from in.readInt() to in.readString()
        isInvited = in.readByte() != 0;
        hasAccepted = in.readByte() != 0;
        specialRequirements = in.readString();
    }

    public static final Creator<Guest> CREATOR = new Creator<Guest>() {
        @Override
        public Guest createFromParcel(Parcel in) {
            return new Guest(in);
        }

        @Override
        public Guest[] newArray(int size) {
            return new Guest[size];
        }
    };


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {  // Changed return type to String
        return age;
    }

    public void setAge(String age) {  // Changed parameter type to String
        this.age = age;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean invited) {
        isInvited = invited;
    }

    public boolean isHasAccepted() {
        return hasAccepted;
    }

    public void setHasAccepted(boolean hasAccepted) {
        this.hasAccepted = hasAccepted;
    }

    public String getSpecialRequirements() {
        return specialRequirements;
    }

    public void setSpecialRequirements(String specialRequirements) {
        this.specialRequirements = specialRequirements;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(age);  // Changed from dest.writeInt(age) to dest.writeString(age)
        dest.writeByte((byte) (isInvited ? 1 : 0));
        dest.writeByte((byte) (hasAccepted ? 1 : 0));
        dest.writeString(specialRequirements);
    }
}
