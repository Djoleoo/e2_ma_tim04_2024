package com.example.eventplanner.fragments.reportedUsers;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.example.eventplanner.adapters.UserReportedListAdapter;
import com.example.eventplanner.databinding.FragmentUsersReportedBinding;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UsersReportedFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class UsersReportedFragment extends Fragment {

    private UserReportedListAdapter adapter;

    private ArrayList<DocumentSnapshot> mUserReports = new ArrayList<>();

    private FragmentUsersReportedBinding binding;
    private static final String ARG_PARAM = "param";

    private String mParam1;
    private String mParam2;

    public UsersReportedFragment() {
        // Required empty public constructor
    }

    public static UsersReportedFragment newInstance(ArrayList<DocumentSnapshot> products) {
        UsersReportedFragment fragment = new UsersReportedFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, products);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserReports = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new UserReportedListAdapter(getActivity(), mUserReports);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentUsersReportedBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}