package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class SubCategory implements Parcelable {

    public enum Status {
        SERVICE,
        PRODUCT
    }

    private String id;
    private String name;
    private String description;
    private Category category;
    private Status status;

    public SubCategory(String id, String name, String description, Category category, Status status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.category = category;
        this.status = status;
    }

    public SubCategory(){

    }

    public SubCategory(String name, String description, Category category, Status status) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.status = status;
    }


    protected SubCategory(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        category = in.readParcelable(Category.class.getClassLoader());
        status = Status.valueOf(in.readString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeParcelable(category, flags);
        dest.writeString(status.name());
    }

    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };


}
