package com.example.eventplanner.fragments.registration;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEventsAndWorkHoursBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.PUPVRegistration;
import com.example.eventplanner.model.WorkHours;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class EventsAndWorkHoursFragment extends Fragment {
    private FragmentEventsAndWorkHoursBinding binding;
    private static final String USER_TYPE_PARAM = "userType";

    private String usertype;
    private PUPVRegistration registration;

    private ArrayList<DocumentSnapshot> mEventTypeDocuments = new ArrayList<>();
    private ArrayList<String> eventTypeIds = new ArrayList<>();


    public EventsAndWorkHoursFragment() {
        // Required empty public constructor
    }

    public static EventsAndWorkHoursFragment newInstance(String selectedRole , PUPVRegistration registration) {
        EventsAndWorkHoursFragment fragment = new EventsAndWorkHoursFragment();
        Bundle args = new Bundle();
        args.putString(USER_TYPE_PARAM,selectedRole);
        args.putParcelable("registration", registration);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usertype = getArguments().getString(USER_TYPE_PARAM);
            registration = getArguments().getParcelable("registration");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventsAndWorkHoursBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        LinearLayout eventTypeContainer = binding.eventTypeContainer;
        selectEventTypes(getActivity(),eventTypeContainer);

        EditText monday = binding.monday;
        EditText tuesday = binding.tuesday;
        EditText wednesday = binding.wednesday;
        EditText thursday = binding.thursday;
        EditText friday = binding.friday;
        EditText saturday = binding.saturday;
        EditText sunday = binding.sunday;

        Button nextButton = binding.nextBtnWorkHours;
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkHours workHours = new WorkHours(monday.getText().toString(),tuesday.getText().toString(),wednesday.getText().toString(),thursday.getText().toString(),friday.getText().toString(),saturday.getText().toString(),sunday.getText().toString());
                registration.setWorkHours(workHours);
                findCheckedEventTypes(eventTypeContainer, new Runnable() {
                    @Override
                    public void run() {
                        registration.setEventTypes(eventTypeIds);
                        FragmentTransition.to(PersonalInfoFragment.newInstance(usertype,registration),getActivity(),true,R.id.loginCard);
                    }
                });
            }
        });
        return root;
    }


    private void selectEventTypes(FragmentActivity currentActivity, LinearLayout eventTypeContainer){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("eventTypes").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mEventTypeDocuments.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                mEventTypeDocuments.add(document);
                                String categoryName = document.toObject(Category.class).getName();
                                CheckBox checkBox = new CheckBox(currentActivity);
                                checkBox.setText(categoryName);
                                checkBox.setTag(document.getId());
                                eventTypeContainer.addView(checkBox);
                            }
                        } else {
                            Toast.makeText(currentActivity, "Error getting categories", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void findCheckedEventTypes(LinearLayout categoryContainer, Runnable onComplete) {
        for (DocumentSnapshot document : mEventTypeDocuments) {
            String documentId = document.getId();
            CheckBox checkBox = (CheckBox) categoryContainer.findViewWithTag(documentId);
            if (checkBox != null && checkBox.isChecked()) {
                eventTypeIds.add(documentId);
            }
        }
        // Nakon završetka pretrage, pozovite onComplete metodu
        onComplete.run();
    }
}