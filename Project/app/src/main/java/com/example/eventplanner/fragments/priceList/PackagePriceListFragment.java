package com.example.eventplanner.fragments.priceList;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.example.eventplanner.adapters.PackagePriceListAdapter;

import com.example.eventplanner.databinding.FragmentPackagePriceListBinding;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductsPriceListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PackagePriceListFragment extends Fragment {

    private PackagePriceListAdapter adapter;

    private ArrayList<DocumentSnapshot> mProducts = new ArrayList<>();

    private FragmentPackagePriceListBinding binding;
    private static final String ARG_PARAM = "param";


    private String mParam1;
    private String mParam2;

    public PackagePriceListFragment() {
        // Required empty public constructor
    }



    public static PackagePriceListFragment newInstance(ArrayList<DocumentSnapshot> products) {
        PackagePriceListFragment fragment = new PackagePriceListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, products);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProducts = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new PackagePriceListAdapter(getActivity(), mProducts);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPackagePriceListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}