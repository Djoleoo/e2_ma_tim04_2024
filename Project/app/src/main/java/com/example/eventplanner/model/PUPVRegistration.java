package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

public class PUPVRegistration implements Parcelable {

    private String companyEmail;
    private String companyName;
    private String companyAddress;
    private String companyNumber;
    private String companyDescription;
    private String companyImage;
    private ArrayList<String> categoryIds;
    private ArrayList<String> eventTypeIds;
    private User user;
    private WorkHours workHours;
    private Date creationTime;

    public PUPVRegistration() {
    }

    protected PUPVRegistration(Parcel in) {
        companyEmail = in.readString();
        companyName = in.readString();
        companyAddress = in.readString();
        companyNumber = in.readString();
        companyDescription = in.readString();
        categoryIds = in.createStringArrayList();
        companyImage = in.readString();
        eventTypeIds = in.createStringArrayList();
        user = in.readParcelable(User.class.getClassLoader());
        workHours = in.readParcelable(WorkHours.class.getClassLoader());
        creationTime = new Date(in.readLong());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(companyEmail);
        dest.writeString(companyName);
        dest.writeString(companyAddress);
        dest.writeString(companyNumber);
        dest.writeString(companyDescription);
        dest.writeStringList(categoryIds);
        dest.writeString(companyImage);
        dest.writeStringList(eventTypeIds);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(workHours, flags);
        dest.writeLong(creationTime != null ? creationTime.getTime() : -1);
    }

    public static final Creator<PUPVRegistration> CREATOR = new Creator<PUPVRegistration>() {
        @Override
        public PUPVRegistration createFromParcel(Parcel in) {
            return new PUPVRegistration(in);
        }

        @Override
        public PUPVRegistration[] newArray(int size) {
            return new PUPVRegistration[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public ArrayList<String> getCategories() {
        return categoryIds;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categoryIds = categories;
    }

    public ArrayList<String> getEventTypes() {
        return eventTypeIds;
    }

    public void setEventTypes(ArrayList<String> eventTypes) {
        this.eventTypeIds = eventTypes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public WorkHours getWorkHours() {
        return workHours;
    }

    public void setWorkHours(WorkHours workHours) {
        this.workHours = workHours;
    }

    public String getCompanyImage() {
        return companyImage;
    }

    public void setCompanyImage(String companyImage) {
        this.companyImage = companyImage;
    }

    public Date getCreationTime() {
        return creationTime;
    }
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
