package com.example.eventplanner.fragments.products;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ProductListAdapter;
import com.example.eventplanner.adapters.ServiceListAdapter;
import com.example.eventplanner.adapters.PackageListAdapter;
import com.example.eventplanner.model.Favorites;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class FavoritesFragment extends Fragment {

    private FirebaseFirestore db;
    private ListView productsListView;
    private ListView servicesListView;
    private ListView packagesListView;

    private ProductListAdapter productListAdapter;
    private ServiceListAdapter serviceListAdapter;
    private PackageListAdapter packageListAdapter;

    private ArrayList<DocumentSnapshot> favoriteProducts;
    private ArrayList<DocumentSnapshot> favoriteServices;
    private ArrayList<DocumentSnapshot> favoritePackages;

    private User currentUser;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorites, container, false);

        productsListView = rootView.findViewById(R.id.products_list_view);
        servicesListView = rootView.findViewById(R.id.services_list_view);
        packagesListView = rootView.findViewById(R.id.packages_list_view);

        favoriteProducts = new ArrayList<>();
        favoriteServices = new ArrayList<>();
        favoritePackages = new ArrayList<>();

        db = FirebaseFirestore.getInstance();

        loadCurrentUser();  // Load the current user first

        return rootView;
    }

    private void loadCurrentUser() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            currentUser = documentSnapshot.toObject(User.class);
                            loadFavorites();  // Load favorites after the current user is loaded
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Handle any errors here
                    });
        }
    }

    private void loadFavorites() {
        if (currentUser == null) {
            return;  // Ensure currentUser is not null before loading favorites
        }

        db.collection("favorites").whereEqualTo("user.id", currentUser.getId()).get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Favorites favorites = document.toObject(Favorites.class);

                            if (favorites != null) {
                                loadFavoriteProducts(favorites.getFavoriteProducts());
                                loadFavoriteServices(favorites.getFavoriteServices());
                                loadFavoritePackages(favorites.getFavoritePackages());
                            }
                        }
                    }
                });
    }

    private void loadFavoriteProducts(List<Product> products) {
        for (Product product : products) {
            db.collection("products").document(product.getId()).get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    favoriteProducts.add(documentSnapshot);
                    productListAdapter.notifyDataSetChanged();
                }
            });
        }
        productListAdapter = new ProductListAdapter(getActivity(), favoriteProducts);
        productsListView.setAdapter(productListAdapter);
    }

    private void loadFavoriteServices(List<Service> services) {
        for (Service service : services) {
            db.collection("services").document(service.getId()).get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    favoriteServices.add(documentSnapshot);
                    serviceListAdapter.notifyDataSetChanged();
                }
            });
        }
        serviceListAdapter = new ServiceListAdapter(getActivity(), favoriteServices);
        servicesListView.setAdapter(serviceListAdapter);
    }

    private void loadFavoritePackages(List<Package> packages) {
        for (Package aPackage : packages) {
            db.collection("packages").document(aPackage.getId()).get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    favoritePackages.add(documentSnapshot);
                    packageListAdapter.notifyDataSetChanged();
                }
            });
        }
        packageListAdapter = new PackageListAdapter(getActivity(), favoritePackages);
        packagesListView.setAdapter(packageListAdapter);
    }
}
