package com.example.eventplanner.fragments.employeeManagement;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.renderscript.ScriptGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEmployeeManagementBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.registration.EmployeeRegistrationFragment;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeeManagementFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeManagementFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentEmployeeManagementBinding binding;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = auth.getCurrentUser();
    private String userType;

    public EmployeeManagementFragment() {
        checkLoggedInUser();
    }


    public void checkLoggedInUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            String email = currentUser.getEmail();
            String uid = currentUser.getUid();
            boolean isEmailVerified = currentUser.isEmailVerified();

            System.out.println("Logged in user email: " + email);
            System.out.println("User ID: " + uid);
            System.out.println("Is email verified: " + isEmailVerified);
            getUserType();
        } else {
            System.out.println("No user is currently logged in.");
        }
    }

    public void getUserType() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            String uid = currentUser.getUid();

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("users").document(uid);

            docRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    userType = documentSnapshot.getString("userType");
                    System.out.println("User Type: " + userType);
                } else {
                    System.out.println("No such document exists!");
                }
            }).addOnFailureListener(e -> {
                System.out.println("Error retrieving document: " + e.getMessage());
            });
        } else {
            System.out.println("No user is currently logged in.");
        }
    }



    public static EmployeeManagementFragment newInstance(String param1, String param2) {
        EmployeeManagementFragment fragment = new EmployeeManagementFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentEmployeeManagementBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        // Register button
        Button buttonRegisterEmployee = binding.buttonRegisterEmployee;
        buttonRegisterEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransition.to(EmployeeRegistrationFragment.newInstance("", ""), getActivity(), true, R.id.parentlayout);
            }
        });

        // View all employees button
        Button buttonList = binding.buttonViewAllEmployees;
        buttonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransition.to(EmployeeListFragment.newInstance(), getActivity(), true, R.id.parentlayout);
            }
        });

        // Search button
        Button buttonSearch = binding.buttonSearch;
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchText = binding.editTextSearchEmployee.getText().toString();
                FragmentTransition.to(EmployeeInfoFragment.newInstance(searchText, ""), getActivity(), true, R.id.parentlayout);
            }
        });

        return root;
    }



}