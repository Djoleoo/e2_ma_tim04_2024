package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.SingleCategoryFragment;
import com.example.eventplanner.fragments.events.SingleEventFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class EventListAdapter extends ArrayAdapter<DocumentSnapshot> {

    private ArrayList<DocumentSnapshot> mDocuments;
    private FragmentActivity mContext;

    public EventListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> documents){
        super(context, R.layout.event_list_item);
        mContext = context;
        mDocuments = documents;
    }

    @Override
    public int getCount() {
        return mDocuments.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return mDocuments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.event_list_item,
                    parent, false);
        }
        LinearLayout eventItem = convertView.findViewById(R.id.event_list_item);
        TextView eventName = convertView.findViewById(R.id.event_name);
        Button viewButton = convertView.findViewById(R.id.view_button);
        Button deleteButton = convertView.findViewById(R.id.delete_button);


        if(document != null){
            String eventNameText = document.getString("name");
            eventName.setText(eventNameText);

            boolean isDisabled = document.getBoolean("disabled");
            if(isDisabled){
                eventItem.setEnabled(false);
                eventItem.setBackgroundColor(Color.LTGRAY);
                viewButton.setVisibility(View.GONE);
                deleteButton.setText("Enable");
            }
        }

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event event = document.toObject(Event.class);
                View parent = (View) v.getParent();
                FloatingActionButton addBtn = parent.getRootView().findViewById(R.id.floating_action_button);
                addBtn.setVisibility(View.GONE);
                FragmentTransition.to(SingleEventFragment.newInstance(event,document.getId()),mContext,true,R.id.list_layout);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deleteButton.getText().equals("Disable")) {
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    DocumentReference docRef = db.collection("eventTypes").document(document.getId());
                    docRef.update("disabled",true)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unused) {
                                    eventItem.setEnabled(false);
                                    eventItem.setBackgroundColor(Color.LTGRAY);
                                    viewButton.setVisibility(View.GONE);
                                    deleteButton.setText("Enable");// Postavite pozadinu na sivu boju
                                }
                            })
                            .addOnFailureListener(e -> Log.w("REZ_DB", "Error getting documents.", e));
                }else if (deleteButton.getText().equals("Enable")){
                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    DocumentReference docRef = db.collection("eventTypes").document(document.getId());
                    docRef.update("disabled",false)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unused) {
                                    eventItem.setEnabled(true);
                                    eventItem.setBackgroundColor(Color.TRANSPARENT);
                                    viewButton.setVisibility(View.VISIBLE);
                                    deleteButton.setText("Disable");
                                }
                            })
                            .addOnFailureListener(e -> Log.d("REZ_DB", "Error getting documents.", e));
                }

            }
        });

        return convertView;
    }

}
