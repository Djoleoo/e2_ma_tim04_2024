package com.example.eventplanner.fragments.categories;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentAddSubCategoryBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class AddSubCategoryFragment extends Fragment {

    private ArrayList<Category> categories = new ArrayList<>();
    private FragmentAddSubCategoryBinding binding;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    public AddSubCategoryFragment() {
        // Required empty public constructor
    }


    public static AddSubCategoryFragment newInstance() {
        AddSubCategoryFragment fragment = new AddSubCategoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       binding = FragmentAddSubCategoryBinding.inflate(getLayoutInflater());
       View root = binding.getRoot();
       selectCategories(new OnCategoriesLoadedListener() {
           @Override
           public void onCategoriesLoaded() {
               // Kreiraj adapter nakon što su kategorije učitane
               setupSpinner();
           }
       });
       EditText subCategoryName = binding.subCategoryName;
       TextView subCategoryDescription = binding.descriptionTextview;
       Button button = binding.createSubcategoryBtn;
       button.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String name = subCategoryName.getText().toString();
               String description = subCategoryDescription.getText().toString();
               Spinner categorySpinner = binding.categoryBtn;
               Spinner typeSpinner = binding.typeBtn;
               String selectedCategoryName = (String) categorySpinner.getSelectedItem();

               Category selectedCategory = null;
               for (Category category : categories) {
                   if (category.getName().equals(selectedCategoryName)) {
                       selectedCategory = category;
                       break;
                   }
               }
               if (selectedCategory != null) {
                   String selectedStatus = (String) typeSpinner.getSelectedItem();
                   SubCategory.Status status = SubCategory.Status.valueOf(selectedStatus.toUpperCase());
                   // Kreiranje podkategorije sa imenom, opisom i odgovarajućom kategorijom
                   SubCategory subCategory = new SubCategory(name, description, selectedCategory, status);
                   // Prikazivanje dijaloga za potvrdu
                   showConfirmationDialog(subCategory);
               } else {
                   // Ako nije pronađena odgovarajuća kategorija
                   Toast.makeText(getActivity(), "Selected category not found", Toast.LENGTH_SHORT).show();
               }
           }
       });
       Spinner typeSpinner = binding.typeBtn;
       ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
               android.R.layout.simple_spinner_item,
               getResources().getStringArray(R.array.subCategory_role));
       arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       typeSpinner.setAdapter(arrayAdapter);

       return root;
    }

    private void showConfirmationDialog(SubCategory subCategory) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add SubCategory");
        builder.setMessage("Are you sure you want to add this subCategory?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("subCategories").add(subCategory)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                FragmentTransition.to(CategoriesPageFragment.newInstance(),getActivity(),true,R.id.list_layout);
                                Toast.makeText(getActivity(), "SubCategory created!", Toast.LENGTH_SHORT).show();
                                FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                addBtn.setVisibility(View.VISIBLE);
                                MaterialButtonToggleGroup toggleGroup = requireActivity().findViewById(R.id.toggle_group);
                                toggleGroup.check(R.id.button_categories); // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                            public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Something went wrong,try again!", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setupSpinner() {
        Spinner categorySpinner = binding.categoryBtn;
        ArrayAdapter<String> categoryArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getCategoryNames());
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoryArrayAdapter);
    }

    private void selectCategories(OnCategoriesLoadedListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            categories.clear();
                            // Prolazimo kroz rezultate upita i dodajemo kategorije u listu
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Category category = document.toObject(Category.class);
                                categories.add(category);
                            }
                            listener.onCategoriesLoaded();
                        } else {
                            Toast.makeText(getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private ArrayList<String> getCategoryNames() {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
        }
        return categoryNames;
    }

    interface OnCategoriesLoadedListener {
        void onCategoriesLoaded();
    }
}