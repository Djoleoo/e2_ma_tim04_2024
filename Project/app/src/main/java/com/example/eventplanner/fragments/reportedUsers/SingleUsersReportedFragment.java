package com.example.eventplanner.fragments.reportedUsers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleProductBinding;
import com.example.eventplanner.databinding.FragmentSingleUsersReportedBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.ProductsPageFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.report.UserReport;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SingleUsersReportedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SingleUsersReportedFragment extends Fragment {

    private FragmentSingleUsersReportedBinding binding;
    private UserReport mProduct;
    private String documentId;





    private static final String ARG_PRODUCT = "product";
    private static final String ARG_PRODUCT_ID = "productId";

    public SingleUsersReportedFragment() {
        // Required empty public constructor
    }



    public static SingleUsersReportedFragment newInstance(UserReport product,String documentId) {
        SingleUsersReportedFragment fragment = new SingleUsersReportedFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        args.putString(ARG_PRODUCT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_PRODUCT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSingleUsersReportedBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        EditText userReportEmail=binding.userReportEdit;
        EditText userReportedFirstName=binding.firstNameEdit;
        EditText userReportedLastName=binding.lastNameEdit;
        EditText userReportedEmail=binding.emailEdit;
        EditText userReportedPhone=binding.phoneNumberEdit;
        EditText userReportedAddress=binding.addressEdit;
        EditText timeReport=binding.timeEdit;
        EditText reasonReport=binding.reasonEdit;
        EditText status=binding.statusEdit;
        Button acceptBtn=binding.buttonAccept;
        Button declineBtn=binding.buttonDecline;

        if (mProduct != null) {
            userReportEmail.setText(mProduct.getUserReport());
            userReportedFirstName.setText(mProduct.getUserReported().getFirstName());
            userReportedLastName.setText(mProduct.getUserReported().getLastName());
            userReportedEmail.setText(mProduct.getUserReported().getEmail());
            userReportedPhone.setText(mProduct.getUserReported().getPhoneNumber());
            userReportedAddress.setText(mProduct.getUserReported().getAddress());
            timeReport.setText(mProduct.getTime().toString());
            reasonReport.setText(mProduct.getReason());
            status.setText(mProduct.getStatus().toString());

        }

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserReport product = new UserReport(mProduct.getUserReport(),mProduct.getUserReported(),mProduct.getTime(),mProduct.getReason(), UserReport.Status.ACCEPTED);

                showConfirmationDialog(product);
            }
        });

        declineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserReport product = new UserReport(mProduct.getUserReport(),mProduct.getUserReported(),mProduct.getTime(),mProduct.getReason(), UserReport.Status.REJECTED);
                showDeclinationDialog(product);
            }
        });




        return root;
    }

    private void showConfirmationDialog(UserReport product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Accept report");
        builder.setMessage("Are you sure you want to accept this report?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("userReports").document(documentId);
                docRef.set(product)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {


                                FragmentTransition.to(UsersReportedPageFragment.newInstance(),getActivity(),true,R.id.list_layout_reports);
                                Toast.makeText(getActivity(), "Accepted!", Toast.LENGTH_SHORT).show();
                                updateUserEnabledStatusByEmail(mProduct.getUserReported().getEmail());
                                //FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                //addBtn.setVisibility(View.VISIBLE);
                                //MaterialButtonToggleGroup toggleGroup = requireActivity().findViewById(R.id.toggle_group);
                                //toggleGroup.check(R.id.button_categories); // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDeclinationDialog(UserReport product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Decline report");
        builder.setMessage("Are you sure you want to decline this report?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("userReports").document(documentId);
                docRef.set(product)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {

                                FragmentTransition.to(UsersReportedPageFragment.newInstance(),getActivity(),true,R.id.list_layout_reports);
                                Toast.makeText(getActivity(), "Declined!", Toast.LENGTH_SHORT).show();
                                //FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                //addBtn.setVisibility(View.VISIBLE);
                                //MaterialButtonToggleGroup toggleGroup = requireActivity().findViewById(R.id.toggle_group);
                                //toggleGroup.check(R.id.button_categories); // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateUserEnabledStatusByEmail(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .whereEqualTo("email", email)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                                // Assuming there may be more than one user with the same email
                                String userId = document.getId();
                                updateUserEnabledStatus(userId);
                            }
                        } else {
                            Toast.makeText(getActivity(), "No user found with the provided email.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to find user by email.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateUserEnabledStatus(String userId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference userRef = db.collection("users").document(userId);
        userRef.update("enabled", true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getActivity(), "User status updated successfully!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to update user status.", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}