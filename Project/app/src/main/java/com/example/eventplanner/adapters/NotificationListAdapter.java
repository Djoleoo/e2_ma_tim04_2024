package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Notification;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class NotificationListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private ArrayList<DocumentSnapshot> aProduct;
    private FragmentActivity mContext;
    private FirebaseFirestore db;

    public NotificationListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> products) {
        super(context, R.layout.notificaiton_list_item, products);
        mContext = context;
        aProduct = products;
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public int getCount() {
        return aProduct.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.notificaiton_list_item, parent, false);
        }

        TextView title=convertView.findViewById(R.id.notification_title);
        TextView text=convertView.findViewById(R.id.notification_text);
        TextView read=convertView.findViewById(R.id.notification_read);


        Button readButton = convertView.findViewById(R.id.view_notification_button);


        if (document != null) {
            Notification product = document.toObject(Notification.class);

           title.setText(product.getTitle());
           text.setText(product.getText());
           read.setText(product.getRead().toString());





            readButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Update the read status in Firestore
                    db.collection("notifications").document(document.getId())
                            .update("read", true)
                            .addOnSuccessListener(aVoid -> {
                                // Update the local status and notify the adapter
                                product.setRead(true);

                                //read.setText("Read");
                            })
                            .addOnFailureListener(e -> {
                                // Handle the error
                            });
                }
            });



        }

        return convertView;
    }

}
