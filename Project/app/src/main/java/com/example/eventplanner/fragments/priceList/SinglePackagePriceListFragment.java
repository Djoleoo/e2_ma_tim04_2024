package com.example.eventplanner.fragments.priceList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSinglePackagePriceListBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SinglePackagePriceListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SinglePackagePriceListFragment extends Fragment {

    private FragmentSinglePackagePriceListBinding binding;
    private Package mPackage;
    private String documentId;

    private static final String ARG_PRODUCT = "product";
    private static final String ARG_PRODUCT_ID = "productId";

    public SinglePackagePriceListFragment() {
        // Required empty public constructor
    }


    public static SinglePackagePriceListFragment newInstance(Package product,String documentId) {
        SinglePackagePriceListFragment fragment = new SinglePackagePriceListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        args.putString(ARG_PRODUCT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPackage = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_PRODUCT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSinglePackagePriceListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        EditText productName = binding.productNameEdit;
        EditText productDiscount = binding.productDiscountEdit;
        Button submitButton = binding.buttonEditProduct;

        if (mPackage != null) {
            productName.setText(mPackage.getName());
            productDiscount.setText(String.valueOf(mPackage.getDiscount()));

        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                Package package1= new Package(productName.getText().toString(),mPackage.getDescription(),Double.parseDouble(productDiscount.getText().toString()),mPackage.isVisible(),mPackage.isAvailable(),mPackage.getCategory(),mPackage.getProducts(),mPackage.getServices(),mPackage.isAutomaticAcceptance(),mPackage.getUserEmail(),mPackage.getCompany());
                showEditConfirmationDialog(package1);
            }
        });




        return root;
    }

    private void showEditConfirmationDialog(Package product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit Package");
        builder.setMessage("Are you sure you want to edit this product?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("packages").document(documentId);
                docRef.set(product)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {

                                FragmentTransition.to(PriceListPageFragment.newInstance(),getActivity(),true, R.id.list_layout_products);
                                Toast.makeText(getActivity(), "Package updated!", Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}