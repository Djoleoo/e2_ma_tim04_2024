package com.example.eventplanner.fragments.products;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.example.eventplanner.adapters.ServiceListAdapter;
import com.example.eventplanner.databinding.FragmentServicesBinding;
import com.example.eventplanner.model.Service;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class ServicesFragment extends ListFragment {

    private ServiceListAdapter adapter;
    private ArrayList<DocumentSnapshot> mServices;

    private FragmentServicesBinding binding;
    private static final String ARG_PARAM = "param";

    private String mParam1;
    private String mParam2;

    public ServicesFragment() {
        // Required empty public constructor
    }

    public static ServicesFragment newInstance(ArrayList<DocumentSnapshot> services) {
        ServicesFragment fragment = new ServicesFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, services);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mServices = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new ServiceListAdapter(getActivity(), mServices);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentServicesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}