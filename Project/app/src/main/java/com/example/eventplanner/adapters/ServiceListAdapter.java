package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.SingleServiceFragment;
import com.example.eventplanner.model.Favorites;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class ServiceListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private ArrayList<DocumentSnapshot> aService;
    private FragmentActivity mContext;
    private FirebaseFirestore db;
    private User currentUser;

    public ServiceListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> services) {
        super(context, R.layout.service_list_item);
        mContext = context;
        aService = services;
        db = FirebaseFirestore.getInstance();
        loadCurrentUser();
    }

    private void loadCurrentUser() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            currentUser = documentSnapshot.toObject(User.class);
                            // Notify the adapter to refresh the list after the user is loaded
                            notifyDataSetChanged();
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Handle any errors here
                    });
        }
    }

    @Override
    public int getCount() {
        return aService.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aService.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.service_list_item, parent, false);
        }

        LinearLayout serviceItem = convertView.findViewById(R.id.service_list_item);
        TextView serviceName = convertView.findViewById(R.id.service_name);
        Button viewButton = convertView.findViewById(R.id.view_service_button);
        Button addToFavorites = convertView.findViewById(R.id.service_to_favorites_button);
        Button deleteButton = convertView.findViewById(R.id.delete_service_button);

        ImageView imageView = convertView.findViewById(R.id.service_image);

        if (document != null) {
            serviceName.setText(document.getString("name"));
            // Update the favorite button text based on whether the service is in favorites
            checkIfServiceIsFavorite(document.getId(), addToFavorites);
        }

        viewButton.setOnClickListener(v -> {
            Service service = document.toObject(Service.class);
            FragmentTransition.to(SingleServiceFragment.newInstance(service, document.getId()), mContext, true, R.id.list_layout_products);
        });

        addToFavorites.setOnClickListener(v -> {
            Service service = document.toObject(Service.class);
            if (service != null) {
                toggleFavoriteStatus(document.getId(), service, addToFavorites);
            }
        });

        if (currentUser != null && currentUser.getUserType() == User.UserType.OD) {
            addToFavorites.setVisibility(View.VISIBLE);
        } else {
            addToFavorites.setVisibility(View.GONE);
        }

        deleteButton.setOnClickListener(v -> showDeleteConfirmationDialog(document));

        return convertView;
    }

    private void checkIfServiceIsFavorite(String serviceId, Button addToFavorites) {
        if (currentUser == null) {
            // If currentUser is not loaded, return and avoid further execution
            return;
        }

        db.collection("favorites").whereEqualTo("user.id", currentUser.getId())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot favoriteDoc = queryDocumentSnapshots.getDocuments().get(0);
                        Favorites favorites = favoriteDoc.toObject(Favorites.class);

                        if (favorites != null && favorites.getFavoriteServices().stream().anyMatch(s -> s.getId().equals(serviceId))) {
                            addToFavorites.setText("Remove from favorites");
                        } else {
                            addToFavorites.setText("Favorite");
                        }
                    } else {
                        addToFavorites.setText("Favorite");
                    }
                })
                .addOnFailureListener(e -> {
                    Log.d("FAIL LOAD FAVORITE", "Failed to load favorites.");
                });
    }

    private void toggleFavoriteStatus(String serviceId, Service service, Button addToFavorites) {
        if (currentUser == null) {
            Toast.makeText(mContext, "User not loaded. Please try again.", Toast.LENGTH_SHORT).show();
            return;
        }

        db.collection("favorites").whereEqualTo("user.id", currentUser.getId())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot favoriteDoc = queryDocumentSnapshots.getDocuments().get(0);
                        Favorites favorites = favoriteDoc.toObject(Favorites.class);

                        if (favorites != null) {
                            if (favorites.getFavoriteServices().stream().anyMatch(s -> s.getId().equals(serviceId))) {
                                favorites.removeFavoriteServiceById(serviceId);
                                addToFavorites.setText("Favorite");
                            } else {
                                service.setId(serviceId); // Ensure service has the correct ID
                                favorites.addFavoriteService(service);
                                addToFavorites.setText("Remove from favorites");
                            }

                            db.collection("favorites").document(favoriteDoc.getId())
                                    .set(favorites)
                                    .addOnSuccessListener(aVoid -> {
                                        Toast.makeText(mContext, "Favorites updated", Toast.LENGTH_SHORT).show();
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(mContext, "Failed to update favorites", Toast.LENGTH_SHORT).show();
                                    });
                        }
                    } else {
                        // No Favorites entry exists, create one
                        Favorites newFavorites = new Favorites();
                        newFavorites.setUser(currentUser);
                        service.setId(serviceId); // Ensure service has the correct ID
                        newFavorites.addFavoriteService(service);

                        db.collection("favorites").add(newFavorites)
                                .addOnSuccessListener(documentReference -> {
                                    addToFavorites.setText("Remove from favorites");
                                    Toast.makeText(mContext, "Added to favorites", Toast.LENGTH_SHORT).show();
                                })
                                .addOnFailureListener(e -> {
                                    Toast.makeText(mContext, "Failed to add to favorites", Toast.LENGTH_SHORT).show();
                                });
                    }
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(mContext, "Failed to load favorites", Toast.LENGTH_SHORT).show();
                });
    }

    private void showDeleteConfirmationDialog(DocumentSnapshot document) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete service");
        builder.setMessage("Are you sure you want to delete this service?");
        builder.setPositiveButton("Yes", (dialog, which) -> {
            db.collection("services").document(document.getId()).delete()
                    .addOnSuccessListener(aVoid -> {
                        aService.remove(document);
                        notifyDataSetChanged();
                    }).addOnFailureListener(e -> {
                        Toast.makeText(getContext(), "Failed to delete service", Toast.LENGTH_SHORT).show();
                    });
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
