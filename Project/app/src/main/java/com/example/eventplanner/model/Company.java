package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

public class Company implements Parcelable {

    private String email;
    private String name;
    private String address;
    private String number;
    private String description;
    private WorkHours workHours;
    private ArrayList<String> categoryIds;
    private ArrayList<String> eventTypeIds;
    private User user;
    private ArrayList<Comment> comments;

    public Company() {
        this.comments = new ArrayList<>();
    }

    public Company(String email, String name, String address, String number, String description, WorkHours workHours, ArrayList<String> categoryIds, ArrayList<String> eventTypeIds, User user) {
        this.email = email;
        this.name = name;
        this.address = address;
        this.number = number;
        this.description = description;
        this.workHours = workHours;
        this.categoryIds = categoryIds;
        this.eventTypeIds = eventTypeIds;
        this.user = user;
        this.comments = new ArrayList<>();
    }

    protected Company(Parcel in) {
        email = in.readString();
        name = in.readString();
        address = in.readString();
        number = in.readString();
        description = in.readString();
        workHours = in.readParcelable(WorkHours.class.getClassLoader());
        categoryIds = in.createStringArrayList();
        eventTypeIds = in.createStringArrayList();
        user = in.readParcelable(User.class.getClassLoader());
        comments = in.createTypedArrayList(Comment.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(number);
        dest.writeString(description);
        dest.writeParcelable(workHours, flags);
        dest.writeStringList(categoryIds);
        dest.writeStringList(eventTypeIds);
        dest.writeParcelable(user, flags);
        dest.writeTypedList(comments);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

    // Getters and Setters

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public WorkHours getWorkHours() {
        return workHours;
    }

    public void setWorkHours(WorkHours workHours) {
        this.workHours = workHours;
    }

    public ArrayList<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(ArrayList<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public ArrayList<String> getEventTypeIds() {
        return eventTypeIds;
    }

    public void setEventTypeIds(ArrayList<String> eventTypeIds) {
        this.eventTypeIds = eventTypeIds;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }
}
