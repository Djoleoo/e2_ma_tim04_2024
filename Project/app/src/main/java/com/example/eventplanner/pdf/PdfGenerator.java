package com.example.eventplanner.pdf;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.eventplanner.fragments.reportedUsers.UsersReportedPageFragment;
import com.example.eventplanner.model.Activity;
import com.example.eventplanner.model.Guest;
import com.example.eventplanner.model.Happening;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PdfGenerator {

    private Context context;
    private FirebaseFirestore db;

    private User user;

    public PdfGenerator(Context context) {
        this.context = context;
        db = FirebaseFirestore.getInstance();
    }

    public void generateGuestsPdf(Happening happening) {
        db.collection("happenings").document(happening.getId()).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                try {
                    File pdfFile = createGuestsPdfFile(happening.getName());
                    PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
                    PdfDocument pdfDocument = new PdfDocument(writer);
                    Document document = new Document(pdfDocument);

                    // Create a table with columns for guest details
                    float[] columnWidths = {200f, 100f, 300f};
                    Table table = new Table(columnWidths);

                    // Add table headers
                    table.addCell(new Cell().add(new Paragraph("Guest Name")));
                    table.addCell(new Cell().add(new Paragraph("Age Range")));
                    table.addCell(new Cell().add(new Paragraph("Special Requirements")));

                    // Add guest details to the table
                    for (Guest guest : happening.getGuests()) {
                        table.addCell(new Cell().add(new Paragraph(guest.getName())));
                        table.addCell(new Cell().add(new Paragraph(guest.getAge())));
                        table.addCell(new Cell().add(new Paragraph(guest.getSpecialRequirements())));
                    }

                    // Add the table to the document
                    document.add(table);

                    document.close();
                    writer.close();

                    Toast.makeText(context, "PDF created successfully: " + pdfFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
                    Log.d("PdfGenerator", "PDF generated and saved at: " + pdfFile.getAbsolutePath());

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Failed to create PDF", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to fetch happening data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private File createGuestsPdfFile(String happeningName) throws IOException {
        File pdfDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "EventPlanner");
        if (!pdfDir.exists()) {
            pdfDir.mkdirs();
        }
        File pdfFile = new File(pdfDir, "Guests_" + happeningName + ".pdf");
        if (!pdfFile.exists()) {
            pdfFile.createNewFile();
        }
        return pdfFile;
    }

    public void generateActivitiesPdf(Happening happening) {
        db.collection("happenings").document(happening.getId()).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                try {
                    File pdfFile = createActivitiesPdfFile(happening.getName());
                    PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
                    PdfDocument pdfDocument = new PdfDocument(writer);
                    Document document = new Document(pdfDocument);

                    // Create a table with columns for activity details
                    float[] columnWidths = {200f, 200f, 200f, 300f};
                    Table table = new Table(columnWidths);

                    // Add table headers
                    table.addCell(new Cell().add(new Paragraph("Activity Name")));
                    table.addCell(new Cell().add(new Paragraph("Time (Start - End)")));
                    table.addCell(new Cell().add(new Paragraph("Location")));
                    table.addCell(new Cell().add(new Paragraph("Description")));

                    // Add activity details to the table
                    for (Activity activity : happening.getActivities()) {
                        table.addCell(new Cell().add(new Paragraph(activity.getName())));
                        table.addCell(new Cell().add(new Paragraph(activity.getStartTime() + " - " + activity.getEndTime())));
                        table.addCell(new Cell().add(new Paragraph(activity.getLocation())));
                        table.addCell(new Cell().add(new Paragraph(activity.getDescription())));
                    }

                    // Add the table to the document
                    document.add(table);

                    document.close();
                    writer.close();

                    Toast.makeText(context, "PDF created successfully: " + pdfFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
                    Log.d("PdfGenerator", "PDF generated and saved at: " + pdfFile.getAbsolutePath());

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Failed to create PDF", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Failed to fetch happening data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private File createActivitiesPdfFile(String happeningName) throws IOException {
        File pdfDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "EventPlanner");
        if (!pdfDir.exists()) {
            pdfDir.mkdirs();
        }
        File pdfFile = new File(pdfDir, "Activities_" + happeningName + ".pdf");
        if (!pdfFile.exists()) {
            pdfFile.createNewFile();
        }
        return pdfFile;
    }



    public void generateCompaniesProductPdf(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        findUserByUserEmail(currentUser.getEmail(), new OnUserLoadedListener() {
            @Override
            public void onUserLoaded(User company) {
                user = company;
                generateProductPdf();

            }
        });
    }

    public void generateCompaniesServicePdf(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        findUserByUserEmail(currentUser.getEmail(), new OnUserLoadedListener() {
            @Override
            public void onUserLoaded(User company) {
                user = company;
                generateServicePdf();

            }
        });
    }

    public void generateCompaniesPackagePdf(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        findUserByUserEmail(currentUser.getEmail(), new OnUserLoadedListener() {
            @Override
            public void onUserLoaded(User company) {
                user = company;
                generatePackagePdf();

            }
        });
    }

    public void generateCompaniesConsolidatedPdf(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        findUserByUserEmail(currentUser.getEmail(), new OnUserLoadedListener() {
            @Override
            public void onUserLoaded(User company) {
                user = company;
                generateConsolidatedPdf();

            }
        });
    }


    public void generateProductPdf() {
        if(user.getUserType()== User.UserType.PUP) {

            db.collection("products").whereEqualTo("userEmail",user.getEmail()).whereEqualTo("visible",true).get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    try {
                        File pdfFile = createProductPdfFile();
                        PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
                        PdfDocument pdfDocument = new PdfDocument(writer);
                        Document document = new Document(pdfDocument);

                        // Create a table with 3 columns
                        float[] columnWidths = {200f, 100f, 70f, 230f};
                        Table table = new Table(columnWidths);

                        // Add table headers
                        table.addCell(new Cell().add(new Paragraph("Name")));
                        table.addCell(new Cell().add(new Paragraph("Price")));
                        table.addCell(new Cell().add(new Paragraph("Discount")));
                        table.addCell(new Cell().add(new Paragraph("Price with discount")));


                        // Add product details to the table
                        for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                            String productName = documentSnapshot.getString("name");
                            Double productPrice = documentSnapshot.getDouble("price");
                            Double productDiscount = documentSnapshot.getDouble("discount");
                            Double productPriceWithDiscount = documentSnapshot.getDouble("priceWithDiscount");

                            if (productName != null && productDiscount != null && productPrice != null && productPriceWithDiscount != null) {
                                table.addCell(new Cell().add(new Paragraph(productName)));
                                table.addCell(new Cell().add(new Paragraph(productPrice + " dinara")));
                                table.addCell(new Cell().add(new Paragraph(productDiscount + "%")));
                                table.addCell(new Cell().add(new Paragraph(productPriceWithDiscount + " dinara")));

                            }
                        }

                        // Add the table to the document
                        document.add(table);

                        document.close();
                        writer.close();

                        Toast.makeText(context, "PDF created successfully: " + pdfFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
                        Log.d("PdfGenerator", "PDF generated and saved at: " + pdfFile.getAbsolutePath());

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed to create PDF", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Failed to fetch products", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private File createProductPdfFile() throws IOException {
        // Use public external storage directory
        File pdfDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "EventPlanner");
        if (!pdfDir.exists()) {
            pdfDir.mkdirs();
        }
        File pdfFile = new File(pdfDir, "products.pdf");
        if (!pdfFile.exists()) {
            pdfFile.createNewFile();
        }
        return pdfFile;
    }


    public void generateServicePdf() {
        if(user.getUserType()== User.UserType.PUP) {
            db.collection("services").whereEqualTo("userEmail",user.getEmail()).whereEqualTo("visible",true).get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    try {
                        File pdfFile = createServicesPdfFile();
                        PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
                        PdfDocument pdfDocument = new PdfDocument(writer);
                        Document document = new Document(pdfDocument);

                        // Create a table with 3 columns
                        float[] columnWidths = {200f, 100f, 70f, 230f};
                        Table table = new Table(columnWidths);

                        // Add table headers
                        table.addCell(new Cell().add(new Paragraph("Name")));
                        table.addCell(new Cell().add(new Paragraph("Price")));
                        table.addCell(new Cell().add(new Paragraph("Discount")));
                        table.addCell(new Cell().add(new Paragraph("Price with discount")));


                        // Add product details to the table
                        for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                            String productName = documentSnapshot.getString("name");
                            Double productPrice = documentSnapshot.getDouble("pricePerHour") * documentSnapshot.getDouble("hours");
                            Double productDiscount = documentSnapshot.getDouble("discount");
                            Double productPriceWithDiscount = documentSnapshot.getDouble("priceTotal");

                            if (productName != null && productDiscount != null && productPrice != null && productPriceWithDiscount != null) {
                                table.addCell(new Cell().add(new Paragraph(productName)));
                                table.addCell(new Cell().add(new Paragraph(productPrice + " dinara")));
                                table.addCell(new Cell().add(new Paragraph(productDiscount + "%")));
                                table.addCell(new Cell().add(new Paragraph(productPriceWithDiscount + " dinara")));

                            }
                        }

                        // Add the table to the document
                        document.add(table);

                        document.close();
                        writer.close();

                        Toast.makeText(context, "PDF created successfully: " + pdfFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
                        Log.d("PdfGenerator", "PDF generated and saved at: " + pdfFile.getAbsolutePath());

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed to create PDF", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Failed to fetch products", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private File createServicesPdfFile() throws IOException {
        // Use public external storage directory
        File pdfDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "EventPlanner");
        if (!pdfDir.exists()) {
            pdfDir.mkdirs();
        }
        File pdfFile = new File(pdfDir, "services.pdf");
        if (!pdfFile.exists()) {
            pdfFile.createNewFile();
        }
        return pdfFile;
    }

    public void generatePackagePdf() {
        if(user.getUserType()== User.UserType.PUP) {
            db.collection("packages").whereEqualTo("userEmail", user.getEmail()).whereEqualTo("visible",true).get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    try {
                        File pdfFile = createPackagesPdfFile();
                        PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
                        PdfDocument pdfDocument = new PdfDocument(writer);
                        Document document = new Document(pdfDocument);

                        // Create a table with 3 columns
                        float[] columnWidths = {200f, 100f, 70f, 230f};
                        Table table = new Table(columnWidths);

                        // Add table headers
                        table.addCell(new Cell().add(new Paragraph("Name")));
                        table.addCell(new Cell().add(new Paragraph("Price")));
                        table.addCell(new Cell().add(new Paragraph("Discount")));
                        table.addCell(new Cell().add(new Paragraph("Price with discount")));


                        // Add product details to the table
                        for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                            String productName = documentSnapshot.getString("name");
                            Double productDiscount = documentSnapshot.getDouble("discount");
                            Double productPrice = documentSnapshot.getDouble("price") * (100 / (100 - productDiscount));
                            Double productPriceWithDiscount = documentSnapshot.getDouble("price");

                            if (productName != null && productDiscount != null && productPrice != null && productPriceWithDiscount != null) {
                                table.addCell(new Cell().add(new Paragraph(productName)));
                                table.addCell(new Cell().add(new Paragraph(productPrice + " dinara")));
                                table.addCell(new Cell().add(new Paragraph(productDiscount + "%")));
                                table.addCell(new Cell().add(new Paragraph(productPriceWithDiscount + " dinara")));

                            }
                        }

                        // Add the table to the document
                        document.add(table);

                        document.close();
                        writer.close();

                        Toast.makeText(context, "PDF created successfully: " + pdfFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
                        Log.d("PdfGenerator", "PDF generated and saved at: " + pdfFile.getAbsolutePath());

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(context, "Failed to create PDF", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Failed to fetch products", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private File createPackagesPdfFile() throws IOException {
        // Use public external storage directory
        File pdfDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "EventPlanner");
        if (!pdfDir.exists()) {
            pdfDir.mkdirs();
        }
        File pdfFile = new File(pdfDir, "packages.pdf");
        if (!pdfFile.exists()) {
            pdfFile.createNewFile();
        }
        return pdfFile;
    }

    public void generateConsolidatedPdf() {
        if (user.getUserType() == User.UserType.PUP) {
            try {

                File pdfFile = createPdfFile();
                PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
                PdfDocument pdfDocument = new PdfDocument(writer);
                Document document = new Document(pdfDocument);

                List<Task<QuerySnapshot>> tasks = new ArrayList<>();
                tasks.add(addProductsSection(document));
                tasks.add(addServicesSection(document));
                tasks.add(addPackagesSection(document));

                Tasks.whenAllComplete(tasks).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        document.close();
                        try {
                            writer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(context, "PDF created successfully: " + pdfFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
                        Log.d("PdfGenerator", "PDF generated and saved at: " + pdfFile.getAbsolutePath());
                    } else {
                        Toast.makeText(context, "Failed to create PDF", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(context, "Failed to create PDF", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private File createPdfFile() throws IOException {
        File pdfDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "EventPlanner");
        if (!pdfDir.exists()) {
            pdfDir.mkdirs();
        }
        File pdfFile = new File(pdfDir, "consolidated.pdf");
        if (!pdfFile.exists()) {
            pdfFile.createNewFile();
        }
        return pdfFile;
    }

    private Task<QuerySnapshot> addProductsSection(Document document) {
        return db.collection("products").whereEqualTo("userEmail", user.getEmail()).whereEqualTo("visible",true).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                document.add(new Paragraph("Products").setBold().setFontSize(18));
                float[] columnWidths = {200f, 100f, 70f, 230f};
                Table table = new Table(columnWidths);

                table.addCell(new Cell().add(new Paragraph("Name")));
                table.addCell(new Cell().add(new Paragraph("Price")));
                table.addCell(new Cell().add(new Paragraph("Discount")));
                table.addCell(new Cell().add(new Paragraph("Price with discount")));

                for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                    String productName = documentSnapshot.getString("name");
                    Double productPrice = documentSnapshot.getDouble("price");
                    Double productDiscount = documentSnapshot.getDouble("discount");
                    Double productPriceWithDiscount = documentSnapshot.getDouble("priceWithDiscount");

                    if (productName != null && productDiscount != null && productPrice != null && productPriceWithDiscount != null) {
                        table.addCell(new Cell().add(new Paragraph(productName)));
                        table.addCell(new Cell().add(new Paragraph(productPrice + " dinara")));
                        table.addCell(new Cell().add(new Paragraph(productDiscount + "%")));
                        table.addCell(new Cell().add(new Paragraph(productPriceWithDiscount + " dinara")));
                    }
                }

                document.add(table);
                document.add(new Paragraph("\n"));
            } else {
                Toast.makeText(context, "Failed to fetch products", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Task<QuerySnapshot> addServicesSection(Document document) {
        return db.collection("services").whereEqualTo("userEmail", user.getEmail()).whereEqualTo("visible",true).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                document.add(new Paragraph("Services").setBold().setFontSize(18));
                float[] columnWidths = {200f, 100f, 70f, 230f};
                Table table = new Table(columnWidths);

                table.addCell(new Cell().add(new Paragraph("Name")));
                table.addCell(new Cell().add(new Paragraph("Price")));
                table.addCell(new Cell().add(new Paragraph("Discount")));
                table.addCell(new Cell().add(new Paragraph("Price with discount")));

                for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                    String serviceName = documentSnapshot.getString("name");
                    Double servicePrice = documentSnapshot.getDouble("pricePerHour") * documentSnapshot.getDouble("hours");
                    Double serviceDiscount = documentSnapshot.getDouble("discount");
                    Double servicePriceWithDiscount = documentSnapshot.getDouble("priceTotal");

                    if (serviceName != null && serviceDiscount != null && servicePrice != null && servicePriceWithDiscount != null) {
                        table.addCell(new Cell().add(new Paragraph(serviceName)));
                        table.addCell(new Cell().add(new Paragraph(servicePrice + " dinara")));
                        table.addCell(new Cell().add(new Paragraph(serviceDiscount + "%")));
                        table.addCell(new Cell().add(new Paragraph(servicePriceWithDiscount + " dinara")));
                    }
                }

                document.add(table);
                document.add(new Paragraph("\n"));
            } else {
                Toast.makeText(context, "Failed to fetch services", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Task<QuerySnapshot> addPackagesSection(Document document) {
        return db.collection("packages").whereEqualTo("userEmail", user.getEmail()).whereEqualTo("visible",true).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                document.add(new Paragraph("Packages").setBold().setFontSize(18));
                float[] columnWidths = {200f, 100f, 70f, 230f};
                Table table = new Table(columnWidths);

                table.addCell(new Cell().add(new Paragraph("Name")));
                table.addCell(new Cell().add(new Paragraph("Price")));
                table.addCell(new Cell().add(new Paragraph("Discount")));
                table.addCell(new Cell().add(new Paragraph("Price with discount")));

                for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {
                    String packageName = documentSnapshot.getString("name");
                    Double packageDiscount = documentSnapshot.getDouble("discount");
                    Double packagePrice = documentSnapshot.getDouble("price") * (100 / (100 - packageDiscount));
                    Double packagePriceWithDiscount = documentSnapshot.getDouble("price");

                    if (packageName != null && packageDiscount != null && packagePrice != null && packagePriceWithDiscount != null) {
                        table.addCell(new Cell().add(new Paragraph(packageName)));
                        table.addCell(new Cell().add(new Paragraph(packagePrice + " dinara")));
                        table.addCell(new Cell().add(new Paragraph(packageDiscount + "%")));
                        table.addCell(new Cell().add(new Paragraph(packagePriceWithDiscount + " dinara")));
                    }
                }

                document.add(table);
            } else {
                Toast.makeText(context, "Failed to fetch packages", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void findUserByUserEmail(String email, OnUserLoadedListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                user= document.toObject(User.class);
                                listener.onUserLoaded(user);
                                return;
                            }
                        }
                    }
                });
    }
    interface OnUserLoadedListener {
        void onUserLoaded(User user);
    }
}
