package com.example.eventplanner.model.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.eventplanner.model.Product;


import java.util.Date;


public class ProductReport implements Parcelable {

    private Long id;

    private Product product;
    private Date time;
    private String reason;
    private Status status;

    public ProductReport(){

    }

    public ProductReport(Long id, Product product, Date time, String reason, Status status) {
        this.id = id;
        this.product = product;
        this.time = time;
        this.reason = reason;
        this.status = status;
    }

    protected ProductReport(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        product = in.readParcelable(Product.class.getClassLoader());
        time = new Date(in.readLong());
        reason = in.readString();
        status = Status.valueOf(in.readString());


    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeParcelable(product, flags);
        dest.writeLong(time.getTime());
        dest.writeString(reason);
        dest.writeString(status.name());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductReport> CREATOR = new Creator<ProductReport>() {
        @Override
        public ProductReport createFromParcel(Parcel in) {
            return new ProductReport(in);
        }

        @Override
        public ProductReport[] newArray(int size) {
            return new ProductReport[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }




    public enum Status {
        REPORTED,
        ACCEPTED,
        REJECTED
    }



}
