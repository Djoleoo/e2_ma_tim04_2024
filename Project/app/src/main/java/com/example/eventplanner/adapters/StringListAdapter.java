package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eventplanner.R;
import java.util.ArrayList;

public class StringListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final ArrayList<String> subcategoryNames;
    private boolean isInEditMode = false;

    public StringListAdapter(Context context, ArrayList<String> subcategoryNames) {
        super(context, R.layout.eventype_subcategories_list_view_item, subcategoryNames);
        this.context = context;
        this.subcategoryNames = subcategoryNames;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.eventype_subcategories_list_view_item, parent, false);

        TextView subcategoryNameTextView = rowView.findViewById(R.id.subcategory_name);
        subcategoryNameTextView.setText(subcategoryNames.get(position));

        ImageView deleteIconImageView = rowView.findViewById(R.id.delete_icon);
        if (isInEditMode) {
            deleteIconImageView.setVisibility(View.VISIBLE);
        } else {
            deleteIconImageView.setVisibility(View.GONE);
        }
        deleteIconImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


            }
        });

        return rowView;
    }
}
