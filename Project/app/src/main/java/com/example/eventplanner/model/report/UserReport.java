package com.example.eventplanner.model.report;

import android.os.Parcel;
import android.os.Parcelable;


import com.example.eventplanner.model.User;

import java.util.Date;

public class UserReport implements Parcelable {
    private Long id;

    private String userReport;
    private User userReported;

    private Date time;
    private String reason;
    private Status status;

    public UserReport(){

    }

    public UserReport(Long id, String userReport,User userReported, Date time, String reason, Status status) {
        this.id = id;
        this.userReport = userReport;
        this.userReported=userReported;
        this.time = time;
        this.reason = reason;
        this.status = status;
    }

    public UserReport( String userReport,User userReported, Date time, String reason, Status status) {

        this.userReport = userReport;
        this.userReported=userReported;
        this.time = time;
        this.reason = reason;
        this.status = status;
    }

    protected UserReport(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        userReport = in.readString();
        userReported = in.readParcelable(User.class.getClassLoader());
        time = new Date(in.readLong());
        reason = in.readString();
        status = Status.valueOf(in.readString());


    }

    public static final Creator<UserReport> CREATOR = new Creator<UserReport>() {
        @Override
        public UserReport createFromParcel(Parcel in) {
            return new UserReport(in);
        }

        @Override
        public UserReport[] newArray(int size) {
            return new UserReport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(userReport);
        dest.writeParcelable(userReported, flags);
        dest.writeLong(time.getTime());
        dest.writeString(reason);
        dest.writeString(status.name());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserReport() {
        return userReport;
    }

    public void setUserReport(String user) {
        this.userReport = user;
    }

    public User getUserReported() {
        return userReported;
    }

    public void setUserReported(User user) {
        this.userReported = user;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Status {
        REPORTED,
        ACCEPTED,
        REJECTED
    }

}
