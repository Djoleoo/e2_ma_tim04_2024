package com.example.eventplanner.fragments.categories;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CategoryListAdapter;
import com.example.eventplanner.adapters.SubCategoryListAdapter;
import com.example.eventplanner.databinding.FragmentCategoriesBinding;
import com.example.eventplanner.databinding.FragmentSubCategoriesBinding;
import com.example.eventplanner.model.SubCategory;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;


public class SubCategoriesFragment extends ListFragment {


    private SubCategoryListAdapter adapter;
    private ArrayList<DocumentSnapshot> mSubCategories;
    private FragmentSubCategoriesBinding binding;

    private static final String ARG_PARAM = "param";


    public SubCategoriesFragment() {
        // Required empty public constructor
    }

    public static SubCategoriesFragment newInstance(ArrayList<DocumentSnapshot> subCategories) {
        SubCategoriesFragment fragment = new SubCategoriesFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM,subCategories);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSubCategories = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new SubCategoryListAdapter(getActivity(), mSubCategories);
            setListAdapter(adapter);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSubCategoriesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        return root;
    }
}