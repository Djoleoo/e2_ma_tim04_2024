package com.example.eventplanner.fragments.happenings;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ActivityAdapter;
import com.example.eventplanner.model.Activity;
import com.example.eventplanner.model.Happening;
import com.example.eventplanner.pdf.PdfGenerator;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ActivitiesFragment extends Fragment {

    private static final String ARG_HAPPENING = "happening";
    private Happening mHappening;
    private FirebaseFirestore db;
    private ActivityAdapter activityAdapter;

    public ActivitiesFragment() {
        // Required empty public constructor
    }

    public static ActivitiesFragment newInstance(Happening happening) {
        ActivitiesFragment fragment = new ActivitiesFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_HAPPENING, happening);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mHappening = getArguments().getParcelable(ARG_HAPPENING);
        }
        db = FirebaseFirestore.getInstance();  // Initialize Firestore
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activities, container, false);

        // Example: Show the Happening name in a TextView
        TextView happeningNameTextView = rootView.findViewById(R.id.happening_name_text_view);
        happeningNameTextView.setText(mHappening.getName());

        // Set up RecyclerView
        RecyclerView activitiesRecyclerView = rootView.findViewById(R.id.activities_recycler_view);
        activitiesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        activityAdapter = new ActivityAdapter(mHappening.getActivities(), mHappening, getContext());
        activitiesRecyclerView.setAdapter(activityAdapter);

        Button generatePdfButton = rootView.findViewById(R.id.generate_pdf_button);
        generatePdfButton.setOnClickListener(v -> generateActivitiesPdf());

        // Floating Action Button to add an activity
        FloatingActionButton fab = rootView.findViewById(R.id.floating_action_button_activities);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddActivityDialog();
            }
        });

        return rootView;
    }

    private void showAddActivityDialog() {
        // Create a dialog for adding an activity
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_activity, null);
        builder.setView(dialogView);

        EditText activityNameEditText = dialogView.findViewById(R.id.activity_name_edit_text);
        EditText activityDescriptionEditText = dialogView.findViewById(R.id.activity_description_edit_text);
        EditText startTimeEditText = dialogView.findViewById(R.id.start_time_edit_text);
        EditText endTimeEditText = dialogView.findViewById(R.id.end_time_edit_text);
        EditText locationEditText = dialogView.findViewById(R.id.location_edit_text);

        // Set up time picker for start time
        startTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(startTimeEditText);
            }
        });

        // Set up time picker for end time
        endTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(endTimeEditText);
            }
        });

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = activityNameEditText.getText().toString();
                String description = activityDescriptionEditText.getText().toString();
                String startTime = startTimeEditText.getText().toString();
                String endTime = endTimeEditText.getText().toString();
                String location = locationEditText.getText().toString();

                // Validate the time inputs
                if (!isEndTimeValid(startTime, endTime)) {
                    Toast.makeText(getContext(), "End time cannot be before start time.", Toast.LENGTH_SHORT).show();
                    return;  // Prevent the activity from being added
                }

                Activity newActivity = new Activity(name, description, startTime, endTime, location);
                mHappening.getActivities().add(newActivity);



                activityAdapter.notifyItemInserted(mHappening.getActivities().size() - 1);
                updateHappeningInFirestore();
                Toast.makeText(getContext(), "Activity added", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void generateActivitiesPdf() {
        PdfGenerator pdfGenerator = new PdfGenerator(getContext());
        pdfGenerator.generateActivitiesPdf(mHappening);  // Generate the PDF for the activity list
    }

    private void showTimePickerDialog(EditText timeEditText) {
        // Get current time
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create TimePickerDialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(
                getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        // Format time and set to EditText
                        String time = String.format("%02d:%02d", hourOfDay, minute);
                        timeEditText.setText(time);
                    }
                },
                hour, minute, DateFormat.is24HourFormat(getContext()));

        timePickerDialog.show();
    }

    private void updateHappeningInFirestore() {
        db.collection("happenings").document(mHappening.getId())
                .set(mHappening)
                .addOnSuccessListener(aVoid -> {
                    // Successfully updated the Happening in Firestore
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(getContext(), "Failed to update Happening.", Toast.LENGTH_SHORT).show();
                });
    }

    private boolean isEndTimeValid(String startTime, String endTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date startDate = sdf.parse(startTime);
            Date endDate = sdf.parse(endTime);

            return endDate.after(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
}
