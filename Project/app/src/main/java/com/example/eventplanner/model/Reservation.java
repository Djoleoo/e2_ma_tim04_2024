package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Reservation implements Parcelable {
    private Long id;
    private Service service;
    private Event event;
    private User employee;  // Changed from Employee to User
    private User organizer; // The OD who made the reservation
    private String reservationDate;  // Format: "YYYY-MM-DD HH:MM"
    private String status; // e.g., "novo", "otkazano", "prihvaćeno", "realizovano"

    public Reservation() {
    }

    public Reservation(Long id, Service service, Event event, User employee, User organizer, String reservationDate, String status) {
        this.id = id;
        this.service = service;
        this.event = event;
        this.employee = employee;
        this.organizer = organizer;
        this.reservationDate = reservationDate;
        this.status = status;
    }

    protected Reservation(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        service = in.readParcelable(Service.class.getClassLoader());
        event = in.readParcelable(Event.class.getClassLoader());
        employee = in.readParcelable(User.class.getClassLoader());  // Changed from Employee to User
        organizer = in.readParcelable(User.class.getClassLoader());
        reservationDate = in.readString();
        status = in.readString();
    }

    public static final Creator<Reservation> CREATOR = new Creator<Reservation>() {
        @Override
        public Reservation createFromParcel(Parcel in) {
            return new Reservation(in);
        }

        @Override
        public Reservation[] newArray(int size) {
            return new Reservation[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeParcelable(service, flags);
        dest.writeParcelable(event, flags);
        dest.writeParcelable(employee, flags);  // Changed from Employee to User
        dest.writeParcelable(organizer, flags);
        dest.writeString(reservationDate);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getEmployee() {  // Changed from Employee to User
        return employee;
    }

    public void setEmployee(User employee) {  // Changed from Employee to User
        this.employee = employee;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
