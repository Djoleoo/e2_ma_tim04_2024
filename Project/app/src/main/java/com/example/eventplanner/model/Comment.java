package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Comment implements Parcelable {

    private String id;  // Firestore document ID
    private String companyID;
    private String userID;
    private Date date;  // Date the comment was created
    private String rating;
    private String text;
    private boolean ableToLeaveReview;
    private boolean reported;  // Indicates if the comment has been reported
    private String reportReason;  // Reason for reporting the comment
    private String reportStatus;  // Status of the report (e.g., reported, accepted, rejected)
    private String reportedBy;  // New field to store who reported the comment

    // Default constructor required for Firestore
    public Comment() {}

    // Constructor to initialize all fields
    public Comment(String id, String companyID, String userID, Date date, String rating, String text,
                   boolean ableToLeaveReview, boolean reported, String reportReason, String reportStatus, String reportedBy) {
        this.id = id;
        this.companyID = companyID;
        this.userID = userID;
        this.date = date;
        this.rating = rating;
        this.text = text;
        this.ableToLeaveReview = ableToLeaveReview;
        this.reported = reported;
        this.reportReason = reportReason;
        this.reportStatus = reportStatus;
        this.reportedBy = reportedBy;
    }

    protected Comment(Parcel in) {
        id = in.readString();  // Read the ID
        companyID = in.readString();
        userID = in.readString();
        date = new Date(in.readLong());
        rating = in.readString();
        text = in.readString();
        ableToLeaveReview = in.readByte() != 0;
        reported = in.readByte() != 0;
        reportReason = in.readString();
        reportStatus = in.readString();
        reportedBy = in.readString();  // Read the reportedBy field
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);  // Write the ID
        dest.writeString(companyID);
        dest.writeString(userID);
        dest.writeLong(date.getTime());
        dest.writeString(rating);
        dest.writeString(text);
        dest.writeByte((byte) (ableToLeaveReview ? 1 : 0));
        dest.writeByte((byte) (reported ? 1 : 0));
        dest.writeString(reportReason);
        dest.writeString(reportStatus);
        dest.writeString(reportedBy);  // Write the reportedBy field
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    // Getter and Setter methods
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isAbleToLeaveReview() {
        return ableToLeaveReview;
    }

    public void setAbleToLeaveReview(boolean ableToLeaveReview) {
        this.ableToLeaveReview = ableToLeaveReview;
    }

    public boolean isReported() {
        return reported;
    }

    public void setReported(boolean reported) {
        this.reported = reported;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public String getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(String reportStatus) {
        this.reportStatus = reportStatus;
    }

    public String getReportedBy() {
        return reportedBy;
    }

    public void setReportedBy(String reportedBy) {
        this.reportedBy = reportedBy;
    }
}
