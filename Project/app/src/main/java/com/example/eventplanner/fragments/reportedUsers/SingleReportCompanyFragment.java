package com.example.eventplanner.fragments.reportedUsers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleReportCompanyBinding;
import com.example.eventplanner.databinding.FragmentSingleUsersReportedBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.report.CompanyReport;
import com.example.eventplanner.model.report.UserReport;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SingleReportCompanyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SingleReportCompanyFragment extends Fragment {

    private FragmentSingleReportCompanyBinding binding;
    private CompanyReport mProduct;
    private String documentId;





    private static final String ARG_PRODUCT = "product";
    private static final String ARG_PRODUCT_ID = "productId";


    public SingleReportCompanyFragment() {
        // Required empty public constructor
    }


    public static SingleReportCompanyFragment newInstance(CompanyReport product,String documentId) {
        SingleReportCompanyFragment fragment = new SingleReportCompanyFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        args.putString(ARG_PRODUCT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_PRODUCT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentSingleReportCompanyBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        TextView userReportEmail=binding.userReportEdit;

        // Company Info
        TextView companyEmail = binding.companyEmail;
        TextView companyName = binding.companyName;
        TextView companyAddress = binding.companyAddress;
        TextView companyNumber = binding.companyNumber;
        TextView companyDescription = binding.companyDescription;


        // Work Hours
        TextView workHoursMonday = binding.workhoursMonday;
        TextView workHoursTuesday = binding.workhoursTuesday;
        TextView workHoursWednesday = binding.workhoursWednesday;
        TextView workHoursThursday = binding.workhoursThursday;
        TextView workHoursFriday = binding.workhoursFriday;
        TextView workHoursSaturday = binding.workhoursSaturday;
        TextView workHoursSunday = binding.workhoursSunday;

        // Categories and Event Types


        // User Reported
        EditText userReportedFirstName = binding.firstNameEdit;
        EditText userReportedLastName = binding.lastNameEdit;
        EditText userReportedEmail = binding.emailEdit;
        EditText userReportedPhone = binding.phoneNumberEdit;
        EditText userReportedAddress = binding.addressEdit;
        EditText timeReport = binding.timeEdit;
        EditText reasonReport = binding.reasonEdit;
        EditText status = binding.statusEdit;

        Button acceptBtn = binding.buttonAccept;
        Button declineBtn = binding.buttonDecline;

        if (mProduct != null) {

            userReportEmail.setText(mProduct.getUserReport());
            // Set company info
            companyEmail.setText(mProduct.getCompanyReported().getEmail());
            companyName.setText(mProduct.getCompanyReported().getName());
            companyAddress.setText(mProduct.getCompanyReported().getAddress());
            companyNumber.setText(mProduct.getCompanyReported().getNumber());
            companyDescription.setText(mProduct.getCompanyReported().getDescription());

            // Set work hours
            workHoursMonday.setText(mProduct.getCompanyReported().getWorkHours().getMonday());
            workHoursTuesday.setText(mProduct.getCompanyReported().getWorkHours().getTuesday());
            workHoursWednesday.setText(mProduct.getCompanyReported().getWorkHours().getWednesday());
            workHoursThursday.setText(mProduct.getCompanyReported().getWorkHours().getThursday());
            workHoursFriday.setText(mProduct.getCompanyReported().getWorkHours().getFriday());
            workHoursSaturday.setText(mProduct.getCompanyReported().getWorkHours().getSaturday());
            workHoursSunday.setText(mProduct.getCompanyReported().getWorkHours().getSunday());



            // Set reported user info
            userReportedFirstName.setText(mProduct.getUserReported().getFirstName());
            userReportedLastName.setText(mProduct.getUserReported().getLastName());
            userReportedEmail.setText(mProduct.getUserReported().getEmail());
            userReportedPhone.setText(mProduct.getUserReported().getPhoneNumber());
            userReportedAddress.setText(mProduct.getUserReported().getAddress());
            timeReport.setText(mProduct.getTime().toString());
            reasonReport.setText(mProduct.getReason());
            status.setText(mProduct.getStatus().toString());
        }

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CompanyReport product = new CompanyReport(mProduct.getCompanyReported(),mProduct.getUserReported(),mProduct.getUserReport(),mProduct.getTime(),mProduct.getReason(), CompanyReport.Status.ACCEPTED);

                showConfirmationDialog(product);
            }
        });

        declineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CompanyReport product = new CompanyReport(mProduct.getCompanyReported(),mProduct.getUserReported(),mProduct.getUserReport(),mProduct.getTime(),mProduct.getReason(), CompanyReport.Status.REJECTED);
                showDeclinationDialog(product);
            }
        });


        return root;
    }


    private void showConfirmationDialog(CompanyReport product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Accept report");
        builder.setMessage("Are you sure you want to accept this report?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("companyReports").document(documentId);
                docRef.set(product)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {


                                FragmentTransition.to(UsersReportedPageFragment.newInstance(),getActivity(),true,R.id.list_layout_reports);
                                Toast.makeText(getActivity(), "Accepted!", Toast.LENGTH_SHORT).show();
                                updateUserEnabledStatusByEmail(mProduct.getUserReported().getEmail());
                                updateProductVisibleByCompanyEmail(mProduct.getUserReported().getEmail());
                                updateServiceVisibleByCompanyEmail(mProduct.getUserReported().getEmail());
                                updatePackageVisibleByCompanyEmail(mProduct.getUserReported().getEmail());

                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDeclinationDialog(CompanyReport product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Decline report");
        builder.setMessage("Are you sure you want to decline this report?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("companyReports").document(documentId);
                docRef.set(product)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {

                                FragmentTransition.to(UsersReportedPageFragment.newInstance(),getActivity(),true,R.id.list_layout_reports);
                                Toast.makeText(getActivity(), "Declined!", Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateUserEnabledStatusByEmail(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .whereEqualTo("email", email)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                                // Assuming there may be more than one user with the same email
                                String userId = document.getId();
                                updateUserEnabledStatus(userId);
                            }
                        } else {
                            Toast.makeText(getActivity(), "No user found with the provided email.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to find user by email.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateUserEnabledStatus(String userId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference userRef = db.collection("users").document(userId);
        userRef.update("enabled", true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getActivity(), "User status updated successfully!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to update user status.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateProductVisibleByCompanyEmail(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products")
                .whereEqualTo("userEmail", email)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                                // Assuming there may be more than one user with the same email
                                String userId = document.getId();
                                updateProductVisibleStatus(userId);
                            }
                        } else {
                            Toast.makeText(getActivity(), "No user found with the provided email.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to find user by email.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateProductVisibleStatus(String userId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference userRef = db.collection("products").document(userId);
        userRef.update("visible", false)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Toast.makeText(getActivity(), "Pr updated successfully!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Toast.makeText(getActivity(), "Failed to update user status.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateServiceVisibleByCompanyEmail(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("services")
                .whereEqualTo("userEmail", email)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                                // Assuming there may be more than one user with the same email
                                String userId = document.getId();
                                updateServiceVisibleStatus(userId);
                            }
                        } else {
                            Toast.makeText(getActivity(), "No user found with the provided email.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to find user by email.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateServiceVisibleStatus(String userId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference userRef = db.collection("services").document(userId);
        userRef.update("visible", false)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Toast.makeText(getActivity(), "Pr updated successfully!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Toast.makeText(getActivity(), "Failed to update user status.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updatePackageVisibleByCompanyEmail(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("packages")
                .whereEqualTo("userEmail", email)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                                // Assuming there may be more than one user with the same email
                                String userId = document.getId();
                                updatePackageVisibleStatus(userId);
                            }
                        } else {
                            Toast.makeText(getActivity(), "No user found with the provided email.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to find user by email.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updatePackageVisibleStatus(String userId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference userRef = db.collection("packages").document(userId);
        userRef.update("visible", false)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Toast.makeText(getActivity(), "Pr updated successfully!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Toast.makeText(getActivity(), "Failed to update user status.", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}