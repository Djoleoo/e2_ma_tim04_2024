package com.example.eventplanner.fragments.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentNotificaitonsPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class NotificaitonsPageFragment extends Fragment implements ShakeDetector.OnShakeListener {

    public static ArrayList<DocumentSnapshot> mNotificationsDocuments = new ArrayList<>();
    private User user;
    private FragmentNotificaitonsPageBinding binding;
    private Spinner spinnerFilter;
    private ShakeDetector shakeDetector;
    private int currentFilter = 0; // 0 = Unread, 1 = Read, 2 = All

    public NotificaitonsPageFragment() {
        // Required empty public constructor
    }

    public static NotificaitonsPageFragment newInstance() {
        return new NotificaitonsPageFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initialize ShakeDetector
        shakeDetector = new ShakeDetector(getContext(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();
        binding = FragmentNotificaitonsPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        // Set up Spinner
        spinnerFilter = binding.spinnerFilter;
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(
                currentActivity,
                R.array.notification_filter_array,
                android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFilter.setAdapter(spinnerAdapter);

        spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String filter = (String) parent.getItemAtPosition(position);
                loadNotifications(currentActivity, currentUser.getEmail(), filter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // No action needed
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register the shake detector
        shakeDetector.register();
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister the shake detector
        shakeDetector.unregister();
    }

    @Override
    public void onShake() {
        currentFilter = (currentFilter + 1) % 3;
        switch (currentFilter) {
            case 0:
                loadNotifications(getActivity(), FirebaseAuth.getInstance().getCurrentUser().getEmail(), "Unread");
                Toast.makeText(getActivity(), "Showing Unread Notifications", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                loadNotifications(getActivity(), FirebaseAuth.getInstance().getCurrentUser().getEmail(), "Read");
                Toast.makeText(getActivity(), "Showing Read Notifications", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                loadNotifications(getActivity(), FirebaseAuth.getInstance().getCurrentUser().getEmail(), "All");
                Toast.makeText(getActivity(), "Showing All Notifications", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void loadNotifications(FragmentActivity currentActivity, String currentUserEmail, String filter) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Query query = db.collection("notifications").whereEqualTo("receiverId", currentUserEmail);

        if (filter != null) {
            switch (filter) {
                case "Read":
                    query = query.whereEqualTo("read", true);
                    break;
                case "Unread":
                    query = query.whereEqualTo("read", false);
                    break;
                case "All":
                default:
                    // No additional filtering
                    break;
            }
        }

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    mNotificationsDocuments.clear();
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        mNotificationsDocuments.add(document);
                    }
                    FragmentTransition.to(NotificationListFragment.newInstance(mNotificationsDocuments), currentActivity, true, R.id.list_layout_notifications);
                } else {
                    Toast.makeText(currentActivity, "Error loading notifications", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
