package com.example.eventplanner.fragments.reservations;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ReservationsAdapter;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Comment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Reservation;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WorkHours;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReservationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReservationsFragment extends Fragment {

    private RecyclerView recyclerView;
    private ReservationsAdapter adapter;
    private ArrayList<Reservation> reservations;
    private Button filterStatusButton;


    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = auth.getCurrentUser();

    private String userType;


    public void getUserType() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            String uid = currentUser.getUid();

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("users").document(uid);

            docRef.get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    userType = documentSnapshot.getString("userType");
                    // Now you have the userType
                    System.out.println("User Type: " + userType);
                } else {
                    System.out.println("No such document exists!");
                }
            }).addOnFailureListener(e -> {
                System.out.println("Error retrieving document: " + e.getMessage());
            });
        } else {
            System.out.println("No user is currently logged in.");
        }
    }

    public ReservationsFragment() {
        // Required empty public constructor
    }

    public static ReservationsFragment newInstance() {
        return new ReservationsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reservations, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_reservations);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        filterStatusButton = view.findViewById(R.id.btn_filter_status);
        filterStatusButton.bringToFront();

        filterStatusButton.setOnClickListener(v -> {
            Log.d("ReservationsFragment", "Filter button clicked!");
            showStatusFilterMenu(v);
        });

        adapter = new ReservationsAdapter(generateFakeReservations());
        adapter.setOnCancelButtonClickListener(this::cancelReservation);

        recyclerView.setAdapter(adapter);

        // Load user type and display reservations accordingly
        checkLoggedInUser();

        return view;
    }


    private void checkLoggedInUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            String uid = currentUser.getUid();
            getUserType(uid);
        } else {
            System.out.println("No user is currently logged in.");
        }
    }

    private void getUserType(String uid) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("users").document(uid);

        docRef.get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                userType = documentSnapshot.getString("userType");
                String userEmail = documentSnapshot.getString("email"); // Assuming email is stored in Firestore
                displayReservationsBasedOnRole(userType, userEmail);
            } else {
                System.out.println("No such document exists!");
            }
        }).addOnFailureListener(e -> {
            System.out.println("Error retrieving document: " + e.getMessage());
        });
    }

    private void displayReservationsBasedOnRole(String userType, String userEmail) {
        reservations = generateFakeReservations();
        ArrayList<Reservation> reservationsToDisplay = new ArrayList<>();

        switch (userType) {
            case "PUPV":
                // PUPV: Show all reservations and enable filtration
                filterStatusButton.setVisibility(View.VISIBLE);
                reservationsToDisplay = reservations;
                break;

            case "PUPZ":
                // PUPZ: Show reservations only where the logged-in user is the employee
                filterStatusButton.setVisibility(View.GONE);
                for (Reservation reservation : reservations) {
                    if (reservation.getEmployee().getEmail().equals(userEmail) &&
                            (reservation.getStatus().equals("novo") || reservation.getStatus().equals("prihvaćeno")
                                    || reservation.getStatus().equals("otkazano od strane PUP-a") || reservation.getStatus().equals("realizovano"))) {
                        reservationsToDisplay.add(reservation);
                    }
                }
                break;

            case "OD":
                // OD: Show reservations only where the logged-in user is the organizer and enable filtration
                filterStatusButton.setVisibility(View.GONE);
                for (Reservation reservation : reservations) {
                    if (reservation.getOrganizer().getEmail().equals(userEmail) &&
                            (reservation.getStatus().equals("novo") || reservation.getStatus().equals("prihvaćeno")
                                    || reservation.getStatus().equals("otkazano od strane OD-a") || reservation.getStatus().equals("realizovano"))) {
                        reservationsToDisplay.add(reservation);
                    }
                }
                break;

            default:
                Log.d("ReservationsFragment", "Unknown user type: " + userType);
        }

        adapter.updateReservations(reservationsToDisplay);
    }


    private void showStatusFilterMenu(View v) {
        PopupMenu popup = new PopupMenu(getContext(), v);
        popup.getMenuInflater().inflate(R.menu.reservation_status_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(item -> {
            String selectedStatus = item.getTitle().toString();
            filterReservationsByStatus(selectedStatus);
            return true;
        });

        popup.show();
    }

    private void filterReservationsByStatus(String status) {
        ArrayList<Reservation> filteredReservations = new ArrayList<>();
        for (Reservation reservation : reservations) {
            if (reservation.getStatus().equals(status)) {
                filteredReservations.add(reservation);
            }
        }
        adapter.updateReservations(filteredReservations);
    }

    private void cancelReservation(Reservation reservation, View view) {
        if (reservation.getStatus().equals("novo") || reservation.getStatus().equals("prihvaćeno")) {
            if ("OD".equals(userType)) {
                reservation.setStatus("otkazano od strane OD-a");
                createNotificationForODCancellation(reservation);
            } else if ("PUPV".equals(userType) || "PUPZ".equals(userType)) {
                reservation.setStatus("otkazano od strane PUP-a");
                createNotificationForPUPCancellation(reservation); // This will handle creating the comment as well
            }
            sendCancellationNotification(reservation);
            refreshReservationsList();
        } else {
            Log.d("ReservationsFragment", "Cannot cancel this reservation as it's not in a cancelable state.");
        }
    }



    private void createNotificationForODCancellation(Reservation reservation) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Get the current user's name for the notification text
        String currentUserName = currentUser.getDisplayName();
        if (currentUserName == null || currentUserName.isEmpty()) {
            currentUserName = "OD";
        }

        // Create the notification object
        Notification notification = new Notification(
                "Cancellation",
                "OD " + currentUserName + " canceled the reservation.",
                reservation.getEmployee().getEmail(),  // Receiver's ID (the employee's email)
                false,  // Mark as unread
                Notification.UserType.OD  // User type
        );

        // Save the notification to the Firestore "notifications" collection
        db.collection("notifications")
                .add(notification)
                .addOnSuccessListener(documentReference -> {
                    Log.d("ReservationsFragment", "Notification created successfully.");
                })
                .addOnFailureListener(e -> {
                    Log.e("ReservationsFragment", "Error creating notification", e);
                });
    }

    private void createNotificationForPUPCancellation(Reservation reservation) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Get the current user's name for the notification text
        String currentUserName = currentUser.getDisplayName();
        if (currentUserName == null || currentUserName.isEmpty()) {
            currentUserName = "PUP";
        }

        // Create the notification object
        Notification notification = new Notification(
                "Cancellation",
                "PUP " + currentUserName + " canceled the reservation.",
                reservation.getOrganizer().getEmail(),  // Receiver's ID (the organizer's email)
                false,  // Mark as unread
                Notification.UserType.PUP  // User type
        );

        // Save the notification to the Firestore "notifications" collection
        db.collection("notifications")
                .add(notification)
                .addOnSuccessListener(documentReference -> {
                    Log.d("ReservationsFragment", "Notification created successfully.");
                    createNotificationForReview(reservation);
                })
                .addOnFailureListener(e -> {
                    Log.e("ReservationsFragment", "Error creating notification", e);
                });
    }

    private void createNotificationForReview(Reservation reservation) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Get the company associated with the reservation
        Company company = reservation.getService().getCompany();

        // Get the organizer (OD) associated with the reservation
        User organizer = reservation.getOrganizer();

        // Create the notification object
        Notification notification = new Notification(
                "Review Opportunity",
                "You can leave a review and rating for " + company.getName() + ".",
                organizer.getEmail(),  // Receiver's ID (email of the OD who made the reservation)
                false,  // Mark as unread
                Notification.UserType.OD  // User type
        );

        // Save the notification to the Firestore "notifications" collection
        db.collection("notifications")
                .add(notification)
                .addOnSuccessListener(documentReference -> {
                    Log.d("ReservationsFragment", "Notification created successfully.");
                    createCommentForCompany(reservation);
                })
                .addOnFailureListener(e -> {
                    Log.e("ReservationsFragment", "Error creating notification", e);
                });
    }


    private void createCommentForCompany(Reservation reservation) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Get the company associated with the reservation
        Company company = reservation.getService().getCompany();

        // Get the organizer (OD) associated with the reservation
        User organizer = reservation.getOrganizer();

        // Calculate the date for the review deadline (current date + 5 days)
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 5);
        Date reviewDeadline = calendar.getTime();

        // Create the comment object using the updated constructor
        Comment comment = new Comment(
                null,                             // ID will be set by Firestore, set null for now
                company.getEmail(),               // companyID (email of the company)
                organizer.getEmail(),             // userID (email of the OD)
                reviewDeadline,                   // date (5 days from now)
                "",                               // rating (to be filled later)
                "",                               // text (to be filled later)
                true,                             // ableToLeaveReview (set to true)
                false,                            // reported (initially set to false)
                "",                               // reportReason (empty initially)
                "",                               // reportStatus (empty initially)
                null                              // reportedBy (null because it hasn't been reported yet)
        );


        // Save the comment to the Firestore "comments" collection
        db.collection("comments")
                .add(comment)
                .addOnSuccessListener(documentReference -> {
                    // Once saved, we can update the ID if needed
                    comment.setId(documentReference.getId());
                    Log.d("ReservationsFragment", "Comment object created successfully.");
                })
                .addOnFailureListener(e -> {
                    Log.e("ReservationsFragment", "Error creating comment object", e);
                });
    }



    private void sendCancellationNotification(Reservation reservation) {
        // Logic to send a notification based on who canceled
        Log.d("ReservationsFragment", "Reservation canceled by: " + userType);
    }

    private void refreshReservationsList() {
        adapter.updateReservations(reservations);
        adapter.notifyDataSetChanged();
    }

    public static ArrayList<Reservation> generateFakeReservations() {
        ArrayList<Reservation> reservations = new ArrayList<>();

        Category photographyCategory = new Category("1", "Photography", "Services related to event photography.");

        SubCategory dronePhotography = new SubCategory("1", "Drone Photography", "Aerial photography using drones.", photographyCategory, SubCategory.Status.SERVICE);

        WorkHours companyWorkHours = new WorkHours("08:00-16:00", "08:00-16:00", "08:00-16:00", "08:00-16:00", "08:00-16:00", "Closed", "Closed");

        User companyOwner = new User("John", "Doe", "companyowner@example.com", "password123", "1234567890", "123 Main St", User.UserType.PUP, "profilePicUrl", null);

        ArrayList<String> companyCategoryIds = new ArrayList<>(Arrays.asList("1"));
        ArrayList<String> companyEventTypeIds = new ArrayList<>(Arrays.asList("1", "2"));

        Company company = new Company(
                "info@company.com",
                "Best Photography Co.",
                "123 Main St, New York",
                "123-456-7890",
                "We provide the best photography services for your events.",
                companyWorkHours,
                companyCategoryIds,
                companyEventTypeIds,
                companyOwner
        );

        ArrayList<SubCategory> suggestedSubCategories = new ArrayList<>(Arrays.asList(dronePhotography));
        Event wedding = new Event("1", "Wedding", "Wedding event", suggestedSubCategories, false);
        Event birthday = new Event("2", "Birthday party", "Birthday party", suggestedSubCategories, false);

        ArrayList<Integer> gallery = new ArrayList<>(Arrays.asList(1, 2));
        ArrayList<String> persons = new ArrayList<>(Arrays.asList("John Doe", "Jane Doe"));
        ArrayList<Event> eventTypes = new ArrayList<>(Arrays.asList(wedding, birthday));

        Service dronePhotographyService = new Service(
                "idd",
                photographyCategory,
                dronePhotography,
                "Drone Photography",
                "Capture your event from above with our professional drone services.",
                gallery,
                "Specializing in aerial shots.",
                300.0, // price per hour
                2.0, // hours
                "New York",
                0.1, // 10% discount
                persons,
                eventTypes,
                30, // 30 days before event
                7, // 7 days for cancellation
                true, // Automatic acceptance
                true, // Is available
                true, // Is visible
                "john.doe@example.com",
                company
        );

        // Create users to act as employees
        User johnDoe = new User("John1", "Doe", "erdeljan.ra223.2020@uns.ac.rs", "password123", "1234567890", "123 Main St", User.UserType.PUPZ, "profilePicUrl", null);
        User janeDoe = new User("Jane2", "Doe", "erdeljan.ra223.2020@uns.ac.rs", "password456", "0987654321", "456 Elm St", User.UserType.PUPZ, "profilePicUrl", null);

        // Create organizers (ODs)
        User organizerAlice = new User("Alice3", "Smith", "antazr@gmail.com", "password789", "1234567890", "789 Maple St", User.UserType.OD, "profilePicUrl", null);
        User organizerBob = new User("Bob4", "Johnson", "antazr@gmail.com", "password101", "0987654321", "101 Oak St", User.UserType.OD, "profilePicUrl", null);

        // Create reservations
        reservations.add(new Reservation(1L, dronePhotographyService, wedding, johnDoe, organizerAlice, "2024-08-25 10:00", "novo"));
        reservations.add(new Reservation(2L, dronePhotographyService, birthday, janeDoe, organizerBob, "2024-08-26 12:00", "novo"));
        reservations.add(new Reservation(3L, dronePhotographyService, wedding, johnDoe, organizerAlice, "2024-08-27 14:00", "novo"));
        reservations.add(new Reservation(4L, dronePhotographyService, birthday, janeDoe, organizerBob, "2024-08-28 16:00", "novo"));

        return reservations;
    }
}
