package com.example.eventplanner.fragments.happenings;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.eventplanner.R;
//import com.example.eventplanner.activities.Categories.CategoriesActivity;
import com.example.eventplanner.databinding.FragmentHappeningsPageBinding;
import com.example.eventplanner.databinding.FragmentProductsPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Happening;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;


public class HappeningsPageFragment extends Fragment {

    public static ArrayList<Happening> happenings = new ArrayList<Happening>();
    private ArrayList<DocumentSnapshot> mHappeningDocuments = new ArrayList<>();
    private FragmentHappeningsPageBinding binding;
    private User user;
    //private static final String ARG_PARAM1 = "param1";
    //private static final String ARG_PARAM2 = "param2";
    //private String mParam1;
    //private String mParam2;

    public HappeningsPageFragment() {
        // Required empty public constructor
    }


    public static HappeningsPageFragment newInstance() {
        HappeningsPageFragment fragment = new HappeningsPageFragment();
        //Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //if (getArguments() != null) {
        //mParam1 = getArguments().getString(ARG_PARAM1);
        //mParam2 = getArguments().getString(ARG_PARAM2);
        //}


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();
        binding = FragmentHappeningsPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        if (currentUser != null) {
            String userId = currentUser.getUid();

            // Fetch the user document from Firestore
            db.collection("users").document(userId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            // Convert the document to a User object
                            user = document.toObject(User.class);
                            if (user != null) {
                                // Get the userType (role) of the user
                                User.UserType userType = user.getUserType();
                                Log.d("User Role", "User role: " + userType);

                                // Now that the user is available, set up the UI
                                setupUI(currentActivity);
                            }
                        } else {
                            Log.d("User Info", "No such document exists.");
                        }
                    } else {
                        Log.d("User Info", "get failed with ", task.getException());
                    }
                }
            });
        } else {
            Log.d("User Info", "No user is currently signed in.");
        }

        return root;
    }

    private void setupUI(FragmentActivity currentActivity) {
        FloatingActionButton addButton = binding.floatingActionButtonHappenings;

        // Log the user's email
        if (user != null) {
            Log.d("User Role", "User email: " + user.getEmail());

            // Example logic to show/hide button based on user role
            if (user.getUserType().equals(User.UserType.OD)) {
                addButton.setVisibility(View.VISIBLE);
            } else {
                addButton.setVisibility(View.GONE);
            }
        }

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addButton.setVisibility(View.GONE);
                FragmentTransition.to(AddHappeningFragment.newInstance(), currentActivity, true, R.id.list_layout_happenings);
            }
        });

        // Load happenings after the user is set up
        loadHappenings(currentActivity);
    }

    private void loadHappenings(FragmentActivity currentActivity) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("happenings").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mHappeningDocuments.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Happening happening = document.toObject(Happening.class);
                                mHappeningDocuments.add(document);

                            }

                            FragmentTransition.to(HappeningsFragment.newInstance(mHappeningDocuments), currentActivity, true, R.id.list_layout_happenings);
                        } else {
                            Toast.makeText(currentActivity, "Error getting happenings", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void prepareHappeningsList(ArrayList<Happening> happenings){
        // Create an instance of Event for the event type
//        Category category = new Category(
//                1L,
//                "name",
//                "desc"
//        );
//
//        SubCategory subCategory = new SubCategory(
//                "Subcategory Name",
//                "Subcategory Description",
//                category,
//                SubCategory.Status.SERVICE
//        );
//
//        SubCategory subCategory2 = new SubCategory(
//                "Subcategory Name 2",
//                "Subcategory Description 2",
//                category,
//                SubCategory.Status.SERVICE
//        );
//
//        // Create three Event instances and add the same SubCategory to each of them
//        Event event1 = new Event(
//                "Event 1",
//                "Event 1 Description",
//                new ArrayList<>(Arrays.asList(subCategory, subCategory2)),false
//        );
//
//
//        // Create Happening instances with different details
//        Happening happening1 = new Happening(
//                1L,
//                event1,
//                "Venčanje T i M",
//                "///",
//                200,
//                "Novi Sad",
//                50,
//                true
//        );
//
//        Happening happening2 = new Happening(
//                2L,
//                event1,
//                "Rođendan Marka",
//                "///",
//                50,
//                "Beograd",
//                20,
//                false
//        );
//
//        Happening happening3 = new Happening(
//                3L,
//                event1,
//                "Konferencija o razvoju softvera",
//                "///",
//                100,
//                "Online",
//                0,
//                false
//        );
//
//        // Add the happenings to the list
//        happenings.add(happening1);
//        happenings.add(happening2);
//        happenings.add(happening3);
    }


    public void onDestroyView() {
        happenings = new ArrayList<>();
        super.onDestroyView();
    }



}