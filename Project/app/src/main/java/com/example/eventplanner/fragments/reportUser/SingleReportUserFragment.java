package com.example.eventplanner.fragments.reportUser;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleReportUserBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.report.UserReport;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;


import java.util.Date;


public class SingleReportUserFragment extends Fragment {

    private FragmentSingleReportUserBinding binding;
    private User mProduct;
    private String documentId;





    private static final String ARG_PRODUCT = "product";
    private static final String ARG_PRODUCT_ID = "productId";


    public SingleReportUserFragment() {
        // Required empty public constructor
    }

    public static SingleReportUserFragment newInstance(User product, String documentId) {
        SingleReportUserFragment fragment = new SingleReportUserFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        args.putString(ARG_PRODUCT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_PRODUCT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSingleReportUserBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        TextView userEmail=binding.userEmail;
        TextView userName=binding.userName;
        TextView userAddress=binding.userAddress;
        TextView userPhoneNumber=binding.userNumber;
        Button reportUser=binding.buttonReportUser;
        Button submitReportUser=binding.buttonSubmitReport;
        TextView userReasonLabel=binding.userReportLabel;
        EditText userReasonText=binding.userReportEdit;

        if (mProduct != null) {
            userEmail.setText(mProduct.getEmail());
            userName.setText(mProduct.getFirstName()+" "+mProduct.getLastName());
            userAddress.setText(mProduct.getAddress());
            userPhoneNumber.setText(mProduct.getPhoneNumber());



        }

        reportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                reportUser.setVisibility(View.GONE);
                submitReportUser.setVisibility(View.VISIBLE);

                userReasonText.setVisibility(View.VISIBLE);
                userReasonLabel.setVisibility(View.VISIBLE);
                userReasonText.setEnabled(true);


            }
        });



        submitReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = userReasonText.getText().toString();
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

                UserReport userReport=new UserReport(currentUser.getEmail(),mProduct,new Date(),reason, UserReport.Status.REPORTED);

                showConfirmationDialog(userReport);
            }
        });









// Method to validate the input as a decimal number




        return root;
    }

    private void showConfirmationDialog(UserReport userReport) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Report user");
        builder.setMessage("Are you sure you want to report this user?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("userReports").add(userReport)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                FragmentTransition.to(ReportUserPageFragment.newInstance(),getActivity(),true,R.id.list_layout_reportUser);
                                Toast.makeText(getActivity(), "User reported!", Toast.LENGTH_SHORT).show();



                                // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Something went wrong,try again!", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}