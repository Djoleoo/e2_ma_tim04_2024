package com.example.eventplanner.fragments.priceList;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ProductListAdapter;
import com.example.eventplanner.adapters.ProductPriceListAdapter;
import com.example.eventplanner.adapters.ServicePriceListAdapter;
import com.example.eventplanner.databinding.FragmentProductsBinding;
import com.example.eventplanner.databinding.FragmentProductsPriceListBinding;
import com.example.eventplanner.databinding.FragmentServicesPriceListBinding;
import com.example.eventplanner.fragments.products.ProductsFragment;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductsPriceListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServicesPriceListFragment extends Fragment {

    private ServicePriceListAdapter adapter;

    private ArrayList<DocumentSnapshot> mProducts = new ArrayList<>();

    private FragmentServicesPriceListBinding binding;
    private static final String ARG_PARAM = "param";

    private String mParam1;
    private String mParam2;

    public ServicesPriceListFragment() {
        // Required empty public constructor
    }



    public static ServicesPriceListFragment newInstance(ArrayList<DocumentSnapshot> products) {
        ServicesPriceListFragment fragment = new ServicesPriceListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, products);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProducts = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new ServicePriceListAdapter(getActivity(), mProducts);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentServicesPriceListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}