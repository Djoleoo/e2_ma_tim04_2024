package com.example.eventplanner.fragments.priceList;

import static com.example.eventplanner.model.User.UserType.PUP;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentPriceListPageBinding;
import com.example.eventplanner.databinding.FragmentProductsPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.AddPackageFragment;
import com.example.eventplanner.fragments.products.AddProductFragment;
import com.example.eventplanner.fragments.products.AddServiceFragment;
import com.example.eventplanner.fragments.products.PackagesFragment;
import com.example.eventplanner.fragments.products.ProductsFragment;
import com.example.eventplanner.fragments.products.ServicesFragment;
import com.example.eventplanner.fragments.reportedUsers.UsersReportedPageFragment;
import com.example.eventplanner.model.User;
import com.example.eventplanner.pdf.PdfGenerator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PriceListPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PriceListPageFragment extends Fragment {

    public static ArrayList<DocumentSnapshot> mProductsDocuments = new ArrayList<>();
    public static ArrayList<DocumentSnapshot> mServicesDocuments = new ArrayList<>();
    public static ArrayList<DocumentSnapshot> mPackagesDocuments = new ArrayList<>();

    public User user;

    private FragmentPriceListPageBinding binding;

    public PriceListPageFragment() {
        // Required empty public constructor
    }


    public static PriceListPageFragment newInstance() {
        PriceListPageFragment fragment = new PriceListPageFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentActivity currentActivity = getActivity();

        binding = FragmentPriceListPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        findUserByUserEmail(currentUser.getEmail(), new OnUserLoadedListener() {
            @Override
            public void onUserLoaded(User company) {
                user = company;
                selectProducts(currentActivity);
                int checkedButtonId = binding.toggleGroupProducts.getCheckedButtonId();
                if(checkedButtonId == R.id.button_products){
                    FragmentTransition.to(ProductsPriceListFragment.newInstance(mProductsDocuments),currentActivity,true,R.id.list_layout_products);
                }
                else if (checkedButtonId == R.id.button_services) {
                    FragmentTransition.to(ServicesPriceListFragment.newInstance(mServicesDocuments), currentActivity, true, R.id.list_layout_products);
                }
                else if(checkedButtonId == R.id.button_packages){
                    FragmentTransition.to(PackagePriceListFragment.newInstance(mPackagesDocuments), currentActivity, true, R.id.list_layout_products);
                }
            }
        });

        FloatingActionButton pdfButton=binding.floatingActionButtonProducts;
        FloatingActionButton pdfAllButton=binding.floatingActionButtonAll;



        Button servicesBtn = binding.buttonServices;
        Button productsBtn = binding.buttonProducts;
        Button packagesBtn = binding.buttonPackages;

        servicesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectServices(currentActivity);
            }
        });
        productsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProducts(currentActivity);
            }
        });
        packagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPackages(currentActivity);
            }
        });

        pdfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedButtonId = binding.toggleGroupProducts.getCheckedButtonId();
                if(checkedButtonId == R.id.button_products){
                    PdfGenerator pdfGenerator = new PdfGenerator(getContext());
                    pdfGenerator.generateCompaniesProductPdf();
                    Toast.makeText(getContext(), "PDF product generated and saved.", Toast.LENGTH_SHORT).show();
                }
                else if (checkedButtonId == R.id.button_services) {
                    PdfGenerator pdfGenerator = new PdfGenerator(getContext());
                    pdfGenerator.generateCompaniesServicePdf();
                    Toast.makeText(getContext(), "PDF service generated and saved.", Toast.LENGTH_SHORT).show();
                }
                else if (checkedButtonId == R.id.button_packages) {
                    PdfGenerator pdfGenerator = new PdfGenerator(getContext());
                    pdfGenerator.generateCompaniesPackagePdf();
                    Toast.makeText(getContext(), "PDF package generated and saved.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        pdfAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PdfGenerator pdfGenerator = new PdfGenerator(getContext());
                pdfGenerator.generateCompaniesConsolidatedPdf();
            }
        });

        return root;
    }

    private void selectProducts(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(user.getUserType()== PUP) {
            db.collection("products").whereEqualTo("userEmail", user.getEmail()).whereEqualTo("visible", true).get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mProductsDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mProductsDocuments.add(document);
                                }
                                FragmentTransition.to(ProductsPriceListFragment.newInstance(mProductsDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
    }

    private void selectServices(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(user.getUserType()==PUP) {
            db.collection("services").whereEqualTo("userEmail", user.getEmail()).whereEqualTo("visible", true).get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mServicesDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mServicesDocuments.add(document);
                                }

                                FragmentTransition.to(ServicesPriceListFragment.newInstance(mServicesDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting services", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
    }

    private void selectPackages(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(user.getUserType()== PUP) {
            db.collection("packages").whereEqualTo("userEmail",user.getEmail()).whereEqualTo("visible",true).get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mPackagesDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mPackagesDocuments.add(document);
                                }

                                FragmentTransition.to(PackagePriceListFragment.newInstance(mPackagesDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting packages", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
    }

    private void findUserByUserEmail(String email, OnUserLoadedListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                user= document.toObject(User.class);
                                listener.onUserLoaded(user);
                                return;
                            }
                        } else {
                            Toast.makeText(getActivity(), "Error getting user", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    interface OnUserLoadedListener {
        void onUserLoaded(User user);
    }

}