package com.example.eventplanner.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Reservation;

import java.util.ArrayList;

public class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ViewHolder> {

    private ArrayList<Reservation> reservations;
    private OnCancelButtonClickListener cancelButtonListener;

    public ReservationsAdapter(ArrayList<Reservation> reservations) {
        this.reservations = reservations;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reservation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Reservation reservation = reservations.get(position);
        holder.serviceName.setText(reservation.getService().getName());
        holder.eventName.setText(reservation.getEvent().getName());
        holder.employeeName.setText(reservation.getEmployee().getFirstName() + " " + reservation.getEmployee().getLastName());
        holder.reservationDate.setText(reservation.getReservationDate());
        holder.status.setText(reservation.getStatus());

        // Show or hide the cancel button based on reservation status
        if (reservation.getStatus().equals("novo") || reservation.getStatus().equals("prihvaćeno")) {
            holder.cancelButton.setVisibility(View.VISIBLE);
            holder.cancelButton.setOnClickListener(v -> {
                if (cancelButtonListener != null) {
                    cancelButtonListener.onCancelClicked(reservation, v);
                }
            });
        } else {
            holder.cancelButton.setVisibility(View.GONE);
        }
    }



    @Override
    public int getItemCount() {
        return reservations.size();
    }

    public void updateReservations(ArrayList<Reservation> newReservations) {
        this.reservations = newReservations;
        notifyDataSetChanged();
    }


    public void setOnCancelButtonClickListener(OnCancelButtonClickListener listener) {
        this.cancelButtonListener = listener;
    }

    public interface OnCancelButtonClickListener {
        void onCancelClicked(Reservation reservation, View view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serviceName, eventName, employeeName, reservationDate, status;
        public Button cancelButton;

        public ViewHolder(View itemView) {
            super(itemView);
            serviceName = itemView.findViewById(R.id.tv_service_name);
            eventName = itemView.findViewById(R.id.tv_event_name);
            employeeName = itemView.findViewById(R.id.tv_employee_name);
            reservationDate = itemView.findViewById(R.id.tv_reservation_date);
            status = itemView.findViewById(R.id.tv_reservation_status);
            cancelButton = itemView.findViewById(R.id.btn_cancel_reservation);
        }
    }
}
