package com.example.eventplanner.fragments.categories;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.HomeActivity;
import com.example.eventplanner.databinding.FragmentAddCategoryBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.SubCategorySuggestion;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class AddCategoryFragment extends Fragment {

    private FragmentAddCategoryBinding binding;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    ExecutorService executor = Executors.newSingleThreadExecutor();


    private String mParam1;
    private String mParam2;

    public AddCategoryFragment() {
        // Required empty public constructor
    }


    public static AddCategoryFragment newInstance() {
        AddCategoryFragment fragment = new AddCategoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentAddCategoryBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        EditText categoryName = binding.categoryName;
        TextView categoryDescription = binding.categoryDescription;
        Button button = binding.createCategoryBtn;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = categoryName.getText().toString();
                String description = categoryDescription.getText().toString();
                showConfirmationDialog(name,description);
            }
        });
        return root;
    }

    private void showConfirmationDialog(String name,String description) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Category");
        builder.setMessage("Are you sure you want to add this category?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Category category = new Category(name,description);
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("categories").add(category)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                SubCategorySuggestion suggestion = new SubCategorySuggestion(null,"Subkategorija222", SubCategory.Status.PRODUCT,"AAAAA","srajer.ra128.2020@uns.ac.rs",category,"q1r5nctEwVWNAIoGA4HJIVsMp8x1");
                                db.collection("subCategoriesSuggestions").add(suggestion);
                                String documentId = documentReference.getId();
                                FragmentTransition.to(CategoriesPageFragment.newInstance(),getActivity(),true,R.id.list_layout);
                                Toast.makeText(getActivity(), "Category created!", Toast.LENGTH_SHORT).show();
                                FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                addBtn.setVisibility(View.VISIBLE);
                                MaterialButtonToggleGroup toggleGroup = requireActivity().findViewById(R.id.toggle_group);
                                toggleGroup.check(R.id.button_categories); // Postavljanje togle-ovanog dugmeta na "Categories"
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), "Something went wrong,try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}