package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.SingleProductFragment;
import com.example.eventplanner.fragments.reportUser.SingleReportUserFragment;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ReportUserListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private ArrayList<DocumentSnapshot> aProduct;
    private FragmentActivity mContext;

    public ReportUserListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> products){
        super(context, R.layout.report_user_list_item);
        mContext = context;
        aProduct = products;
    }

    @Override
    public int getCount() {
        return aProduct.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.report_user_list_item,
                    parent, false);
        }
        LinearLayout productItem = convertView.findViewById(R.id.report_user_list_item);
        TextView userName = convertView.findViewById(R.id.user_name);
        Button viewButton = convertView.findViewById(R.id.view_user_button);


        if(document != null){
            userName.setText(document.getString("firstName")+" "+document.getString("lastName"));
            //imageView.setImageResource(document.getGallery().get(0));
        }

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user= document.toObject(User.class);

                FragmentTransition.to(SingleReportUserFragment.newInstance(user,document.getId()),mContext,true,R.id.list_layout_reportUser);

            }
        });








        return convertView;
    }



}
