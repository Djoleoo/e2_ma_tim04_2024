package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.SinglePackageFragment;
import com.example.eventplanner.model.Favorites;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class PackageListAdapter extends ArrayAdapter<DocumentSnapshot> {
    private ArrayList<DocumentSnapshot> aPackage;
    private FragmentActivity mContext;
    private FirebaseFirestore db;
    private User currentUser;

    public PackageListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> packages) {
        super(context, R.layout.package_list_item);
        mContext = context;
        aPackage = packages;
        db = FirebaseFirestore.getInstance();
        loadCurrentUser();
    }

    private void loadCurrentUser() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            db.collection("users").document(firebaseUser.getUid()).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            currentUser = documentSnapshot.toObject(User.class);
                        }
                    })
                    .addOnFailureListener(e -> {
                        // Handle any errors here
                    });
        }
    }

    @Override
    public int getCount() {
        return aPackage.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aPackage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.package_list_item,
                    parent, false);
        }

        LinearLayout packageItem = convertView.findViewById(R.id.package_list_item);
        TextView packageName = convertView.findViewById(R.id.package_name);
        Button viewButton = convertView.findViewById(R.id.view_package_button);
        ImageView imageView = convertView.findViewById(R.id.package_image);
        Button addToFavorites = convertView.findViewById(R.id.package_to_favorites_button);
        Button deleteButton = convertView.findViewById(R.id.delete_package_button);

        if (document != null) {
            packageName.setText(document.getString("name"));

            // Check if the package is already in the favorites and update the button text
            checkIfPackageIsFavorite(document.getId(), addToFavorites);
        }

        viewButton.setOnClickListener(v -> {
            Package aPackage1 = document.toObject(Package.class);
            FragmentTransition.to(SinglePackageFragment.newInstance(aPackage1, document.getId()), mContext, true, R.id.list_layout_products);
        });

        addToFavorites.setOnClickListener(v -> {
            Package aPackage1 = document.toObject(Package.class);
            if (aPackage1 != null) {
                toggleFavoriteStatus(document.getId(), aPackage1, addToFavorites);
            }
        });



        deleteButton.setOnClickListener(v -> showDeleteConfirmationDialog(document));

        return convertView;
    }

    private void checkIfPackageIsFavorite(String packageId, Button addToFavorites) {
        if (currentUser == null) {
            return;
        }

        db.collection("favorites").whereEqualTo("user.id", currentUser.getId())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot favoriteDoc = queryDocumentSnapshots.getDocuments().get(0);
                        Favorites favorites = favoriteDoc.toObject(Favorites.class);

                        if (favorites != null && favorites.getFavoritePackages().stream().anyMatch(p -> p.getId().equals(packageId))) {
                            addToFavorites.setText("Remove from favorites");
                        } else {
                            addToFavorites.setText("Favorite");
                        }
                    } else {
                        addToFavorites.setText("Favorite");
                    }
                })
                .addOnFailureListener(e -> {
                    // Handle failure
                });
    }

    private void toggleFavoriteStatus(String packageId, Package aPackage, Button addToFavorites) {
        if (currentUser == null) {
            Toast.makeText(mContext, "User not loaded. Please try again.", Toast.LENGTH_SHORT).show();
            return;
        }

        db.collection("favorites").whereEqualTo("user.id", currentUser.getId())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        DocumentSnapshot favoriteDoc = queryDocumentSnapshots.getDocuments().get(0);
                        Favorites favorites = favoriteDoc.toObject(Favorites.class);

                        if (favorites != null) {
                            if (favorites.getFavoritePackages().stream().anyMatch(p -> p.getId().equals(packageId))) {
                                favorites.removeFavoritePackageById(packageId);
                                addToFavorites.setText("Favorite");
                            } else {
                                aPackage.setId(packageId); // Ensure package has the correct ID
                                favorites.addFavoritePackage(aPackage);
                                addToFavorites.setText("Remove from favorites");
                            }

                            db.collection("favorites").document(favoriteDoc.getId())
                                    .set(favorites)
                                    .addOnSuccessListener(aVoid -> {
                                        Toast.makeText(mContext, "Favorites updated", Toast.LENGTH_SHORT).show();
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast.makeText(mContext, "Failed to update favorites", Toast.LENGTH_SHORT).show();
                                    });
                        }
                    } else {
                        // No Favorites entry exists, create one
                        Favorites newFavorites = new Favorites();
                        newFavorites.setUser(currentUser);
                        aPackage.setId(packageId); // Ensure package has the correct ID
                        newFavorites.addFavoritePackage(aPackage);

                        db.collection("favorites").add(newFavorites)
                                .addOnSuccessListener(documentReference -> {
                                    addToFavorites.setText("Remove from favorites");
                                    Toast.makeText(mContext, "Added to favorites", Toast.LENGTH_SHORT).show();
                                })
                                .addOnFailureListener(e -> {
                                    Toast.makeText(mContext, "Failed to add to favorites", Toast.LENGTH_SHORT).show();
                                });
                    }
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(mContext, "Failed to load favorites", Toast.LENGTH_SHORT).show();
                });
    }

    private void showDeleteConfirmationDialog(DocumentSnapshot document) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete package");
        builder.setMessage("Are you sure you want to delete this package?");
        builder.setPositiveButton("Yes", (dialog, which) -> {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("packages").document(document.getId()).delete()
                    .addOnSuccessListener(aVoid -> {
                        aPackage.remove(document);
                        notifyDataSetChanged();
                    }).addOnFailureListener(e -> {
                        Toast.makeText(getContext(), "Failed to delete package", Toast.LENGTH_SHORT).show();
                    });
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
