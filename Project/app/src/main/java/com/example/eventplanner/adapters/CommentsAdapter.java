package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Comment;

import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {

    private List<Comment> commentsList;
    private OnCommentReportListener reportListener;

    public interface OnCommentReportListener {
        void onReport(Comment comment);
    }

    public CommentsAdapter(List<Comment> commentsList, OnCommentReportListener reportListener) {
        this.commentsList = commentsList;
        this.reportListener = reportListener;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        Comment comment = commentsList.get(position);
        holder.tvCommentText.setText(comment.getText());
        holder.tvCommentRating.setText(comment.getRating());
        holder.tvCommentDate.setText(comment.getDate().toString());  // Convert date to a string representation

        holder.btnReport.setOnClickListener(v -> reportListener.onReport(comment));
    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder {
        TextView tvCommentText, tvCommentRating, tvCommentDate;
        Button btnReport;

        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCommentText = itemView.findViewById(R.id.tv_comment_text);
            tvCommentRating = itemView.findViewById(R.id.tv_comment_rating);
            tvCommentDate = itemView.findViewById(R.id.tv_comment_date);
            btnReport = itemView.findViewById(R.id.btn_report);
        }
    }

    public void updateComments(List<Comment> newComments) {
        this.commentsList = newComments;
        notifyDataSetChanged();
    }
}
