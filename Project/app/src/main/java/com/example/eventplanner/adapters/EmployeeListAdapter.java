package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.User;

import java.util.ArrayList;

public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.EmployeeViewHolder> {

    private ArrayList<User> userList;
    private Context mContext;

    public EmployeeListAdapter(Context context, ArrayList<User> userList) {
        this.mContext = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.employee_list_item, parent, false);
        return new EmployeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, int position) {
        User user = userList.get(position);
        holder.fullNameTextView.setText(user.getFirstName() + " " + user.getLastName());
        holder.emailTextView.setText(user.getEmail());
        holder.workingHoursTextView.setText("40");
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    static class EmployeeViewHolder extends RecyclerView.ViewHolder {
        TextView fullNameTextView;
        TextView emailTextView;
        TextView workingHoursTextView;

        EmployeeViewHolder(@NonNull View itemView) {
            super(itemView);
            fullNameTextView = itemView.findViewById(R.id.text_view_employee_name);
            emailTextView = itemView.findViewById(R.id.text_view_employee_email);
            workingHoursTextView = itemView.findViewById(R.id.text_view_employee_working_hours);
        }
    }
}
