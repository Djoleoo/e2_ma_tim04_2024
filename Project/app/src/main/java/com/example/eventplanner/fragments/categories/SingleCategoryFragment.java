package com.example.eventplanner.fragments.categories;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentCategoriesBinding;
import com.example.eventplanner.databinding.FragmentSingleCategoryBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.events.EventPageFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Notification;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleCategoryFragment extends Fragment {

    private FragmentSingleCategoryBinding binding;
    private Category mCategory;
    private String documentId;

    private static final String ARG_CATEGORY = "category";
    private static final String ARG_EVENT_ID = "categoryId";
    ExecutorService executor = Executors.newSingleThreadExecutor();


    public SingleCategoryFragment() {
        // Required empty public constructor
    }

    public static SingleCategoryFragment newInstance(Category category,String documentId) {
        SingleCategoryFragment fragment = new SingleCategoryFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CATEGORY, category);
        args.putString(ARG_EVENT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategory = getArguments().getParcelable(ARG_CATEGORY);
            documentId = getArguments().getString(ARG_EVENT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentSingleCategoryBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        EditText categoryName = binding.categoryNameEdit;
        EditText categoryDescription = binding.categoryDescriptionEdit;
        Button editButton = binding.buttonEdit;
        Button submitButton = binding.buttonConfirm;
        Button cancelButton = binding.buttonCancel;

        submitButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);

        if(mCategory != null) {
            categoryName.setText(mCategory.getName());
            categoryDescription.setText(mCategory.getDescription());
        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryName.setEnabled(true);
                categoryDescription.setEnabled(true);
                submitButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                editButton.setVisibility(View.GONE);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryName.setEnabled(false);
                categoryDescription.setEnabled(false);
                submitButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Category category = new Category(categoryName.getText().toString(),categoryDescription.getText().toString());
                showEditConfirmationDialog(category);
            }
        });

        return root;
    }

    private void showEditConfirmationDialog(Category category) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit category");
        builder.setMessage("Are you sure you want to edit this category?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("categories").document(documentId);
                docRef.set(category)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                executor.execute(()->{
                                    Notification notification = new Notification("Category change", "There was a category change ,check it out!", null, false, Notification.UserType.PUP);
                                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                                    db.collection("notifications").add(notification);
                                });
                                FragmentTransition.to(CategoriesPageFragment.newInstance(),getActivity(),true,R.id.list_layout);
                                Toast.makeText(getActivity(), "Category updated!", Toast.LENGTH_SHORT).show();
                                FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                addBtn.setVisibility(View.VISIBLE);
                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nesto je puklo ba!", Toast.LENGTH_SHORT).show();
                        }
                       });
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}