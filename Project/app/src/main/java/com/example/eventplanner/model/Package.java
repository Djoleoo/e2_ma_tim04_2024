package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Package implements Parcelable {
    private String id;
    private String name;
    private String description;
    private double price;
    private double discount;
    private ArrayList<Integer> gallery;
    private boolean visible;
    private boolean available;
    private Category category;
    private ArrayList<Product> products;
    private ArrayList<Service> services;
    private ArrayList<SubCategory> subCategories;
    private ArrayList<Event> eventTypes;
    private int deadlineForReservation;
    private int deadlineForCancel;
    private boolean automaticAcceptance;
    private boolean deleted;

    private String userEmail;
    private Company company;

    public Package() {}

    public Package(String id, String name, String description, double discount, boolean visible, boolean available, Category category, ArrayList<Product> products, ArrayList<Service> services, boolean automaticAcceptance, String userEmail, Company company) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.discount = discount;
        this.visible = visible;
        this.available = available;
        this.category = category;
        this.products = products;
        this.services = services;
        this.subCategories = extractUniqueSubCategoriesFromProducts(products);
        this.subCategories.addAll(extractUniqueSubCategoriesFromServices(services));
        this.eventTypes = extractUniqueEventTypes(products, services);
        this.price = calculateTotalPrice(products, services, discount);
        this.deadlineForReservation = findMinimumDeadlineForReservation(services);
        this.deadlineForCancel = findMinimumDeadlineForCancel(services);
        this.gallery = combineGallery(products, services);
        this.deleted = false;
        this.automaticAcceptance = checkAutomaticAcceptance(automaticAcceptance);
        this.userEmail = userEmail;
        this.company = company;
    }

    public Package(String name, String description, double discount, boolean visible, boolean available, Category category, ArrayList<Product> products, ArrayList<Service> services, boolean automaticAcceptance, String userEmail, Company company) {
        this.name = name;
        this.description = description;
        this.discount = discount;
        this.visible = visible;
        this.available = available;
        this.category = category;
        this.products = products;
        this.services = services;
        this.subCategories = extractUniqueSubCategoriesFromProducts(products);
        this.subCategories.addAll(extractUniqueSubCategoriesFromServices(services));
        this.eventTypes = extractUniqueEventTypes(products, services);
        this.price = calculateTotalPrice(products, services, discount);
        this.deadlineForReservation = findMinimumDeadlineForReservation(services);
        this.deadlineForCancel = findMinimumDeadlineForCancel(services);
        this.gallery = combineGallery(products, services);
        this.deleted = false;
        this.automaticAcceptance = checkAutomaticAcceptance(automaticAcceptance);
        this.userEmail = userEmail;
        this.company = company;
    }

    protected Package(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        price = in.readDouble();
        discount = in.readDouble();
        visible = in.readByte() != 0;
        available = in.readByte() != 0;
        category = in.readParcelable(Category.class.getClassLoader());
        products = in.createTypedArrayList(Product.CREATOR);
        services = in.createTypedArrayList(Service.CREATOR);
        subCategories = in.createTypedArrayList(SubCategory.CREATOR);
        eventTypes = in.createTypedArrayList(Event.CREATOR);
        deadlineForReservation = in.readInt();
        deadlineForCancel = in.readInt();
        automaticAcceptance = in.readByte() != 0;
        deleted = in.readByte() != 0;
        userEmail = in.readString();
        company = in.readParcelable(Company.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeByte((byte) (visible ? 1 : 0));
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeParcelable(category, flags);
        dest.writeTypedList(products);
        dest.writeTypedList(services);
        dest.writeTypedList(subCategories);
        dest.writeTypedList(eventTypes);
        dest.writeInt(deadlineForReservation);
        dest.writeInt(deadlineForCancel);
        dest.writeByte((byte) (automaticAcceptance ? 1 : 0));
        dest.writeByte((byte) (deleted ? 1 : 0));
        dest.writeString(userEmail);
        dest.writeParcelable(company, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Package> CREATOR = new Creator<Package>() {
        @Override
        public Package createFromParcel(Parcel in) {
            return new Package(in);
        }

        @Override
        public Package[] newArray(int size) {
            return new Package[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public ArrayList<Integer> getGallery() {
        return gallery;
    }

    public void setGallery(ArrayList<Integer> gallery) {
        this.gallery = gallery;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public ArrayList<Service> getServices() {
        return services;
    }

    public void setServices(ArrayList<Service> services) {
        this.services = services;
    }

    public ArrayList<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(ArrayList<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    public ArrayList<Event> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(ArrayList<Event> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public int getDeadlineForReservation() {
        return deadlineForReservation;
    }

    public void setDeadlineForReservation(int deadlineForReservation) {
        this.deadlineForReservation = deadlineForReservation;
    }

    public int getDeadlineForCancel() {
        return deadlineForCancel;
    }

    public void setDeadlineForCancel(int deadlineForCancel) {
        this.deadlineForCancel = deadlineForCancel;
    }

    public boolean isAutomaticAcceptance() {
        return automaticAcceptance;
    }

    public void setAutomaticAcceptance(boolean automaticAcceptance) {
        this.automaticAcceptance = automaticAcceptance;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ArrayList<SubCategory> extractUniqueSubCategoriesFromProducts(ArrayList<Product> products) {
        ArrayList<SubCategory> uniqueSubCategories = new ArrayList<>();

        // Loop through each product
        for (Product product : products) {
            SubCategory subCategory = product.getSubcategory();
            // Check if the subcategory is not null and not already in the list
            if (subCategory != null && !uniqueSubCategories.contains(subCategory)) {
                uniqueSubCategories.add(subCategory);
            }
        }

        return uniqueSubCategories;
    }

    public ArrayList<SubCategory> extractUniqueSubCategoriesFromServices(ArrayList<Service> services) {
        ArrayList<SubCategory> uniqueSubCategories = new ArrayList<>();

        // Loop through each product
        for (Service service : services) {
            SubCategory subCategory = service.getSubcategory();
            // Check if the subcategory is not null and not already in the list
            if (subCategory != null && !uniqueSubCategories.contains(subCategory)) {
                uniqueSubCategories.add(subCategory);
            }
        }

        return uniqueSubCategories;
    }


    public ArrayList<Event> extractUniqueEventTypes(ArrayList<Product> products,ArrayList<Service> services) {
        Set<Event> uniqueEventTypes = new HashSet<>();

        // Extract event types from products
        for (Product product : products) {
            if (product.getEventType() != null) {
                uniqueEventTypes.addAll(product.getEventType());
            }
        }

        // Extract event types from services
        for (Service service : services) {
            if (service.getEventTypes() != null) {
                uniqueEventTypes.addAll(service.getEventTypes());
            }
        }

        return new ArrayList<>(uniqueEventTypes);
    }

    public double calculateTotalPrice(ArrayList<Product> products,ArrayList<Service> services,double discount) {
        double totalPrice = 0.0;

        // Calculate total price from products
        for (Product product : products) {
            totalPrice += product.getPrice();
        }

        // Calculate total price from services
        for (Service service : services) {
            totalPrice += service.getPriceTotal();
        }

        totalPrice=totalPrice*(100-discount)/100;

        return totalPrice;
    }

    public int findMinimumDeadlineForReservation(ArrayList<Service> services) {
        int minDeadline = Integer.MAX_VALUE;

        // Iterate through all services
        for (Service service : services) {
            if (service.getDeadlineForReservation() < minDeadline) {
                minDeadline = service.getDeadlineForReservation();
            }
        }

        // If no services found, return -1
        if (minDeadline == Integer.MAX_VALUE) {
            return 0;
        }

        return minDeadline;
    }

    public int findMinimumDeadlineForCancel(ArrayList<Service> services) {
        int minDeadline = Integer.MAX_VALUE;

        // Iterate through all services
        for (Service service : services) {
            if (service.getDeadlineForReservation() < minDeadline) {
                minDeadline = service.getDeadlineForCancel();
            }
        }

        // If no services found, return -1
        if (minDeadline == Integer.MAX_VALUE) {
            return 0;
        }

        return minDeadline;
    }

    public ArrayList<Integer> combineGallery(ArrayList<Product> products,ArrayList<Service> services) {
        ArrayList<Integer> combinedGallery = new ArrayList<>();

        // Combine gallery from products
        for (Product product : products) {
            if (product.getGallery() != null) {
                combinedGallery.addAll(product.getGallery());
            }
        }

        // Combine gallery from services
        for (Service service : services) {
            if (service.getGallery() != null) {
                combinedGallery.addAll(service.getGallery());
            }
        }

        return combinedGallery;
    }

    public boolean checkAutomaticAcceptance(boolean automaticAcceptance) {
        for (Service service : services) {
            if (!service.isAutomaticAcceptance()) {
                return false;
            }
        }
        return automaticAcceptance;
    }



}
