package com.example.eventplanner.activities;

import android.os.Bundle;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.ActivityLoginAndRegistrationBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.LoginFragment;

public class LoginAndRegistrationActivity extends AppCompatActivity {

    private ActivityLoginAndRegistrationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityLoginAndRegistrationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        FragmentTransition.to(LoginFragment.newInstance(),LoginAndRegistrationActivity.this,true,R.id.loginCard);

    }


}