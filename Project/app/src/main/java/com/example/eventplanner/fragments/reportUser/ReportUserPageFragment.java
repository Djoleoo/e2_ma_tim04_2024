package com.example.eventplanner.fragments.reportUser;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentPupVRegistrationsPageBinding;
import com.example.eventplanner.databinding.FragmentReportUserPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.PupVRegistrations.PupVRegistrationsFragment;
import com.example.eventplanner.fragments.PupVRegistrations.PupVRegistrationsPageFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReportUserPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReportUserPageFragment extends Fragment {

    private ArrayList<DocumentSnapshot> mUserDocuments = new ArrayList<>();
    private FragmentReportUserPageBinding binding;

    public ReportUserPageFragment() {
    }

    public static ReportUserPageFragment newInstance() {
        ReportUserPageFragment fragment = new ReportUserPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();
        binding = FragmentReportUserPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        select(currentActivity);
        return root;
    }

    private void select(FragmentActivity currentActivity) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").whereEqualTo("userType", "OD")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            mUserDocuments.clear();
                            for(QueryDocumentSnapshot document : task.getResult()){
                                mUserDocuments.add(document);
                            }
                            FragmentTransition.to(ReportUserFragment.newInstance(mUserDocuments),currentActivity,true,R.id.list_layout_reportUser);
                        }
                        else{
                            Toast.makeText(newInstance().getActivity(), "Error getting registrations", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}