package com.example.eventplanner.fragments.reportUser;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner.adapters.ProductListAdapter;
import com.example.eventplanner.adapters.ReportUserListAdapter;
import com.example.eventplanner.databinding.FragmentProductsBinding;
import com.example.eventplanner.databinding.FragmentReportUserBinding;
import com.example.eventplanner.fragments.products.ProductsFragment;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReportUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReportUserFragment extends Fragment {

    private ReportUserListAdapter adapter;

    private ArrayList<DocumentSnapshot> mUsers = new ArrayList<>();

    private FragmentReportUserBinding binding;
    private static final String ARG_PARAM = "param";

    private String mParam1;
    private String mParam2;

    public ReportUserFragment() {
        // Required empty public constructor
    }

    public static ReportUserFragment newInstance(ArrayList<DocumentSnapshot> products) {
        ReportUserFragment fragment = new ReportUserFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, products);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUsers = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new ReportUserListAdapter(getActivity(), mUsers);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentReportUserBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}