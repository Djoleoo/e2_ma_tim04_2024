package com.example.eventplanner.fragments.PupVRegistrations;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentEventPageBinding;
import com.example.eventplanner.databinding.FragmentPupVRegistrationsPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class PupVRegistrationsPageFragment extends Fragment {
    private ArrayList<DocumentSnapshot>mRegistrationsDocuments  = new ArrayList<>();
    private FragmentPupVRegistrationsPageBinding binding;

    public PupVRegistrationsPageFragment() {
    }

    public static PupVRegistrationsPageFragment newInstance() {
        PupVRegistrationsPageFragment fragment = new PupVRegistrationsPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();
        binding = FragmentPupVRegistrationsPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        select(currentActivity);
        return root;
    }

    private void select(FragmentActivity currentActivity) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("pupvRegistrationRequests").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            mRegistrationsDocuments.clear();
                            for(QueryDocumentSnapshot document : task.getResult()){
                                mRegistrationsDocuments.add(document);
                            }
                            FragmentTransition.to(PupVRegistrationsFragment.newInstance(mRegistrationsDocuments),currentActivity,true,R.id.list_layout);
                        }
                        else{
                            Toast.makeText(newInstance().getActivity(), "Error getting registrations", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}