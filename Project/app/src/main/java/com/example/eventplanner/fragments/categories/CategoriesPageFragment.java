package com.example.eventplanner.fragments.categories;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentCategoriesPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.SubCategorySuggestion;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class CategoriesPageFragment extends Fragment {

    public static ArrayList<DocumentSnapshot> mCategoriesDocuments = new ArrayList<>();
    public static ArrayList<DocumentSnapshot> mSubCategoriesDocuments = new ArrayList<>();
    public static ArrayList<DocumentSnapshot> mSuggestionDocuments = new ArrayList<>();
    public static ArrayList<Category> categories = new ArrayList<Category>();
    public static ArrayList<SubCategory> subCategories = new ArrayList<SubCategory>();
    public static ArrayList<SubCategorySuggestion> suggestions = new ArrayList<SubCategorySuggestion>();
    private FragmentCategoriesPageBinding binding;
    //private static final String ARG_PARAM1 = "param1";
    //private static final String ARG_PARAM2 = "param2";
    //private String mParam1;
    //private String mParam2;

    public CategoriesPageFragment() {
        // Required empty public constructor
    }


    public static CategoriesPageFragment newInstance() {
        CategoriesPageFragment fragment = new CategoriesPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentActivity currentActivity = getActivity();

        binding = FragmentCategoriesPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

       // prepareCategoriesList(categories);
        //FragmentTransition.to(CategoriesFragment.newInstance(categories), currentActivity,true,R.id.list_layout);
        selectCategories(currentActivity);
        FloatingActionButton addButton = binding.floatingActionButton;
        addButton.setVisibility(View.VISIBLE);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedButtonId = binding.toggleGroup.getCheckedButtonId();
                if(checkedButtonId == R.id.button_categories){
                    addButton.setVisibility(View.GONE);
                    FragmentTransition.to(AddCategoryFragment.newInstance(),currentActivity,true,R.id.list_layout);
                }
                else if (checkedButtonId == R.id.button_subcategories) {
                    addButton.setVisibility(View.GONE);
                    FragmentTransition.to(AddSubCategoryFragment.newInstance(), currentActivity, true, R.id.list_layout);
                }
            }
        });

        Button subCategoriesBtn = binding.buttonSubcategories;
        Button categoriesBtn = binding.buttonCategories;
        Button suggestionsBtn = binding.buttonSuggestions;

        subCategoriesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectSubCategories(currentActivity);
                //FragmentTransition.to(SubCategoriesFragment.newInstance(mSubCategoriesDocuments),currentActivity,true,R.id.list_layout);
                addButton.setVisibility(View.VISIBLE);
            }
        });
        categoriesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransition.to(CategoriesFragment.newInstance(mCategoriesDocuments),currentActivity,true,R.id.list_layout);
                addButton.setVisibility(View.VISIBLE);
            }
        });

        suggestionsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //prepareSubCategorySuggestionsList(suggestions);
                //FragmentTransition.to(SuggestionFragment.newInstance(suggestions),currentActivity,true,R.id.list_layout);
                selectSuggestion(currentActivity);
                addButton.setVisibility(View.GONE);
            }
        });

        return root;
    }

    private void selectCategories(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mCategoriesDocuments.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                mCategoriesDocuments.add(document);
                            }
                            FragmentTransition.to(CategoriesFragment.newInstance(mCategoriesDocuments), currentActivity,true,R.id.list_layout);
                        } else {
                            Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void selectSubCategories(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("subCategories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mSubCategoriesDocuments.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                mSubCategoriesDocuments.add(document);
                            }
                            FragmentTransition.to(SubCategoriesFragment.newInstance(mSubCategoriesDocuments), currentActivity,true,R.id.list_layout);
                        } else {
                            Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void selectSuggestion(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("subCategoriesSuggestions").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mSuggestionDocuments.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                mSuggestionDocuments.add(document);
                            }
                            FragmentTransition.to(SuggestionFragment.newInstance(mSuggestionDocuments), currentActivity,true,R.id.list_layout);
                        } else {
                            Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }


    /*
    private void prepareCategoriesList(ArrayList<Category> categories){
        categories.add(new Category("KATEGORIJA 1","Pružanje usluga pripreme i posluživanja hrane i pića za događaje,\n" +
                " uključujući koktele, svečane večere, švedske stolove, tematske obroke i drugo.\n" +
                " Lokacije koje nude mogućnost organizacije događaja unutar njihovih prostora,\n" +
                " često s kompletnim ugostiteljskim uslugama. Ovo može uključivati restorane sa\n" +
                " privatnim sobama ili sale za bankete specijalizovane za veće događaje. Usluge\n" +
                " poput degustacija vina, koktel barmena, mobilnih barova, tematskih hrana i pića\n" +
                " koje dodaju poseban dodir događajima. Specijalizovane pekare i poslastičarnice\n" +
                " koje nude dizajnirane torte, slatke stolove i druge slatkiše prilagođene temi\n" +
                " događaja."));
        categories.add(new Category("KATEGORIJA 2","Hoteli, vile, apartmani i druge opcije smeštaja za goste događaja 2"));
        categories.add(new Category("KATEGORIJA 3","DJ-evi, bendovi, solo izvođači, animatori za decu, plesači"));
        categories.add(new Category("KATEGORIJA 4","Profesionalno fotografisanje i snimanje događaja."));
        categories.add(new Category("KATEGORIJA 5","Ova kategorija može uključivati ne samo tematsku dekoraciju prostora,\n" +
                " cvetne aranžmane, balone, stolnjake, itd. već i specijalističko osvetljenje koje\n" +
                " može transformisati prostor događaja, uključujući scensko osvetljenje, LED\n" +
                " rasvetu, laserske efekte i druge"));
        categories.add(new Category("KATEGORIJA 6","Iznajmljivanje ili kupovina formalne odeće, usluge stilista"));
        categories.add(new Category("KATEGORIJA 7","Agencije ili pojedinci specijalizovani za planiranje i koordinaciju događaja."));
    }

    private void prepareSubCategoriesList(ArrayList<SubCategory> subCategories){
        subCategories.add(new SubCategory("SUBKATEGORIJA 1","Opis podkategorije 1", categories.get(0), SubCategory.Status.SERVICE));
        subCategories.add(new SubCategory("SUBKATEGORIJA 2","Opis podkategorije 2", categories.get(0), SubCategory.Status.PRODUCT));
        subCategories.add(new SubCategory("SUBKATEGORIJA 3","Opis podkategorije 3", categories.get(1), SubCategory.Status.SERVICE));
        subCategories.add(new SubCategory("SUBKATEGORIJA 4","Opis podkategorije 4", categories.get(1), SubCategory.Status.PRODUCT));

    }
q
    private void prepareSubCategorySuggestionsList(ArrayList<SubCategorySuggestion> subCategorySuggestions){
        subCategorySuggestions.add(new SubCategorySuggestion(1L, "Podkategorija 1", SubCategory.Status.SERVICE, "Opis podkategorije 1", "Korisničko ime 1"));
        subCategorySuggestions.add(new SubCategorySuggestion(2L, "Podkategorija 2", SubCategory.Status.PRODUCT, "Opis podkategorije 2", "Korisničko ime 2"));
        subCategorySuggestions.add(new SubCategorySuggestion(3L, "Podkategorija 3", SubCategory.Status.SERVICE, "Opis podkategorije 3", "Korisničko ime 3"));
        subCategorySuggestions.add(new SubCategorySuggestion(4L, "Podkategorija 4", SubCategory.Status.PRODUCT, "Opis podkategorije 4", "Korisničko ime 4"));
        // Dodajte ostale podkategorije kako je potrebno
    }
 */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}