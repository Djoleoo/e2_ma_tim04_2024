package com.example.eventplanner.model.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.eventplanner.model.Package;


import java.util.Date;

public class PackageReport implements  Parcelable {

    private Long id;

    private Package aPackage;
    private Date time;
    private String reason;
    private Status status;

    public PackageReport(){

    }

    public PackageReport(Long id, Package product, Date time, String reason, Status status) {
        this.id = id;
        this.aPackage = product;
        this.time = time;
        this.reason = reason;
        this.status = status;
    }

    protected PackageReport(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        aPackage = in.readParcelable(Package.class.getClassLoader());
        time = new Date(in.readLong());
        reason = in.readString();
        status = Status.valueOf(in.readString());


    }


    public static final Creator<PackageReport> CREATOR = new Creator<PackageReport>() {
        @Override
        public PackageReport createFromParcel(Parcel in) {
            return new PackageReport(in);
        }

        @Override
        public PackageReport[] newArray(int size) {
            return new PackageReport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeParcelable(aPackage, flags);
        dest.writeLong(time.getTime());
        dest.writeString(reason);
        dest.writeString(status.name());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Status {
        REPORTED,
        ACCEPTED,
        REJECTED
    }
}
