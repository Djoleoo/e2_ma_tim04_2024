package com.example.eventplanner.fragments.comment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Comment;
import com.example.eventplanner.model.Notification;
import com.google.firebase.firestore.FirebaseFirestore;

public class ReportedCommentsFragment extends Fragment {

    private LinearLayout commentsContainer;
    private FirebaseFirestore db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reported_comments, container, false);

        commentsContainer = view.findViewById(R.id.comments_container);
        db = FirebaseFirestore.getInstance();

        loadReportedComments();

        return view;
    }

    private void loadReportedComments() {
        db.collection("comments")
                .whereEqualTo("reported", true)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        for (com.google.firebase.firestore.DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                            Comment comment = document.toObject(Comment.class);
                            comment.setId(document.getId());
                            addCommentView(comment);
                        }
                    } else {
                        Toast.makeText(getContext(), "No reported comments found.", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(e -> Toast.makeText(getContext(), "Failed to load reported comments", Toast.LENGTH_SHORT).show());
    }

    private void addCommentView(Comment comment) {
        View commentView = getLayoutInflater().inflate(R.layout.item_reported_comment, commentsContainer, false);

        TextView tvComment = commentView.findViewById(R.id.tv_comment);
        TextView tvReportedBy = commentView.findViewById(R.id.tv_reported_by);
        TextView tvReportReason = commentView.findViewById(R.id.tv_report_reason);
        TextView tvReportDate = commentView.findViewById(R.id.tv_report_date);
        Button btnAccept = commentView.findViewById(R.id.btn_accept);
        Button btnReject = commentView.findViewById(R.id.btn_reject);

        tvComment.setText(comment.getText());
        tvReportedBy.setText("Reported by: " + comment.getReportedBy());
        tvReportReason.setText("Reason: " + comment.getReportReason());
        tvReportDate.setText("Date: " + comment.getDate().toString());

        btnAccept.setOnClickListener(v -> handleReportDecision(comment, true));
        btnReject.setOnClickListener(v -> handleReportDecision(comment, false));

        commentsContainer.addView(commentView);
    }

    private void handleReportDecision(Comment comment, boolean accepted) {
        if (accepted) {
            db.collection("comments")
                    .document(comment.getId())
                    .delete()
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(getContext(), "Comment deleted", Toast.LENGTH_SHORT).show();
                        commentsContainer.removeAllViews();
                        loadReportedComments();
                    });
        } else {
            db.collection("comments")
                    .document(comment.getId())
                    .update("reportStatus", "rejected")
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(getContext(), "Report rejected", Toast.LENGTH_SHORT).show();
                        sendRejectionNotification(comment);
                    });
        }
    }

    private void sendRejectionNotification(Comment comment) {
        String reportedBy = comment.getReportedBy();

        Notification notification = new Notification(
                "Report Rejected",
                "Your report for the comment has been rejected.",
                reportedBy,
                false,
                Notification.UserType.PUP
        );

        db.collection("notifications")
                .add(notification)
                .addOnSuccessListener(documentReference -> Toast.makeText(getContext(), "Notification sent", Toast.LENGTH_SHORT).show())
                .addOnFailureListener(e -> Toast.makeText(getContext(), "Failed to send notification", Toast.LENGTH_SHORT).show());
    }
}
