package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategorySuggestion implements Parcelable {

    private Long id;
    private String subCategoryName;
    private SubCategory.Status status;
    private String description;
    private String senderId;
    private String senderEmail;
    private Category category;



    public SubCategorySuggestion() {
    }

    public SubCategorySuggestion(Long id, String suggestion, SubCategory.Status status, String descripiton, String senderEmail, Category category,String userId) {
        this.id = id;
        this.senderId = userId;
        this.subCategoryName = suggestion;
        this.status = status;
        this.description = descripiton;
        this.senderEmail = senderEmail;
        this.category = category; // Inicijalizacija polja za kategoriju
    }

    protected SubCategorySuggestion(Parcel in) {
        id = in.readLong();
        senderId = in.readString();
        subCategoryName = in.readString();
        description = in.readString();
        status = SubCategory.Status.valueOf(in.readString());
        senderEmail = in.readString();
        category = in.readParcelable(Category.class.getClassLoader());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getsubCategoryName() {
        return subCategoryName;
    }

    public void setsubCategoryName(String suggestion) {
        this.subCategoryName = suggestion;
    }

    public SubCategory.Status getStatus() {
        return status;
    }

    public void setStatus(SubCategory.Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String descripiton) {
        this.description = descripiton;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getUserId() {
        return senderId;
    }

    public void setUserId(String userId) {
        this.senderId = userId;
    }

    @Override
    public String toString() {
        return "SubCategorySuggestion{" +
                "id=" + id +
                ", suggestion='" + subCategoryName + '\'' +
                ", status=" + status +
                ", descripiton='" + description + '\'' +
                ", userName='" + senderEmail + '\'' +
                '}';
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeLong(id);
        dest.writeString(subCategoryName);
        dest.writeString(status.name());
        dest.writeString(description);
        dest.writeString(senderEmail);
        dest.writeParcelable(category, flags);
        dest.writeString(senderId);
    }

    public static final Creator<SubCategorySuggestion> CREATOR = new Creator<SubCategorySuggestion>() {
        @Override
        public SubCategorySuggestion createFromParcel(Parcel in) {
            return new SubCategorySuggestion(in);
        }

        @Override
        public SubCategorySuggestion[] newArray(int size) {
            return new SubCategorySuggestion[size];
        }
    };
}
