package com.example.eventplanner.fragments.products;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
//import com.example.eventplanner.activities.Categories.CategoriesActivity;
import com.example.eventplanner.activities.HomeActivity;
import com.example.eventplanner.databinding.FragmentCategoriesPageBinding;
import com.example.eventplanner.databinding.FragmentProductsPageBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.AddCategoryFragment;
import com.example.eventplanner.fragments.categories.CategoriesFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Favorites;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.SubCategory;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.slider.RangeSlider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class ProductsPageFragment extends Fragment {

    public static ArrayList<Product> products = new ArrayList<Product>();
    //public static ArrayList<Service> services = new ArrayList<Service>();
    public static ArrayList<Package> packages = new ArrayList<Package>();
    public static ArrayList<DocumentSnapshot> mProductsDocuments = new ArrayList<>();
    public static ArrayList<DocumentSnapshot> mServicesDocuments = new ArrayList<>();
    public static ArrayList<DocumentSnapshot> mPackagesDocuments = new ArrayList<>();

    public User user = new User();

    private FragmentProductsPageBinding binding;



    public ProductsPageFragment() {
        // Required empty public constructor
    }


    public static ProductsPageFragment newInstance() {
        ProductsPageFragment fragment = new ProductsPageFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentActivity currentActivity = getActivity();

        binding = FragmentProductsPageBinding.inflate(inflater,container,false);
        View root = binding.getRoot();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String email = currentUser.getEmail();
        findUserByUserEmail(currentUser.getEmail(), new OnUserLoadedListener() {
            @Override
            public void onUserLoaded(User company) {
                user = company;
                Log.d("ProductsPageFragment", "User loaded: " + user.getEmail());
                selectProducts(currentActivity);
            }
        });



        int checkedButtonId = binding.toggleGroupProducts.getCheckedButtonId();
        if(checkedButtonId == R.id.button_products){
            FragmentTransition.to(ProductsFragment.newInstance(mProductsDocuments),currentActivity,true,R.id.list_layout_products);
        }
        else if (checkedButtonId == R.id.button_services) {
            FragmentTransition.to(ServicesFragment.newInstance(mServicesDocuments), currentActivity, true, R.id.list_layout_products);
        }
        else if(checkedButtonId == R.id.button_packages){
            FragmentTransition.to(PackagesFragment.newInstance(mPackagesDocuments), currentActivity, true, R.id.list_layout_products);
        }




        Button favoritesButton = binding.Favorites;
        favoritesButton.setVisibility(View.VISIBLE);
        favoritesButton.setOnClickListener(v -> {
            FragmentTransition.to(new FavoritesFragment(), currentActivity, true, R.id.list_layout_products);
        });


        FloatingActionButton addButton = binding.floatingActionButtonProducts;
        addButton.setVisibility(View.VISIBLE);


        Button btnFilters = binding.btnFilters;
        btnFilters.setVisibility(View.VISIBLE);
        btnFilters.setOnClickListener(v -> {
            Log.i("ShopApp", "Bottom Sheet Dialog");
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity(), R.style.FullScreenBottomSheetDialog);
            View dialogView = getLayoutInflater().inflate(R.layout.product_filter, null);

            // Capture references to the UI elements
            EditText editTextFilterName = dialogView.findViewById(R.id.editTextFilterName);
            Spinner spinnerCategory = dialogView.findViewById(R.id.spinnerCategory);
            Spinner spinnerEventType = dialogView.findViewById(R.id.spinnerEventType);
            CheckBox checkboxAvailability = dialogView.findViewById(R.id.checkboxAvailability);

            // Load categories and event types into Spinners
            loadCategoriesIntoSpinner(spinnerCategory);
            loadEventTypesIntoSpinner(spinnerEventType);

            // Set a listener for the "Apply Filters" button
            Button applyFiltersButton = dialogView.findViewById(R.id.apply_filters_button);
            applyFiltersButton.setOnClickListener(applyView -> {
                // Get filter values from UI
                String nameFilter = editTextFilterName.getText().toString().trim();
                String selectedCategory = spinnerCategory.getSelectedItem().toString();
                String selectedEventType = spinnerEventType.getSelectedItem().toString();
                boolean isAvailableOnly = checkboxAvailability.isChecked();

                // Apply the filtering logic
                applyFilters(nameFilter, selectedCategory, selectedEventType, isAvailableOnly);
                bottomSheetDialog.dismiss();
            });

            bottomSheetDialog.setContentView(dialogView);
            bottomSheetDialog.show();
        });



        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedButtonId = binding.toggleGroupProducts.getCheckedButtonId();
                if(checkedButtonId == R.id.button_products){
                    addButton.setVisibility(View.GONE);
                    FragmentTransition.to(AddProductFragment.newInstance(),currentActivity,true,R.id.list_layout_products);
                   // btnFilters.setVisibility(View.GONE);
                }
                else if (checkedButtonId == R.id.button_services) {
                    //btnFilters.setVisibility(View.GONE);
                    addButton.setVisibility(View.GONE);
                   FragmentTransition.to(AddServiceFragment.newInstance(), currentActivity, true, R.id.list_layout_products);
                }
                else if (checkedButtonId == R.id.button_packages) {
                    //btnFilters.setVisibility(View.GONE);
                    addButton.setVisibility(View.GONE);
                    FragmentTransition.to(AddPackageFragment.newInstance(), currentActivity, true, R.id.list_layout_products);
                }
            }
        });

        Button servicesBtn = binding.buttonServices;
        Button productsBtn = binding.buttonProducts;
        Button packagesBtn = binding.buttonPackages;




        servicesBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               selectServices(currentActivity);
              //FragmentTransition.to(ServicesFragment.newInstance(services),currentActivity,true,R.id.list_layout_products);
              addButton.setVisibility(View.VISIBLE);
              // btnFilters.setVisibility(View.VISIBLE);
            }
        });
        productsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProducts(currentActivity);
                //FragmentTransition.to(ProductsFragment.newInstance(mProductsDocuments),currentActivity,true,R.id.list_layout_products);
                addButton.setVisibility(View.VISIBLE);
                //btnFilters.setVisibility(View.VISIBLE);
            }
        });
        packagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               selectPackages(currentActivity);
               // FragmentTransition.to(PackagesFragment.newInstance(packages),currentActivity,true,R.id.list_layout_products);
                addButton.setVisibility(View.VISIBLE);
                //btnFilters.setVisibility(View.VISIBLE);
            }
        });

        return root;
    }

    private void applyFilters(String nameFilter, String categoryFilter, String eventTypeFilter, boolean isAvailableOnly) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Query query;

        // Determine the collection based on the selected button (products/services/packages)
        int checkedButtonId = binding.toggleGroupProducts.getCheckedButtonId();
        if (checkedButtonId == R.id.button_products) {
            query = db.collection("products");
        } else if (checkedButtonId == R.id.button_services) {
            query = db.collection("services");
        } else {
            query = db.collection("packages");
        }

        // Apply partial name filter using range query
        if (!nameFilter.isEmpty()) {
            query = query.whereGreaterThanOrEqualTo("name", nameFilter)
                    .whereLessThanOrEqualTo("name", nameFilter + "\uf8ff");
        }

        // Apply category filter
        //if (!categoryFilter.equals("All")) { // Assuming "All" means no specific category selected
        //    query = query.whereEqualTo("category.name", categoryFilter);
        //}

        // Apply event type filter
        if (!eventTypeFilter.equals("All")) { // Assuming "All" means no specific event type selected
            query = query.whereEqualTo("eventType.name", eventTypeFilter);
        }

        // Apply availability filter
        if (isAvailableOnly) {
            query = query.whereEqualTo("available", true);
        }

        // Execute the query and handle results
        query.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                ArrayList<DocumentSnapshot> filteredDocuments = new ArrayList<>();
                for (QueryDocumentSnapshot document : task.getResult()) {
                    filteredDocuments.add(document);
                }

                // Display the filtered results based on the selected button
                FragmentActivity currentActivity = getActivity();
                if (checkedButtonId == R.id.button_products) {
                    FragmentTransition.to(ProductsFragment.newInstance(filteredDocuments), currentActivity, true, R.id.list_layout_products);
                } else if (checkedButtonId == R.id.button_services) {
                    FragmentTransition.to(ServicesFragment.newInstance(filteredDocuments), currentActivity, true, R.id.list_layout_products);
                } else if (checkedButtonId == R.id.button_packages) {
                    FragmentTransition.to(PackagesFragment.newInstance(filteredDocuments), currentActivity, true, R.id.list_layout_products);
                }
            } else {
                Toast.makeText(getActivity(), "Error getting documents", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void loadEventTypesIntoSpinner(Spinner spinner) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("eventTypes")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ArrayList<String> eventTypes = new ArrayList<>();
                        eventTypes.add("All");  // Default option for no event type selected
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Event eventType = document.toObject(Event.class);
                            eventTypes.add(eventType.getName()); // Assuming Event class has a getName() method
                        }

                        // Populate the Spinner with event type names
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, eventTypes);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                    } else {
                        Toast.makeText(getActivity(), "Failed to load event types", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void loadCategoriesIntoSpinner(Spinner spinner) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ArrayList<String> categories = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Category category = document.toObject(Category.class);
                            categories.add(category.getName()); // Assuming Category class has a getName() method
                        }

                        // Populate the Spinner with category names
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categories);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                    } else {
                        Toast.makeText(getActivity(), "Failed to load categories", Toast.LENGTH_SHORT).show();
                    }
                });
    }



    private void selectProducts(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(user.getUserType()== User.UserType.OD && user.isEnabled()) {

            db.collection("products").whereEqualTo("visible",true)
            .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mProductsDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mProductsDocuments.add(document);
                                }
                                FragmentTransition.to(ProductsFragment.newInstance(mProductsDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
        if(user.getUserType()== User.UserType.PUP){
            db.collection("products").whereEqualTo("userEmail",user.getEmail()).whereEqualTo("visible",true)
            .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mProductsDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mProductsDocuments.add(document);
                                }
                                FragmentTransition.to(ProductsFragment.newInstance(mProductsDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
        if(user.getUserType()== User.UserType.ADMIN){
            db.collection("products")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mProductsDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mProductsDocuments.add(document);
                                }
                                FragmentTransition.to(ProductsFragment.newInstance(mProductsDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
    }

    private void findUserByUserEmail(String email, ProductsPageFragment.OnUserLoadedListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                user= document.toObject(User.class);
                                listener.onUserLoaded(user);
                                return;
                            }
                        } else {
                            Toast.makeText(getActivity(), "Error getting user", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void selectServices(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(user.getUserType()== User.UserType.OD && user.isEnabled()) {

            db.collection("services").whereEqualTo("visible",true)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mServicesDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mServicesDocuments.add(document);
                                }
                                FragmentTransition.to(ServicesFragment.newInstance(mServicesDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
        if(user.getUserType()== User.UserType.PUP){
            db.collection("services").whereEqualTo("userEmail",user.getEmail()).whereEqualTo("visible",true)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mServicesDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mServicesDocuments.add(document);
                                }
                                FragmentTransition.to(ServicesFragment.newInstance(mServicesDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
        if(user.getUserType()== User.UserType.ADMIN){
            db.collection("services")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mServicesDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mServicesDocuments.add(document);
                                }
                                FragmentTransition.to(ServicesFragment.newInstance(mServicesDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
    }

    private void selectPackages(FragmentActivity currentActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(user.getUserType()== User.UserType.OD && user.isEnabled()) {

            db.collection("packages").whereEqualTo("visible",true)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mPackagesDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mPackagesDocuments.add(document);
                                }
                                FragmentTransition.to(PackagesFragment.newInstance(mPackagesDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
        if(user.getUserType()== User.UserType.PUP){
            db.collection("packages").whereEqualTo("userEmail",user.getEmail()).whereEqualTo("visible",true)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mPackagesDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mPackagesDocuments.add(document);
                                }
                                FragmentTransition.to(PackagesFragment.newInstance(mPackagesDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
        if(user.getUserType()== User.UserType.ADMIN){
            db.collection("packages")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                mPackagesDocuments.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    mPackagesDocuments.add(document);
                                }
                                FragmentTransition.to(PackagesFragment.newInstance(mPackagesDocuments), currentActivity, true, R.id.list_layout_products);
                            } else {
                                Toast.makeText(newInstance().getActivity(), "Error getting categories", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
        }
    }
    public void onDestroyView() {

        super.onDestroyView();



    }

    interface OnUserLoadedListener {
        void onUserLoaded(User user);
    }


}