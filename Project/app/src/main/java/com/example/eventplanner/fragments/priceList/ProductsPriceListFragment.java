package com.example.eventplanner.fragments.priceList;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ProductListAdapter;
import com.example.eventplanner.adapters.ProductPriceListAdapter;
import com.example.eventplanner.databinding.FragmentProductsBinding;
import com.example.eventplanner.databinding.FragmentProductsPriceListBinding;
import com.example.eventplanner.fragments.products.ProductsFragment;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductsPriceListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductsPriceListFragment extends Fragment {

    private ProductPriceListAdapter adapter;

    private ArrayList<DocumentSnapshot> mProducts = new ArrayList<>();

    private FragmentProductsPriceListBinding binding;
    private static final String ARG_PARAM = "param";

    private String mParam1;
    private String mParam2;

    public ProductsPriceListFragment() {
        // Required empty public constructor
    }



    public static ProductsPriceListFragment newInstance(ArrayList<DocumentSnapshot> products) {
        ProductsPriceListFragment fragment = new ProductsPriceListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, products);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProducts = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new ProductPriceListAdapter(getActivity(), mProducts);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProductsPriceListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ListView listView = root.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}