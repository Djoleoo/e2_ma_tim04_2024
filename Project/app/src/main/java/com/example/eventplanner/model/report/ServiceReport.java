package com.example.eventplanner.model.report;

import android.os.Parcel;
import android.os.Parcelable;


import com.example.eventplanner.model.Service;

import java.util.Date;

public class ServiceReport implements Parcelable {
    private Long id;

    private Service service;
    private Date time;
    private String reason;
    private ServiceReport.Status status;

    public ServiceReport(){

    }

    public ServiceReport(Long id, Service product, Date time, String reason, ServiceReport.Status status) {
        this.id = id;
        this.service = product;
        this.time = time;
        this.reason = reason;
        this.status = status;
    }

    protected ServiceReport(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        service = in.readParcelable(Service.class.getClassLoader());
        time = new Date(in.readLong());
        reason = in.readString();
        status = ServiceReport.Status.valueOf(in.readString());


    }

    public static final Creator<ServiceReport> CREATOR = new Creator<ServiceReport>() {
        @Override
        public ServiceReport createFromParcel(Parcel in) {
            return new ServiceReport(in);
        }

        @Override
        public ServiceReport[] newArray(int size) {
            return new ServiceReport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeParcelable(service, flags);
        dest.writeLong(time.getTime());
        dest.writeString(reason);
        dest.writeString(status.name());
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ServiceReport.Status getStatus() {
        return status;
    }

    public void setStatus(ServiceReport.Status status) {
        this.status = status;
    }




    public enum Status {
        REPORTED,
        ACCEPTED,
        REJECTED
    }

}
