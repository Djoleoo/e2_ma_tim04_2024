package com.example.eventplanner.fragments.registration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentCompanyInfoBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.CategoriesFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.PUPVRegistration;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;


public class CompanyInfoFragment extends Fragment {

    private ArrayList<DocumentSnapshot>mCategoriesDocuments = new ArrayList<>();
    private ArrayList<String> categoryId = new ArrayList<>();
    private FragmentCompanyInfoBinding binding;
    private static final String USER_TYPE_PARAM = "userType";
    private String usertype;
    private String userImage;
    ImageView img;
    Uri imageUri;
    private ActivityResultLauncher<Intent> galleryLauncher;


    public CompanyInfoFragment() {
        // Required empty public constructor
    }

    public static CompanyInfoFragment newInstance(String selectedRole) {
        CompanyInfoFragment fragment = new CompanyInfoFragment();
        Bundle args = new Bundle();
        args.putString(USER_TYPE_PARAM,selectedRole);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usertype = getArguments().getString(USER_TYPE_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCompanyInfoBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        LinearLayout categoryContainer = binding.categoryContainer;
        selectCategories(getActivity(),categoryContainer);
        img = binding.companyPicture;
        registerResult();

        EditText companyEmail = binding.companyInfoEmail;
        EditText companyName = binding.companyInfoName;
        EditText companyAddress = binding.companyInfoAdress;
        EditText companyDescription = binding.companyInfoDescription;
        EditText companyNumber = binding.companyInfoNumber;

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryLauncher.launch(galleryIntent); // Pokreće galeriju
            }
        });

        Button nextButton = binding.nextBtnCompanyInfo;
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean atLeastOneChecked = false;
                for (DocumentSnapshot document : mCategoriesDocuments) {
                    String documentId = document.getId();
                    CheckBox checkBox = (CheckBox) categoryContainer.findViewWithTag(documentId);
                    if (checkBox != null && checkBox.isChecked()) {
                        atLeastOneChecked = true;
                        break;
                    }
                }
                if (atLeastOneChecked) {
                    PUPVRegistration registration = new PUPVRegistration();
                    registration.setCompanyEmail(companyEmail.getText().toString());
                    registration.setCompanyName(companyName.getText().toString());
                    registration.setCompanyAddress(companyAddress.getText().toString());
                    registration.setCompanyDescription(companyDescription.getText().toString());
                    registration.setCompanyNumber(companyNumber.getText().toString());
                    if(imageUri != null){
                        registration.setCompanyImage(imageUri.toString());
                    }
                    findCheckedCategories(categoryContainer, new Runnable() {
                        @Override
                        public void run() {
                            for(String id : categoryId){
                                Log.d("ID:",id);
                            }
                            registration.setCategories(categoryId);
                            // Ovo će se izvršiti tek nakon što se završi findCheckedCategories
                            FragmentTransition.to(EventsAndWorkHoursFragment.newInstance(usertype,registration), getActivity(), true, R.id.loginCard);
                        }
                    });
                } else {
                    // Ako nijedan CheckBox nije označen, prikaži toast poruku
                    Toast.makeText(getActivity(), "Morate izabrati barem jednu kategoriju.", Toast.LENGTH_SHORT).show();
                }
            }

        });
        return root;
    }

    private void selectCategories(FragmentActivity currentActivity,LinearLayout categoryContainer){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mCategoriesDocuments.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                mCategoriesDocuments.add(document);
                                String categoryName = document.toObject(Category.class).getName();
                                CheckBox checkBox = new CheckBox(currentActivity);
                                checkBox.setText(categoryName);
                                checkBox.setTag(document.getId());
                                categoryContainer.addView(checkBox);
                            }
                        } else {
                            Toast.makeText(currentActivity, "Error getting categories", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void findCheckedCategories(LinearLayout categoryContainer, Runnable onComplete) {
        for (DocumentSnapshot document : mCategoriesDocuments) {
            Log.d("DOKUMENT",document.toString());
            String documentId = document.getId();
            CheckBox checkBox = (CheckBox) categoryContainer.findViewWithTag(documentId);
            if (checkBox != null && checkBox.isChecked()) {
                categoryId.add(documentId);
            }
        }
        // Nakon završetka pretrage, pozovite onComplete metodu
        onComplete.run();
    }

    private void registerResult(){
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                try{
                    imageUri = result.getData().getData();
                    img.setImageURI(imageUri);
                }catch (Exception e){
                    Toast.makeText(getActivity(),"NO image selected" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}