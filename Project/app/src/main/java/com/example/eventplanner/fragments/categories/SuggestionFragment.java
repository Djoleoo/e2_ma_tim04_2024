package com.example.eventplanner.fragments.categories;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.SuggestionsListAdapter;
import com.example.eventplanner.databinding.FragmentSuggestionBinding;
import com.example.eventplanner.model.SubCategorySuggestion;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class SuggestionFragment extends ListFragment {

    private SuggestionsListAdapter adapter;
    private ArrayList<DocumentSnapshot> mSuggestions = new ArrayList<>();

    private static final String ARG_PARAM = "param";

    private FragmentSuggestionBinding binding;

    public SuggestionFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static SuggestionFragment newInstance(ArrayList<DocumentSnapshot> suggestions) {
        SuggestionFragment fragment = new SuggestionFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM,suggestions);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSuggestions = (ArrayList<DocumentSnapshot>) getArguments().getSerializable(ARG_PARAM);
            adapter = new SuggestionsListAdapter(getActivity(),mSuggestions);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSuggestionBinding.inflate(inflater,container,false);
        View root = binding.getRoot();

        ListView listView = binding.list;
        listView.setAdapter(adapter);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}