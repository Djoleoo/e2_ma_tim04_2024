package com.example.eventplanner.fragments.events;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.StringListAdapter;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.AddSubCategoryFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;


public class SingleEventFragment extends Fragment {

    private static final String ARG_EVENT = "event";
    private static final String ARG_EVENT_ID = "eventId";
    private ArrayList<SubCategory> eventSubCategories = new ArrayList<>();
    private ArrayList<SubCategory> availableSubCategories = new ArrayList<>();
    ArrayList<String> subcategoryNames = new ArrayList<>();
    private Event event;
    private String documentId;



    public SingleEventFragment() {
        // Required empty public constructor
    }

    public static SingleEventFragment newInstance(Event event, String documentId) {
        SingleEventFragment fragment = new SingleEventFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_EVENT, event);
        args.putString(ARG_EVENT_ID, documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            event = getArguments().getParcelable(ARG_EVENT);
            documentId = getArguments().getString(ARG_EVENT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_single_event, container, false);

        eventSubCategories = event.getSuggestedSubCategories();
        selectSubCategories();
        TextView eventNameLabel = rootView.findViewById(R.id.event_type_edit);
        ListView subcategoriesListView = rootView.findViewById(R.id.subcategories_list_view);
        EditText eventDescription = rootView.findViewById(R.id.event_description_edit);
        Button editBtn = rootView.findViewById(R.id.button_edit);
        Button submitBtn = rootView.findViewById(R.id.button_confirm);
        //Button cancelBtn = rootView.findViewById(R.id.button_cancel);
        Button addSubCategoryBtn = rootView.findViewById(R.id.add_subcategory_button);


        eventNameLabel.setText(event.getName());
        eventDescription.setText(event.getDescription());

        for (SubCategory subcategory : eventSubCategories) {
            subcategoryNames.add(subcategory.getName());
        }

        StringListAdapter adapter = new StringListAdapter(requireContext(), subcategoryNames);
        subcategoriesListView.setAdapter(adapter);



        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventDescription.setEnabled(true);
                submitBtn.setVisibility(View.VISIBLE);
                //cancelBtn.setVisibility(View.VISIBLE);
                editBtn.setVisibility(View.GONE);
                addSubCategoryBtn.setVisibility(View.VISIBLE);
                // Pronalazimo sve slike unutar ListView-a i postavljamo njihovu vidljivost
                for (int i = 0; i < subcategoriesListView.getCount(); i++) {
                    View listItem = subcategoriesListView.getChildAt(i);
                    if (listItem != null) {
                        ImageView deleteIconImageView = listItem.findViewById(R.id.delete_icon);
                        deleteIconImageView.setVisibility(View.VISIBLE);
                    }
                }

            }
        });
        addSubCategoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!availableSubCategories.isEmpty()) {
                    availableSubCategories.removeAll(eventSubCategories);
                    showSubcategoryDialog(availableSubCategories);
                }
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event updatedEvent = new Event(event.getName(),eventDescription.getText().toString(),eventSubCategories,false);
                showConfirmationDialog(updatedEvent,eventDescription, submitBtn, editBtn,addSubCategoryBtn,subcategoriesListView);
            }
        });
        return rootView;
    }

    private void showSubcategoryDialog(ArrayList<SubCategory> subcategories) {
        subcategories.removeAll(eventSubCategories);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_subcategory, null);
        builder.setView(dialogView);
        ArrayList<String> names = getSubCategoryNames(subcategories);
        names.removeAll(subcategoryNames);

        // Initialize Spinner
        Spinner subcategorySpinner = dialogView.findViewById(R.id.subcategory_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(),
                android.R.layout.simple_spinner_item,
                names);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subcategorySpinner.setAdapter(adapter);

        builder.setPositiveButton("Add", (dialog, which) -> {
            String selectedSubcategory = (String) subcategorySpinner.getSelectedItem();
            SubCategory subCategory = null;
            for(SubCategory subCategory1 : subcategories){
                if(subCategory1.getName().equals(selectedSubcategory)){
                    subCategory = subCategory1;
                    break;
                }
            }
            addSelectedSubcategory(subCategory);
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void showConfirmationDialog(Event updatedEvent,final EditText eventDescription, final Button submitBtn, final Button editBtn,final Button addSubCategory,final ListView subcategoriesListView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit event");
        builder.setMessage("Are you sure you want to edit this event?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("eventTypes").document(documentId);
                docRef.set(updatedEvent)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                eventDescription.setEnabled(false);
                                submitBtn.setVisibility(View.GONE);
                                //cancelBtn.setVisibility(View.GONE);
                                editBtn.setVisibility(View.VISIBLE);
                                addSubCategory.setVisibility(View.GONE);

                                Toast.makeText(getContext(), "Event edited successfully", Toast.LENGTH_SHORT).show();
                                FragmentTransition.to(EventPageFragment.newInstance(), getActivity(), true, R.id.list_layout);
                                FloatingActionButton addBtn = requireActivity().findViewById(R.id.floating_action_button);
                                addBtn.setVisibility(View.VISIBLE);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                             public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(), "Puklo bajo", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void selectSubCategories() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("subCategories").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(task.isSuccessful()){
                                availableSubCategories.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    SubCategory subCategory = document.toObject(SubCategory.class);
                                    availableSubCategories.add(subCategory);
                                }
                            }
                        }
                });
    }
    private ArrayList<String> getSubCategoryNames(ArrayList<SubCategory> list) {
        ArrayList<String> categoryNames = new ArrayList<>();
        for (SubCategory subCategory : list) {
            categoryNames.add(subCategory.getName());
        }
        return categoryNames;
    }
    private void addSelectedSubcategory(SubCategory addedSubCategory) {
        eventSubCategories.add(addedSubCategory); // Dodajemo novu subkategoriju u listu
        subcategoryNames = getSubCategoryNames(eventSubCategories);
        // Pronalazimo ListView
        ListView selectedSubcategoriesListView = requireView().findViewById(R.id.subcategories_list_view);

        // Ako je ListView bio nevidljiv, sada ga prikazujemo
        selectedSubcategoriesListView.setVisibility(View.VISIBLE);
        ArrayAdapter<String> selectedSubcategoriesAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, subcategoryNames);
        selectedSubcategoriesListView.setAdapter(selectedSubcategoriesAdapter);

    }

    /*
    <Button
            android:id="@+id/button_cancel"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Cancel"
            android:visibility="gone"
            android:onClick="onCancelClick"
            />
     */

}