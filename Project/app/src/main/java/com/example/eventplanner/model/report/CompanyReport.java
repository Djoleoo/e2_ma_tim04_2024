package com.example.eventplanner.model.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;

import java.util.Date;

public class CompanyReport implements Parcelable{
    private Long id;

    private Company companyReported;
    private User userReported;
    private String userReport;

    private Date time;
    private String reason;
    private Status status;

    public CompanyReport(){

    }

    public CompanyReport(Long id, Company companyReported, User userReported, String userReport, Date time, String reason, Status status) {
        this.id = id;
        this.companyReported = companyReported;
        this.userReported = userReported;
        this.userReport = userReport;
        this.time = time;
        this.reason = reason;
        this.status = status;
    }

    public CompanyReport(Company companyReported, User userReported, String userReport, Date time, String reason, Status status) {
        this.companyReported = companyReported;
        this.userReported = userReported;
        this.userReport = userReport;
        this.time = time;
        this.reason = reason;
        this.status = status;
    }

    protected CompanyReport(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        companyReported = in.readParcelable(Company.class.getClassLoader());
        userReported = in.readParcelable(User.class.getClassLoader());
        userReport=in.readString();
        time = new Date(in.readLong());
        reason = in.readString();
        status = Status.valueOf(in.readString());


    }

    public static final Creator<CompanyReport> CREATOR = new Creator<CompanyReport>() {
        @Override
        public CompanyReport createFromParcel(Parcel in) {
            return new CompanyReport(in);
        }

        @Override
        public CompanyReport[] newArray(int size) {
            return new CompanyReport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeParcelable(companyReported, flags);
        dest.writeParcelable(userReported,flags);
        dest.writeString(userReport);
        dest.writeLong(time.getTime());
        dest.writeString(reason);
        dest.writeString(status.name());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompanyReported() {
        return companyReported;
    }

    public void setCompanyReported(Company companyReported) {
        this.companyReported = companyReported;
    }

    public User getUserReported() {
        return userReported;
    }

    public void setUserReported(User userReported) {
        this.userReported = userReported;
    }

    public String getUserReport() {
        return userReport;
    }

    public void setUserReport(String userReport) {
        this.userReport = userReport;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Status {
        REPORTED,
        ACCEPTED,
        REJECTED
    }
}
