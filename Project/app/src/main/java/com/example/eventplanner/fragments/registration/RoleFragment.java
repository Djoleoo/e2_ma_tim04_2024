package com.example.eventplanner.fragments.registration;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentLoginBinding;
import com.example.eventplanner.databinding.FragmentRoleBinding;
import com.example.eventplanner.fragments.FragmentTransition;

public class RoleFragment extends Fragment {

    private FragmentRoleBinding binding;

    public RoleFragment() {
        // Required empty public constructor
    }
    public static RoleFragment newInstance() {
        RoleFragment fragment = new RoleFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        //}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentRoleBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();

        Spinner spinner = binding.roleButton;
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.role_array));
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        Button NextButton = binding.nextBtnRole;
        NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedRole = spinner.getSelectedItem().toString();
                if(selectedRole.equals("OD")) {
                    FragmentTransition.to(PersonalInfoFragment.newInstance(selectedRole,null),getActivity(),true,R.id.loginCard);
                }else if (selectedRole.equals("PUP")) {
                    FragmentTransition.to(CompanyInfoFragment.newInstance(selectedRole), getActivity(), true, R.id.loginCard);
                }
            }
        });

        return root;
    }
}