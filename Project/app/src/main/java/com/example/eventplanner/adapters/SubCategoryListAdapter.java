package com.example.eventplanner.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.categories.SingleSubCategoryFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class SubCategoryListAdapter extends ArrayAdapter<DocumentSnapshot> {

    private ArrayList<DocumentSnapshot> aSubCategory;
    private FragmentActivity mContext;

    public SubCategoryListAdapter(FragmentActivity context, ArrayList<DocumentSnapshot> subCategories){
        super(context, R.layout.subcategory_list_item);
        mContext = context;
        aSubCategory = subCategories;
    }

    @Override
    public int getCount() {
        return aSubCategory.size();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return aSubCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DocumentSnapshot document = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.subcategory_list_item,
                    parent, false);
        }

        TextView subCategoryName = convertView.findViewById(R.id.subCategory_name);
        TextView subCategoryType = convertView.findViewById(R.id.subCategory_type);
        Button viewButton = convertView.findViewById(R.id.view_subCategory_button);
        Button deleteButton = convertView.findViewById(R.id.delete_button);


        if(document != null){
            String nameText = document.getString("name");
            subCategoryName.setText(nameText);
            String statusText = document.getString("status");
            SubCategory.Status status = SubCategory.Status.valueOf(statusText);
            subCategoryType.setText(status.toString());
            deleteFilter(document.toObject(SubCategory.class).getName(),deleteButton);
        }

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubCategory subCategory = document.toObject(SubCategory.class);
                View parent = (View) v.getParent();
                FloatingActionButton addBtn = parent.getRootView().findViewById(R.id.floating_action_button);
                addBtn.setVisibility(View.GONE);
                FragmentTransition.to(SingleSubCategoryFragment.newInstance(subCategory, document.getId()),mContext,true,R.id.list_layout);
            }

        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteConfirmationDialog(document);
            }
        });

        return convertView;
    }

    private void showDeleteConfirmationDialog(DocumentSnapshot documentSnapshot) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete SubCategory");
        builder.setMessage("Are you sure you want to delete this subCategory?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("categories").document(documentSnapshot.getId()).delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                Toast.makeText(getContext(),"Obrisalo se hehe",Toast.LENGTH_SHORT).show();
                                aSubCategory.remove(documentSnapshot);
                                notifyDataSetChanged();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getContext(),"Nije se obrisalo jbg",Toast.LENGTH_SHORT).show();
                            }
                        });

            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void deleteFilter(String subCategoryName,Button deleteButton){
        ArrayList<DocumentSnapshot>products = new ArrayList<>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if(subCategoryName.equals(document.toObject(Product.class).getSubcategory().getName())){
                                    products.add(document);
                                }
                            }
                            if (products.isEmpty()) {
                                deleteButton.setVisibility(View.VISIBLE);
                            } else {
                                deleteButton.setVisibility(View.INVISIBLE);
                            }

                        }
                    }
                });
    }
}
