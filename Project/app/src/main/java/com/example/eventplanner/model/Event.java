package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Event implements Parcelable {

    private String id;
    private String name;
    private String description;
    private ArrayList<SubCategory> suggestedSubCategories;
    private boolean isDisabled ;

    public Event() {
    }

    public Event(String id, String name, String description, ArrayList<SubCategory> suggestedSubCategories,boolean b) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.suggestedSubCategories = suggestedSubCategories;
        this.isDisabled = b;
    }

    public Event(String name, String description, ArrayList<SubCategory> suggestedSubCategories,boolean b) {
        this.name = name;
        this.description = description;
        this.suggestedSubCategories = suggestedSubCategories;
        this.isDisabled = false;
    }

    protected Event(Parcel in){
        id = in.readString();
        name = in.readString();
        description = in.readString();
        suggestedSubCategories = in.createTypedArrayList(SubCategory.CREATOR);
        isDisabled = in.readByte() != 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDisabled() {
        return isDisabled;
    }

    public void setDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    public ArrayList<SubCategory> getSuggestedSubCategories() {
        return suggestedSubCategories;
    }

    public void setSuggestedSubCategories(ArrayList<SubCategory> suggestedSubCategories) {
        this.suggestedSubCategories = suggestedSubCategories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeTypedList(suggestedSubCategories);
        dest.writeByte((byte) (isDisabled ? 1 : 0));
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return name; // Assuming 'name' is the field containing the name of the event
    }
}
