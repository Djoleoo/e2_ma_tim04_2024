package com.example.eventplanner.fragments.happenings;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.GuestAdapter;
import com.example.eventplanner.model.Guest;
import com.example.eventplanner.model.Happening;
import com.example.eventplanner.pdf.PdfGenerator;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.google.firebase.firestore.FirebaseFirestore;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GuestsFragment extends Fragment {

    private static final String ARG_HAPPENING = "happening";
    private Happening mHappening;
    private FirebaseFirestore db;
    private GuestAdapter guestAdapter;

    public GuestsFragment() {
        // Required empty public constructor
    }

    public static GuestsFragment newInstance(Happening happening) {
        GuestsFragment fragment = new GuestsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_HAPPENING, happening);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mHappening = getArguments().getParcelable(ARG_HAPPENING);
        }
        db = FirebaseFirestore.getInstance();  // Initialize Firestore
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_guests, container, false);

        // Example: Show the Happening name in a TextView
        TextView happeningNameTextView = rootView.findViewById(R.id.happening_name_text_view);
        happeningNameTextView.setText(mHappening.getName());

        // Set up RecyclerView
        RecyclerView guestsRecyclerView = rootView.findViewById(R.id.guests_recycler_view);
        guestsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        guestAdapter = new GuestAdapter(mHappening.getGuests(), mHappening, getContext());
        guestsRecyclerView.setAdapter(guestAdapter);

        // Floating Action Button to add a guest
        FloatingActionButton fab = rootView.findViewById(R.id.floating_action_button_guests);

        Button generatePdfButton = rootView.findViewById(R.id.generate_pdf_button);
        generatePdfButton.setOnClickListener(v -> generateGuestListPdf());
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddGuestDialog();
            }
        });

        return rootView;
    }

    private void showAddGuestDialog() {
        // Create a dialog for adding a guest
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_guest, null);
        builder.setView(dialogView);

        EditText guestNameEditText = dialogView.findViewById(R.id.guest_name_edit_text);
        Spinner ageSpinner = dialogView.findViewById(R.id.age_spinner);
        Spinner invitedSpinner = dialogView.findViewById(R.id.invited_spinner);
        Spinner acceptedSpinner = dialogView.findViewById(R.id.accepted_spinner);
        EditText specialRequirementsEditText = dialogView.findViewById(R.id.special_requirements_edit_text);

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = guestNameEditText.getText().toString();
                String age = ageSpinner.getSelectedItem().toString();  // Store age as a String
                boolean isInvited = invitedSpinner.getSelectedItem().toString().equals("Yes");
                boolean hasAccepted = acceptedSpinner.getSelectedItem().toString().equals("Yes");
                String specialRequirements = specialRequirementsEditText.getText().toString();

                Guest newGuest = new Guest(name, age, isInvited, hasAccepted, specialRequirements);
                mHappening.getGuests().add(newGuest);

                guestAdapter.notifyItemInserted(mHappening.getGuests().size() - 1);
                guestAdapter.updateHappeningInFirestore();
                Toast.makeText(getContext(), "Guest added", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void generateGuestListPdf() {
        PdfGenerator pdfGenerator = new PdfGenerator(getContext());
        pdfGenerator.generateGuestsPdf(mHappening);  // Generate the PDF for the guest list
    }
}
