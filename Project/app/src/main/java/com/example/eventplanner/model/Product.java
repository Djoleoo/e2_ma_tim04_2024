package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class Product implements Parcelable {
    private String id;  // Changed from Long to String
    private Category category;
    private SubCategory subcategory;
    private String name;
    private String description;
    private double price;
    private double discount;
    private double priceWithDiscount;
    private ArrayList<Integer> gallery;
    private ArrayList<Event> eventType;
    private boolean available;
    private boolean visible;

    private String userEmail;
    private Company company;

    // Constructor
    public Product(String id, Category category, SubCategory subcategory, String name, String description, double price,
                   double discount, ArrayList<Integer> gallery, ArrayList<Event> eventType, boolean available, boolean visible,String userEmail,Company company) {
        this.id = id;
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.priceWithDiscount = calculatePriceWithDiscount(price, discount);
        this.gallery = gallery;
        this.eventType = eventType;
        this.available = available;
        this.visible = visible;
        this.userEmail = userEmail;
        this.company = company;
    }

    public Product(Category category, SubCategory subcategory, String name, String description, double price,
                   double discount, ArrayList<Integer> gallery, ArrayList<Event> eventType, boolean available, boolean visible,String userEmail,Company company) {
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.priceWithDiscount = calculatePriceWithDiscount(price, discount);
        this.gallery = gallery;
        this.eventType = eventType;
        this.available = available;
        this.visible = visible;
        this.userEmail = userEmail;
        this.company = company;
    }

    public Product() {
    }

    // Getter and setter methods

    protected Product(Parcel in) {
        id = in.readString();  // Changed from Long to String
        category = in.readParcelable(Category.class.getClassLoader());
        subcategory = in.readParcelable(SubCategory.class.getClassLoader());
        name = in.readString();
        description = in.readString();
        price = in.readDouble();
        discount = in.readDouble();
        priceWithDiscount = in.readDouble();
        gallery = new ArrayList<>();
        in.readList(gallery, Integer.class.getClassLoader());
        eventType = in.createTypedArrayList(Event.CREATOR);
        available = in.readByte() != 0;
        visible = in.readByte() != 0;
        userEmail = in.readString();
        company = in.readParcelable(Company.class.getClassLoader());
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getId() {  // Changed from Long to String
        return id;
    }

    public void setId(String id) {  // Changed from Long to String
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public SubCategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(SubCategory subcategory) {
        this.subcategory = subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(double priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    public List<Integer> getGallery() {
        return gallery;
    }
    public ArrayList<Integer> getGallerys(){return  gallery;}

    public void setGallery(ArrayList<Integer> gallery) {
        this.gallery = gallery;
    }

    public List<Event> getEventType() {
        return eventType;
    }
    public ArrayList<Event> getEventTypes(){
        return eventType;
    }

    public void setEventType(ArrayList<Event> eventType) {
        this.eventType = eventType;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    // Method to calculate price with discount
    private double calculatePriceWithDiscount(double price, double discount) {
        return price - (price * discount / 100);
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);  // Changed from Long to String
        dest.writeParcelable(category, flags);
        dest.writeParcelable(subcategory, flags);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeDouble(priceWithDiscount);
        dest.writeTypedList(eventType);
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeByte((byte) (visible ? 1 : 0));
        dest.writeString(userEmail);
        dest.writeParcelable(company, flags);
    }
}
