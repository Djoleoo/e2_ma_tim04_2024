package com.example.eventplanner.fragments.priceList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.databinding.FragmentSingleProductBinding;
import com.example.eventplanner.databinding.FragmentSingleProductPriceListBinding;
import com.example.eventplanner.fragments.FragmentTransition;
import com.example.eventplanner.fragments.products.ProductsPageFragment;
import com.example.eventplanner.model.Category;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.SubCategory;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;


public class SingleProductPriceListFragment extends Fragment {

    private FragmentSingleProductPriceListBinding binding;
    private Product mProduct;
    private String documentId;

    private static final String ARG_PRODUCT = "product";
    private static final String ARG_PRODUCT_ID = "productId";


    public SingleProductPriceListFragment() {
        // Required empty public constructor
    }



    public static SingleProductPriceListFragment newInstance(Product product,String documentId) {
        SingleProductPriceListFragment fragment = new SingleProductPriceListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        args.putString(ARG_PRODUCT_ID,documentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(ARG_PRODUCT);
            documentId = getArguments().getString(ARG_PRODUCT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSingleProductPriceListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        EditText productName = binding.productNameEdit;
        EditText productPrice = binding.productPriceEdit;
        EditText productDiscount = binding.productDiscountEdit;
        Button submitButton = binding.buttonEditProduct;

        if (mProduct != null) {
            productName.setText(mProduct.getName());
            productPrice.setText(String.valueOf(mProduct.getPrice()));
            productDiscount.setText(String.valueOf(mProduct.getDiscount()));

        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                Product product = new Product(mProduct.getCategory(),mProduct.getSubcategory(),mProduct.getName(),mProduct.getDescription(),Double.parseDouble(productPrice.getText().toString()),Double.parseDouble(productDiscount.getText().toString()), mProduct.getGallerys(), mProduct.getEventTypes(),mProduct.isAvailable(),mProduct.isVisible(),currentUser.getEmail(),mProduct.getCompany());
                showEditConfirmationDialog(product);
            }
        });

        productPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Not needed
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    try {
                        // Attempt to parse the input as a decimal number
                        Double.parseDouble(s.toString());
                        productPrice.setText(s.toString());
                    } catch (NumberFormatException e) {
                        productPrice.setText("");
                    }
                }
            }
        });


        return root;
    }

    private void showEditConfirmationDialog(Product product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Edit Product");
        builder.setMessage("Are you sure you want to edit this product?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference docRef = db.collection("products").document(documentId);
                docRef.set(product)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {

                                FragmentTransition.to(PriceListPageFragment.newInstance(),getActivity(),true,R.id.list_layout_products);
                                Toast.makeText(getActivity(), "Product updated!", Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {@Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Nista bato", Toast.LENGTH_SHORT).show();
                        }});
            }
        });
        builder.setNegativeButton("No", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}