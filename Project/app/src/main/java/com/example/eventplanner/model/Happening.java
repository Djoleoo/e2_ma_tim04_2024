package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Happening implements Parcelable {
    private String id;
    private Event eventType;
    private String name;
    private String description;
    private int maxGuests;
    private String location;
    private int range;
    private boolean isPrivate;
    private List<Guest> guests;
    private List<Activity> activities;
    private User createdBy;  // New field for the User who created the Happening

    public Happening( Event eventType, String name, String description,
                     int maxGuests, String location, int range, boolean isPrivate, User createdBy) {
        this.eventType = eventType;
        this.name = name;
        this.description = description;
        this.maxGuests = maxGuests;
        this.location = location;
        this.range = range;
        this.isPrivate = isPrivate;
        this.guests = new ArrayList<>();
        this.activities = new ArrayList<>();
        this.createdBy = createdBy;
    }

    public Happening() {
        this.guests = new ArrayList<>();
        this.activities = new ArrayList<>();
    }

    protected Happening(Parcel in) {
        id = in.readString();
        eventType = in.readParcelable(Event.class.getClassLoader());
        name = in.readString();
        description = in.readString();
        maxGuests = in.readInt();
        location = in.readString();
        range = in.readInt();
        isPrivate = in.readByte() != 0;
        guests = in.createTypedArrayList(Guest.CREATOR);
        activities = in.createTypedArrayList(Activity.CREATOR);
        createdBy = in.readParcelable(User.class.getClassLoader());  // Read User from parcel
    }

    public static final Creator<Happening> CREATOR = new Creator<Happening>() {
        @Override
        public Happening createFromParcel(Parcel in) {
            return new Happening(in);
        }

        @Override
        public Happening[] newArray(int size) {
            return new Happening[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeParcelable(eventType, flags);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(maxGuests);
        dest.writeString(location);
        dest.writeInt(range);
        dest.writeByte((byte) (isPrivate ? 1 : 0));
        dest.writeTypedList(guests);
        dest.writeTypedList(activities);
        dest.writeParcelable(createdBy, flags);  // Write User to parcel
    }

    // Getters and Setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Event getEventType() {
        return eventType;
    }

    public void setEventType(Event eventType) {
        this.eventType = eventType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(int maxGuests) {
        this.maxGuests = maxGuests;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }
}
